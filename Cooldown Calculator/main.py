import numpy as np
import sys
import os
import time
import ConfigParser
import matplotlib.pyplot as plt

from scipy import sparse
from scipy.sparse import linalg
from subprocess import call

from src.cryolib import conductivity as TC
from src.cryolib import heatcapacity as HC
from src.cryolib import density as DEN
from src.cooling import coolers as CL
from src.cryolib import loading_sim as LS

from src.PreProcessing import mesh
from src.PreProcessing import contact
from src.PreProcessing import matrixbuilder


#80K Heat straps array definition
pos_80hs = np.array([])
r_80hs = np.array([])
l_80hs = np.array([])
m_80hs = []
typeHT_80hs = []
#40K Heat straps array definition
pos_40hs = np.array([])
r_40hs = np.array([])
l_40hs = np.array([])
m_40hs = []
typeHT_40hs = []
#4K Heat straps array definition
pos_4hs = np.array([])
r_4hs = np.array([])
l_4hs = np.array([])
m_4hs = []
typeHT_4hs = []

pos_1hs = np.array([])
r_1hs = np.array([])
l_1hs = np.array([])
m_1hs = []
typeHT_1hs=[]

pos1_drhs = np.array([])

pos_100hs = np.array([])
r_100hs = np.array([])
l_100hs = np.array([])
m_100hs = []
typeHT_100hs =[]

pos100_drhs = np.array([])
r_100drhs = np.array([])
l_100drhs = np.array([])
m_100drhs =[]
typeHT_100drhs =[]

#Import Files
conf_name = sys.argv[1]
filepath = 'config/'+conf_name
filtersfile = filepath+'/filters.cfg'
loadingfile = filepath+'/loading.cfg'
coolfile = filepath+'/masses.cfg'

model = ConfigParser.ConfigParser()
model.read(coolfile)
sections = model.sections()

for section in sections:

    if section.lower() == 'model':
        modelID = model.get(section,'Identifier').split('#')[0].strip()
        opt_num = int(model.get(section,'TubeQuantity').split('#')[0].strip())
        stage_num = int(model.get(section,'Stages').split('#')[0].strip())

    elif section.lower() == 'stages':
        options = model.options(section)
        stage_n = np.array([])
        for i in range(len(options)):
            stageval = float(model.get(section,options[i]).split('#')[0].strip())
            stage_n = np.append(stage_n, stageval)

    elif 'coolers' in section.lower():
        options = model.options(section)
        cool = []
        numbers_mc = np.array([], dtype = int)
        for opt in options:
            if 'stage' in opt.lower():
                mc = str(model.get(section,opt).split('#')[0])
                cool.append(mc)
            if 'numbers' in opt.lower():
                num_mc = int(model.get(section,opt).split('#')[0])
                numbers_mc = np.append(numbers_mc, num_mc)

    elif '80k' in section.lower():
        if 'generalinfo' in section.lower():
            n_80 = int(model.get(section,'ComponentNumbers').split('#')[0])
            n_80s = int(model.get(section,'ShellsNumbers').split('#')[0])
            if n_80s > 1:
                d_80shell = np.array([])
                l_80shell = np.array([])
                t_80shell = np.array([])
                m_80shell = []
                g_80shell = []
        elif 'shell' in section.lower():
            if n_80s == 1:
                d_80shell = float(model.get(section,'Diameter').split('#')[0])*1e-2
                l_80shell = float(model.get(section,'Length').split('#')[0])*1e-2
                t_80shell = float(model.get(section,'Thickness').split('#')[0])*1e-2
                m_80shell = model.get(section, 'Material').split('#')[0]
                g_80shell = model.get(section, 'Type').split('#')[0]
            else:
                d_80shell = np.append(d_80shell, float(model.get(section,\
                                     'Diameter').split('#')[0])*1e-2)
                l_80shell = np.append(l_80shell, float(model.get(section,\
                                      'Length').split('#')[0])*1e-2)
                t_80shell = np.append(t_80shell, float(model.get(section,\
                                      'Thickness').split('#')[0])*1e-2)
                m_80shell.append(model.get(section, 'Material').split('#')[0])
                g_80shell.append(model.get(section, 'Type').split('#')[0])
        elif 'filterplate' in section.lower():
            d_80fp = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_80fp = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_80fp = model.get(section, 'Material').split('#')[0]
            g_80fp = model.get(section, 'Type').split('#')[0]
        elif 'heat strap' in section.lower():
            pos = model.get(section,'Origin').split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos_80hs = np.append(pos_80hs, pos*1e-2)
            r_80hs = np.append(r_80hs, float(model.get(section,'Radius').split('#')[0].strip())*1e-2)
            l_80hs = np.append(l_80hs, float(model.get(section,'Length').split('#')[0].strip())*1e-2)
            m_80hs.append(model.get(section,'Material').split('#')[0].strip())
            typeHT_80hs.append(model.get(section,'Type').split('#')[0].strip())

    elif '40k' in section.lower():
        if 'generalinfo' in section.lower():
            n_40 = int(model.get(section,'ComponentNumbers').split('#')[0])
            n_40s = int(model.get(section,'ShellsNumbers').split('#')[0])
            if n_40s > 1:
                d_40shell = np.array([])
                l_40shell = np.array([])
                t_40shell = np.array([])
                m_40shell = []
                g_40shell = []
        elif 'shell' in section.lower():
            if n_40s == 1:
                d_40shell = float(model.get(section,'Diameter').split('#')[0])*1e-2
                l_40shell = float(model.get(section,'Length').split('#')[0])*1e-2
                t_40shell = float(model.get(section,'Thickness').split('#')[0])*1e-2
                m_40shell = (model.get(section, 'Material').split('#')[0]).strip()
                g_40shell = model.get(section, 'Type').split('#')[0]
            else:
                d_40shell = np.append(d_40shell, float(model.get(section,\
                                     'Diameter').split('#')[0])*1e-2)
                l_40shell = np.append(l_40shell, float(model.get(section,\
                                      'Length').split('#')[0])*1e-2)
                t_40shell = np.append(t_40shell, float(model.get(section,\
                                      'Thickness').split('#')[0])*1e-2)
                m_40shell.append(model.get(section, 'Material').split('#')[0])
                g_40shell.append(model.get(section, 'Type').split('#')[0])
        elif 'filterplate' in section.lower():
            d_40fp = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_40fp = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_40fp = model.get(section, 'Material').split('#')[0]
            g_40fp = model.get(section, 'Type').split('#')[0]
        elif 'bottomplate' in section.lower():
            d_40bp = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_40bp = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_40bp = model.get(section, 'Material').split('#')[0]
            g_40bp = model.get(section, 'Type').split('#')[0]
        elif 'heat strap' in section.lower():
            pos = model.get(section,'Origin').split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos_40hs = np.append(pos_40hs, pos*1e-2)
            r_40hs = np.append(r_40hs, float(model.get(section,'Radius').split('#')[0].strip())*1e-2)
            l_40hs = np.append(l_40hs, float(model.get(section,'Length').split('#')[0].strip())*1e-2)
            m_40hs.append(model.get(section,'Material').split('#')[0].strip())
            typeHT_40hs.append(model.get(section,'Type').split('#')[0].strip())

    elif '4k' in section.lower():
        if 'generalinfo' in section.lower():
            n_4 = int(model.get(section,'ComponentNumbers').split('#')[0])
            n_4s = int(model.get(section,'ShellsNumbers').split('#')[0])
            if n_4s > 1:
                d_4shell = np.array([])
                l_4shell = np.array([])
                t_4shell = np.array([])
                m_4shell = []
                g_4shell = []
        elif 'shell' in section.lower():
            if n_4s == 1:
                d_4shell = float(model.get(section,'Diameter').split('#')[0])*1e-2
                l_4shell = float(model.get(section,'Length').split('#')[0])*1e-2
                t_4shell = float(model.get(section,'Thickness').split('#')[0])*1e-2
                m_4shell = model.get(section, 'Material').split('#')[0]
                g_4shell = model.get(section, 'Type').split('#')[0]
            else:
                d_4shell = np.append(d_4shell, float(model.get(section,\
                                     'Diameter').split('#')[0])*1e-2)
                l_4shell = np.append(l_4shell, float(model.get(section,\
                                      'Length').split('#')[0])*1e-2)
                t_4shell = np.append(t_4shell, float(model.get(section,\
                                      'Thickness').split('#')[0])*1e-2)
                m_4shell.append(model.get(section, 'Material').split('#')[0])
                g_4shell.append(model.get(section, 'Type').split('#')[0])
        elif 'filterplate' in section.lower():
            d_4fp = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_4fp = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_4fp = model.get(section, 'Material').split('#')[0]
            g_4fp = model.get(section, 'Type').split('#')[0]
        elif 'bottomplate' in section.lower():
            d_4bp = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_4bp = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_4bp = model.get(section, 'Material').split('#')[0]
            g_4bp = model.get(section, 'Type').split('#')[0]
        elif 'magnetic shield' in section.lower():
            d_4ms = float(model.get(section,'Diameter').split('#')[0])*1e-2
            l_4ms = float(model.get(section,'Length').split('#')[0])*1e-2
            t_4ms = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_4ms = model.get(section, 'Material').split('#')[0]
            g_4ms = model.get(section, 'Type').split('#')[0]
        elif 'top optics tube' in section.lower():
            d_4ott = float(model.get(section,'Diameter').split('#')[0])*1e-2
            l_4ott = float(model.get(section,'Length').split('#')[0])*1e-2
            t_4ott = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_4ott = model.get(section, 'Material').split('#')[0]
            g_4ott = model.get(section, 'Type').split('#')[0]
        elif 'bottom optics tube' in section.lower():
            d_4otb = float(model.get(section,'Diameter').split('#')[0])*1e-2
            l_4otb = float(model.get(section,'Length').split('#')[0])*1e-2
            t_4otb = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_4otb = model.get(section, 'Material').split('#')[0]
            g_4otb = model.get(section, 'Type').split('#')[0]
        elif 'heat strap' in section.lower():
            pos = model.get(section,'Origin').split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos_4hs = np.append(pos_4hs, pos*1e-2)
            r_4hs = np.append(r_4hs, float(model.get(section,'Radius').split('#')[0].strip())*1e-2)
            l_4hs = np.append(l_4hs, float(model.get(section,'Length').split('#')[0].strip())*1e-2)
            m_4hs.append(model.get(section,'Material').split('#')[0].strip())
            typeHT_4hs.append(model.get(section,'Type').split('#')[0].strip())

    elif '1k' in section.lower():
        if 'generalinfo' in section.lower():
            n_1 = int(model.get(section,'ComponentNumbers').split('#')[0])
            n_1s = int(model.get(section,'ShellsNumbers').split('#')[0])
            if n_1s > 1:
                d_1shell = np.array([])
                l_1shell = np.array([])
                t_1shell = np.array([])
                m_1shell = []
                g_1shell = []
        elif 'shell' in section.lower():
            if n_1s == 1:
                d_1shell = float(model.get(section,'Diameter').split('#')[0])*1e-2
                l_1shell = float(model.get(section,'Length').split('#')[0])*1e-2
                t_1shell = float(model.get(section,'Thickness').split('#')[0])*1e-2
                m_1shell = model.get(section, 'Material').split('#')[0]
                g_1shell = model.get(section, 'Type').split('#')[0]
            else:
                d_1shell = np.append(d_1shell, float(model.get(section,\
                                     'Diameter').split('#')[0])*1e-2)
                l_1shell = np.append(l_1shell, float(model.get(section,\
                                      'Length').split('#')[0])*1e-2)
                t_1shell = np.append(t_1shell, float(model.get(section,\
                                      'Thickness').split('#')[0])*1e-2)
                m_1shell.append(model.get(section, 'Material').split('#')[0])
                g_1shell.append(model.get(section, 'Type').split('#')[0])
        elif 'busplate' in section.lower():
            d_1p = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_1p = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_1p = model.get(section, 'Material').split('#')[0]
            g_1p = model.get(section, 'Type').split('#')[0]
        elif 'bottomplate' in section.lower():
            d_1bp = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_1bp = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_1bp = model.get(section, 'Material').split('#')[0]
            g_1bp = model.get(section, 'Type').split('#')[0]
        elif 'drplate' in section.lower():
            d_1drp = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_1drp = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_1drp = model.get(section, 'Material').split('#')[0]
            g_1drp = model.get(section, 'Type').split('#')[0]
        elif 'top optics tube' in section.lower():
            d_1ott = float(model.get(section,'Diameter').split('#')[0])*1e-2
            l_1ott = float(model.get(section,'Length').split('#')[0])*1e-2
            t_1ott = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_1ott = model.get(section, 'Material').split('#')[0]
            g_1ott = model.get(section, 'Type').split('#')[0]
        elif 'bottom optics tube' in section.lower():
            d_1otb = float(model.get(section,'Diameter').split('#')[0])*1e-2
            l_1otb = float(model.get(section,'Length').split('#')[0])*1e-2
            t_1otb = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_1otb = model.get(section, 'Material').split('#')[0]
            g_1otb = model.get(section, 'Type').split('#')[0]
        elif 'heat strap' in section.lower():
            print section
            pos = model.get(section,'Origin').split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos_1hs = np.append(pos_1hs, pos*1e-2)
            r_1hs = np.append(r_1hs, float(model.get(section,'Radius').split('#')[0].strip())*1e-2)
            l_1hs = np.append(l_1hs, float(model.get(section,'Length').split('#')[0].strip())*1e-2)
            m_1hs.append(model.get(section,'Material').split('#')[0].strip())
            typeHT_1hs.append(model.get(section,'Type').split('#')[0].strip())
            pos = model.get(section,'DRorigin').split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos1_drhs = np.append(pos1_drhs, pos*1e-2)

    elif '100mk' in section.lower():
        if 'generalinfo' in section.lower():
            n_100 = int(model.get(section,'ComponentNumbers').split('#')[0])
            n_100s = int(model.get(section,'ShellsNumbers').split('#')[0])
            if n_100s > 1:
                d_100shell = np.array([])
                l_100shell = np.array([])
                t_100shell = np.array([])
                m_100shell = []
                g_100shell = []
        elif 'shell' in section.lower():
            if n_100s == 1:
                d_100shell = float(model.get(section,'Diameter').split('#')[0])*1e-2
                l_100shell = float(model.get(section,'Length').split('#')[0])*1e-2
                t_100shell = float(model.get(section,'Thickness').split('#')[0])*1e-2
                m_100shell = model.get(section, 'Material').split('#')[0]
                g_100shell = model.get(section, 'Type').split('#')[0]
            else:
                d_100shell = np.append(d_100shell, float(model.get(section,\
                                     'Diameter').split('#')[0])*1e-2)
                l_100shell = np.append(l_100shell, float(model.get(section,\
                                      'Length').split('#')[0])*1e-2)
                t_100shell = np.append(t_100shell, float(model.get(section,\
                                      'Thickness').split('#')[0])*1e-2)
                m_100shell.append(model.get(section, 'Material').split('#')[0])
                g_100shell.append(model.get(section, 'Type').split('#')[0])
        elif 'busplate' in section.lower():
            d_100p = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_100p = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_100p = model.get(section, 'Material').split('#')[0]
            g_100p = model.get(section, 'Type').split('#')[0]
        elif 'bottomplate' in section.lower():
            d_100bp = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_100bp = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_100bp = model.get(section, 'Material').split('#')[0]
            g_100bp = model.get(section, 'Type').split('#')[0]
        elif 'drplate' in section.lower():
            d_100drp = float(model.get(section,'Diameter').split('#')[0])*1e-2
            t_100drp = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_100drp = model.get(section, 'Material').split('#')[0]
            g_100drp = model.get(section, 'Type').split('#')[0]
        elif 'top optics tube' in section.lower():
            d_100ott = float(model.get(section,'Diameter').split('#')[0])*1e-2
            l_100ott = float(model.get(section,'Length').split('#')[0])*1e-2
            t_100ott = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_100ott = model.get(section, 'Material').split('#')[0]
            g_100ott = model.get(section, 'Type').split('#')[0]
        elif 'bottom optics tube' in section.lower():
            d_100otb = float(model.get(section,'Diameter').split('#')[0])*1e-2
            l_100otb = float(model.get(section,'Length').split('#')[0])*1e-2
            t_100otb = float(model.get(section,'Thickness').split('#')[0])*1e-2
            m_100otb = model.get(section, 'Material').split('#')[0]
            g_100otb = model.get(section, 'Type').split('#')[0]
        elif 'heat strap' in section.lower():
            pos = model.get(section,'Origin').split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos_100hs = np.append(pos_1hs, pos*1e-2)
            r_100hs = np.append(r_100hs, float(model.get(section,'Radius').split('#')[0].strip())*1e-2)
            l_100hs = np.append(l_100hs, float(model.get(section,'Length').split('#')[0].strip())*1e-2)
            m_100hs.append(model.get(section,'Material').split('#')[0].strip())
            typeHT_100hs.append(model.get(section,'Type').split('#')[0].strip())
            pos = model.get(section,'DRorigin').split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos100_drhs = np.append(pos100_drhs, pos*1e-2)


    elif '4dr plate' in section.lower():
        d_4drp = float(model.get(section,'Diameter').split('#')[0])*1e-2
        t_4drp = float(model.get(section,'Thickness').split('#')[0])*1e-2
        m_4drp = model.get(section, 'Material').split('#')[0]
        g_4drp = model.get(section, 'Type').split('#')[0]
        pos_4drp = np.zeros(2)
        pos = model.get(section,'PTCpos').split('#')[0].strip()[1:-1]
        pos = np.array(pos.split(',')).astype(float)

        pos_4drp = pos*1e-2
        rad_4drpPTC = float(model.get(section,'RadiusPTC').split('#')[0])*1e-2


    elif section.lower() == 'filters position':
        options = model.options(section)

        pos_fil = np.zeros((len(options)/2, 2))
        rad_fil = np.zeros(len(options)/2)
        for i in range(len(options)/2):

            pos = model.get(section,options[i*2]).split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos_fil[i,0] = pos[0]*1e-2
            pos_fil[i,1] = pos[1]*1e-2

            rad_fil[i] = float(model.get(section,options[i*2+1]).split('#')[0].strip())*1e-2

    elif section.lower() == '80/40 switch':

        options = model.options(section)
        pos80_1 = np.zeros((len(options)/5, 2))
        pos40_1 = np.zeros((len(options)/5, 2))
        rad_1 = np.zeros(len(options)/5)
        type_1 = []
        value_1 = []

        for i in range(len(options)/5):

            pos = model.get(section,options[i*5]).split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos80_1[i,0] = pos[0]*1e-2
            pos80_1[i,1] = pos[1]*1e-2

            pos = model.get(section,options[i*5+1]).split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos40_1[i,0] = pos[0]*1e-2
            pos40_1[i,1] = pos[1]*1e-2

            rad_1[i] = float(model.get(section,options[i*5+2]).split('#')[0].strip())*1e-2

            type_1.append(model.get(section,options[i*5+3]).split('#')[0].strip())

            if type_1[-1] == 'file':
                value_1.append(np.loadtxt(model.get(section,options[i*5+4]).split('#')[0].strip()))
            else:
                value_1.append(float(model.get(section,options[i*5+4]).split('#')[0]))

    elif section.lower() == '40/4 switch':

        options = model.options(section)
        pos40_2 = np.zeros((len(options)/5, 2))
        pos4_2 = np.zeros((len(options)/5, 2))
        rad_2 = np.zeros(len(options)/5)
        type_2 = []
        value_2 = []

        for i in range(len(options)/5):

            pos = model.get(section,options[i*5]).split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos40_2[i,0] = pos[0]*1e-2
            pos40_2[i,1] = pos[1]*1e-2

            pos = model.get(section,options[i*5+1]).split('#')[0].strip()[1:-1]
            pos = np.array(pos.split(',')).astype(float)

            pos4_2[i,0] = pos[0]*1e-2
            pos4_2[i,1] = pos[1]*1e-2

            rad_2[i] = float(model.get(section,options[i*5+2]).split('#')[0].strip())*1e-2

            type_2.append(model.get(section,options[i*5+3]).split('#')[0].strip())

            if type_2[-1] == 'file':
                value_2.append(np.loadtxt(model.get(section,options[i*5+4]).split('#')[0].strip()))
            else:
                value_2.append(float(model.get(section,options[i*5+4]).split('#')[0]))

    elif section.lower() == '1dr switch':
        options = model.options(section)

        pos = model.get(section,options[0]).split('#')[0].strip()[1:-1]
        pos = np.array(pos.split(',')).astype(float)

        pos41_dr = np.zeros(2)
        pos41_dr[0] = pos[0]*1e-2
        pos41_dr[1] = pos[1]*1e-2

        pos = model.get(section,options[1]).split('#')[0].strip()[1:-1]
        pos = np.array(pos.split(',')).astype(float)

        pos1_dr = np.zeros(2)
        pos1_dr[0] = pos[0]*1e-2
        pos1_dr[1] = pos[1]*1e-2

        rad_1dr = float(model.get(section,options[2]).split('#')[0].strip())*1e-2
        type_1dr = []

        type_1dr.append(model.get(section,options[3]).split('#')[0].strip())

        value_1dr = []

        if type_1dr[-1] == 'file':
            value_1dr.append(np.loadtxt(model.get(section,options[4]).split('#')[0].strip()))
        else:
            value_1dr.append(float(model.get(section,options[4]).split('#')[0]))

    elif section.lower() == '100dr switch':

        options = model.options(section)

        pos = model.get(section,options[0]).split('#')[0].strip()[1:-1]
        pos = np.array(pos.split(',')).astype(float)

        pos1100_dr = np.zeros(2)
        pos1100_dr[0] = pos[0]*1e-2
        pos1100_dr[1] = pos[1]*1e-2

        pos = model.get(section,options[1]).split('#')[0].strip()[1:-1]
        pos = np.array(pos.split(',')).astype(float)

        pos100_dr = np.zeros(2)
        pos100_dr[0] = pos[0]*1e-2
        pos100_dr[1] = pos[1]*1e-2

        rad_100dr = float(model.get(section,options[2]).split('#')[0].strip())*1e-2
        type_100dr =[]
        type_100dr.append(model.get(section,options[3]).split('#')[0].strip())
        value_100dr = []
        if type_100dr[-1]  == 'file':
            value_100dr.append(np.loadtxt(model.get(section,options[4]).split('#')[0].strip()))
        else:
            value_100dr.append(float(model.get(section,options[4]).split('#')[0]))


pos_80hs = np.reshape(pos_80hs, (len(pos_80hs)/2, 2))
pos_40hs = np.reshape(pos_40hs, (len(pos_40hs)/2, 2))
pos_4hs = np.reshape(pos_4hs, (len(pos_4hs)/2, 2))
pos_1hs = np.reshape(pos_1hs, (len(pos_1hs)/2, 2))
pos_100hs = np.reshape(pos_100hs, (len(pos_100hs)/2, 2))
pos1_drhs = np.reshape(pos1_drhs, (len(pos1_drhs)/2, 2))
pos100_drhs = np.reshape(pos100_drhs, (len(pos100_drhs)/2, 2))

#Meshing dimensions of the plates
nx = 250
ny = 250
#Meshing dimensions of the dr plates
nxdr = 80
nydr = 80
#Meshing dimensions of the cylinders
nr = 10
ne = 6
#Meshing dimensions of the rods
n = 10
#Time info
dt = 600. #Timestep in s
t = np.array([])
tf = 0.

#Optics Tubes Temperature Arrays 4K
T_ot_top = np.ones((opt_num, nr, ne))*290.
T_ot_bottom = np.ones((opt_num, nr, ne))*290.
T_ot_top_f = np.ones((opt_num, nr, ne))*290.
T_ot_bottom_f = np.ones((opt_num, nr, ne))*290.

#Optics Tubes Temperature Arrays 1K
T_ot_top1 = np.ones((opt_num, nr, ne))*290.
T_ot_bottom1 = np.ones((opt_num, nr, ne))*290.
T_ot_top1_f = np.ones((opt_num, nr, ne))*290.
T_ot_bottom1_f = np.ones((opt_num, nr, ne))*290.

#Optics Tubes Temperature Arrays 0.1K
T_ot_top100 = np.ones((opt_num, nr, ne))*290.
T_ot_bottom100 = np.ones((opt_num, nr, ne))*290.
T_ot_top100_f = np.ones((opt_num, nr, ne))*290.
T_ot_bottom100_f = np.ones((opt_num, nr, ne))*290.

#Shells Temperature Arrays
T_80shell = np.ones((nr,ne))*290.
T_80shell_f = T_80shell.copy()
T_40shell = np.ones((nr,ne))*290.
T_40shell_f = T_40shell.copy()
T_4shell = np.ones((nr,ne))*290.
T_4shell_f = T_4shell.copy()
T_1shell = np.ones((nr,ne))*290.
T_1shell_f = T_1shell.copy()
T_100shell = np.ones((nr,ne))*290.
T_100shell_f = T_100shell.copy()

#Heat Straps Temperature Arrays
T_80strap = np.ones((numbers_mc[0], n))*290.
T_80strap_f = T_80strap.copy()
T_40strap = np.ones((numbers_mc[1], n))*290.
T_40strap_f = T_40strap.copy()
T_4strap = np.ones((numbers_mc[1], n))*290.
T_4strap_f = T_4strap.copy()
T_1strap = np.ones((4, n))*290.
T_1strap_f = T_1strap.copy()
T_100strap = np.ones((4, n))*290.
T_100strap_f = T_100strap.copy()

#Cooling Power
cool80 = np.zeros(numbers_mc[0])
cool40 = np.zeros(numbers_mc[1])
cool4 = np.zeros(numbers_mc[1])

'''
80K Stage Preprocessing
'''

#Meshing filter Plate
filterplate = mesh.holedplate(d_80fp/2., t_80fp, opt_num, rad_fil-t_4ott, pos_fil)
T_80FilterPlate = filterplate.meshing(nx, ny, '80K_plate')*290.

#Contact between straps and filter plate
Strap_FilterPlate_80_cont = []
for i in range(numbers_mc[0]):
    c = contact.contactCRod(d_80fp/2., r_80hs[i]*10, pos_80hs[i])
    cont = c.contact(T_80FilterPlate, '80Kstrap_'+str(i), plot=False)
    Strap_FilterPlate_80_cont.append(cont)


#Contact between filter plate and filters
Opt_FilterPlate_80_cont = []
for i in range(opt_num):
    c = contact.contactCRingInt(d_80fp/2., rad_fil[i], rad_fil[i]-t_4ott, pos_fil[i])
    cont = c.contact(T_80FilterPlate, ne, '80Kfilters_'+str(i), plot=False)
    Opt_FilterPlate_80_cont.append(cont)

#Contact between filter plate and switches
Switch_FilterPlate_80_cont = []
for i in range(len(rad_1)):
    c = contact.contactCRod(d_80fp/2., rad_1[i], pos80_1[i])
    cont = c.contact(T_80FilterPlate, '80Kswitch_'+str(i), plot=False)
    Switch_FilterPlate_80_cont.append(cont)

#Contact between shell and filter plate
c = contact.contactCRing(d_80fp/2., d_80shell/2.-t_80shell, np.array([0.,0.]))
Shell_Plate_80_cont = c.contact(T_80FilterPlate, ne, '80Kshell_', plot=False)
'''
40K Stage Preprocessing
'''
#Meshing filter Plate
filterplate = mesh.holedplate(d_40fp/2., t_40fp, opt_num, rad_fil-t_4ott, pos_fil)
T_40FilterPlate = filterplate.meshing(nx, ny, '40K_FilterPlate')*290.

#Meshing bottom Plate
bottomplate = mesh.plate(d_40bp/2.)
T_40BottomPlate = bottomplate.meshing(nx, ny, '40K_BottomPlate')*290.

#Contact between straps and filter plate
Strap_FilterPlate_40_cont = []
for i in range(numbers_mc[1]):
    c = contact.contactCRod(d_40fp/2., r_40hs[i], pos_40hs[i])
    cont = c.contact(T_40FilterPlate, '40Kstrap_'+str(i), plot=True)
    Strap_FilterPlate_40_cont.append(cont)


#Contact between filter plate and filters
Opt_FilterPlate_40_cont = []
for i in range(opt_num):
    c = contact.contactCRingInt(d_40fp/2., rad_fil[i], rad_fil[i]-t_4ott, pos_fil[i])
    cont = c.contact(T_40FilterPlate, ne, '40Kfilters_'+str(i), plot=False)
    Opt_FilterPlate_40_cont.append(cont)

SwitchCold_FilterPlate_40_cont = []
for i in range(len(rad_1)):
    c = contact.contactCRod(d_80fp/2., rad_1[i], pos40_1[i])
    cont = c.contact(T_40FilterPlate, '40Kswitchcold_'+str(i), plot=False)
    SwitchCold_FilterPlate_40_cont.append(cont)

SwitchHot_FilterPlate_40_cont = []
for i in range(len(rad_2)):
    c = contact.contactCRod(d_40fp/2., rad_2[i], pos40_2[i])
    cont = c.contact(T_40FilterPlate, '40Kswitchhot_'+str(i), plot=False)
    SwitchHot_FilterPlate_40_cont.append(cont)

#Contact between shell and filter plate
c = contact.contactCRing(d_40fp/2., d_40shell/2.-t_40shell, np.array([0.,0.]))
Shell_FilterPlate_40_cont = c.contact(T_40FilterPlate, ne, '40Kshellfilter', plot=False)

#Contact between shell and bottom plate
c = contact.contactCRing(d_40bp/2., d_40shell/2.-t_40shell, np.array([0.,0.]))
Shell_BottomPlate_40_cont = c.contact(T_40BottomPlate, ne, '40Kshellbottom', plot=False)

'''
4K Stage Preprocessing
'''
#Meshing filter Plate
filterplate = mesh.holedplate(d_4fp/2., t_4fp, opt_num, d_4ott/2.-t_4ott, pos_fil)
T_4FilterPlate = filterplate.meshing(nx, ny, '4K_FilterPlate')*290.

#Meshing bottom Plate
bottomplate = mesh.plate(d_4bp/2.)
T_4BottomPlate = bottomplate.meshing(nx, ny, '4K_BottomPlate')*290.

#Contact between straps and filter plate
Strap_FilterPlate_4_cont = []
for i in range(numbers_mc[1]):
    c = contact.contactCRod(d_4fp/2., r_4hs[i], pos_4hs[i])
    cont = c.contact(T_4FilterPlate, '4Kstrap_'+str(i), plot=False)
    Strap_FilterPlate_4_cont.append(cont)

Switch_FilterPlate_4_cont = []
for i in range(len(rad_2)):
    c = contact.contactCRod(d_4fp/2., rad_2[i], pos4_2[i])
    cont = c.contact(T_4FilterPlate, '4Kswitch_'+str(i), plot=False)
    Switch_FilterPlate_4_cont.append(cont)

#Contact between shell and filter plate
c = contact.contactCRing(d_4fp/2., d_4shell/2.-t_4shell, np.array([0.,0.]))
Shell_FilterPlate_4_cont = c.contact(T_4FilterPlate, ne, '4Kshellfilter', plot=False)

#Contact between shell and bottom plate
c = contact.contactCRing(d_4bp/2., d_4shell/2.-t_4shell, np.array([0.,0.]))
Shell_BottomPlate_4_cont = c.contact(T_4BottomPlate, ne, '4Kshellbottom', plot=False)

#Contact between filter plate and optics tubes (this contact is valid for superior
#and inferior optics tubes and for magnetic shields)
Opt_FilterPlate_4_cont = []
for i in range(opt_num):
    c = contact.contactCRingInt(d_4fp/2., d_4ott/2., d_4ott/2.-t_4ott, pos_fil[i])
    cont = c.contact(T_4FilterPlate, ne, '40Kot_'+str(i), plot=False)
    Opt_FilterPlate_4_cont.append(cont)

'''
1K Stage Preprocessing
'''
#Meshing Plate
filterplate = mesh.holedplate(d_1p/2., t_1p, opt_num, d_1ott/2.-t_1ott, pos_fil)
T_1Plate = filterplate.meshing(nx, ny, '1K_Plate')*290.

#Meshing bottom Plate
bottomplate = mesh.plate(d_1bp/2.)
T_1BottomPlate = bottomplate.meshing(nx, ny, '1K_BottomPlate')*290.

#Meshing DR Plate
drplate = mesh.plate(d_1drp/2.)
T_1drPlate = bottomplate.meshing(nxdr, nydr, '1K_DRPlate')*290.

#Contact between straps and  plate
Strap_Plate_1_cont = []
for i in range(4):
    c = contact.contactCRod(d_1p/2., r_1hs[i], pos_1hs[i])
    cont = c.contact(T_1Plate, '1Kstrap_'+str(i), plot=False)
    Strap_Plate_1_cont.append(cont)

#Contact between straps and  dr plate
Strap_drPlate_1_cont=[]
for i in range(4):
    c = contact.contactCRod(d_1drp/2., r_1hs[i], pos1_drhs[i])
    cont = c.contact(T_1drPlate, '1KDRstrap_'+str(i), plot=False)
    Strap_drPlate_1_cont.append(cont)

#Contact between switch and DRplate
Switch_drPlate_14_cont = []
c = contact.contactCRod(d_1drp/2., rad_1dr, pos1_dr)
cont = c.contact(T_1drPlate, '1-4Kswitchdr1_'+str(i), plot=False)
Switch_drPlate_14_cont.append(cont)

#Contact between switch and DRplate
Switch_drPlate_1100_cont = []
c = contact.contactCRod(d_1drp/2., rad_100dr, pos1100_dr)
cont = c.contact(T_1drPlate, '1-0,1Kswitchdr1_'+str(i), plot=False)
Switch_drPlate_1100_cont.append(cont)

#Contact between shell and  plate
c = contact.contactCRing(d_1p/2., d_1shell/2.-t_1shell, np.array([0.,0.]))
Shell_Plate_1_cont = c.contact(T_1Plate, ne, '1Kshellfilter', plot=False)

#Contact between shell and bottom plate
c = contact.contactCRing(d_1bp/2., d_1shell/2.-t_1shell, np.array([0.,0.]))
Shell_BottomPlate_1_cont = c.contact(T_1BottomPlate, ne, '1Kshellbottom', plot=False)

#Contact between filter plate and optics tubes (this contact is valid for superior
#and inferior optics tubes and for magnetic shields)
Opt_Plate_1_cont = []
for i in range(opt_num):
    c = contact.contactCRingInt(d_1p/2., d_1ott/2., d_1ott/2.-t_1ott, pos_fil[i])
    cont = c.contact(T_1Plate, ne, '1Kot_'+str(i), plot=False)
    Opt_Plate_1_cont.append(cont)

'''
100mK Stage Preprocessing
'''
#Meshing Plate
filterplate = mesh.holedplate(d_100p/2., t_100p, opt_num, d_100ott/2.-t_100ott, pos_fil)
T_100Plate = filterplate.meshing(nx, ny, '100mK_Plate')*290.

#Meshing bottom Plate
bottomplate = mesh.plate(d_100bp/2.)
T_100BottomPlate = bottomplate.meshing(nx, ny, '100mK_BottomPlate')*290.

#Meshing DR Plate
drplate = mesh.plate(d_100drp/2.)
T_100drPlate = bottomplate.meshing(nxdr, nydr, '100mK_DRPlate')*290.

#Contact between straps and  plate
Strap_Plate_100_cont = []
for i in range(4):
    c = contact.contactCRod(d_100p/2., r_100hs[i], pos_100hs[i])
    cont = c.contact(T_100Plate, '100mKstrap_'+str(i), plot=False)
    Strap_Plate_100_cont.append(cont)

#Contact between straps and  dr plate
Strap_drPlate_100_cont = []
for i in range(4):
    c = contact.contactCRod(d_100drp/2., r_100hs[i], pos100_drhs[i])
    cont = c.contact(T_100drPlate, '100mKDRstrap_'+str(i), plot=False)
    Strap_drPlate_100_cont.append(cont)

#Contact between switch and DRplate
Switch_drPlate_100_cont = []
c = contact.contactCRod(d_100drp/2., rad_100dr, pos100_dr)
cont = c.contact(T_100drPlate, '100mKswitchdr1_'+str(i), plot=False)
Switch_drPlate_100_cont.append(cont)

#Contact between shell and  plate
c = contact.contactCRing(d_100p/2., d_100shell/2.-t_100shell, np.array([0.,0.]))
Shell_Plate_100_cont = c.contact(T_100Plate, ne, '100mKshellfilter', plot=False)

#Contact between shell and bottom plate
c = contact.contactCRing(d_100bp/2., d_100shell/2.-t_100shell, np.array([0.,0.]))
Shell_BottomPlate_100_cont = c.contact(T_100BottomPlate, ne, '100mKshellbottom', plot=False)

#Contact between filter plate and optics tubes (this contact is valid for superior
#and inferior optics tubes and for magnetic shields)
Opt_Plate_100_cont = []
for i in range(opt_num):
    c = contact.contactCRingInt(d_100p/2., d_100ott/2., d_100ott/2.-t_100ott, pos_fil[i])
    cont = c.contact(T_100Plate, ne, '100mKot_'+str(i), plot=False)
    Opt_Plate_100_cont.append(cont)

'''
4K DR Stage Preprocessing
'''

#Meshing bottom Plate
bottomplate = mesh.plate(d_4drp/2.)
T_4drPlate = bottomplate.meshing(nxdr, nydr, '4K_DRPlate')*290.

#Contact with switch
Switch_drPlate_41_cont = []
c = contact.contactCRod(d_4drp/2., rad_1dr, pos41_dr)
cont = c.contact(T_4drPlate, '4Kswitchdr1_'+str(i), plot=False)
Switch_drPlate_41_cont.append(cont)

#Contact with the pulse tube
c = contact.contactCRod(d_4drp/2., rad_4drpPTC, pos_4drp)
PTC_drPlate_41_cont = c.contact(T_4drPlate, '4KPTCdr1_'+str(i), plot=False)


outdir = 'stacks/'+conf_name+'/'
'''
Ray-Tracing
'''
if os.path.isfile(outdir+'Ray_Tracing.txt') == True and os.path.isfile(outdir+'Ray_Tracing_shape.txt') == True:
    print 'Ray Tracing has been already done (Check config file for info)'
else:
    call(['mpirun', '-np', '4', 'python', 'src/cryolib/Ray_Tracing.py', filtersfile])

if os.path.isfile(outdir+'final/40K_Shell_f.txt') == True:

    time1 = np.loadtxt(outdir+'time.txt')
    tf = time1[-1]

    if os.path.isfile(outdir+'final/80K_Shell_f.txt') == True:
        T_80shell = np.loadtxt(outdir+'final/80K_Shell_f.txt')
        T_80strap = np.loadtxt(outdir+'final/80K_HS_f.txt')
        T_80FilterPlate = np.loadtxt(outdir+'final/80K_plate_f.txt')

    T_40shell = np.loadtxt(outdir+'final/40K_Shell_f.txt')
    T_40strap = np.loadtxt(outdir+'final/40K_HS_f.txt')
    T_40FilterPlate = np.loadtxt(outdir+'final/40K_plate_f.txt')
    T_40BottomPlate = np.loadtxt(outdir+'final/40K_bottom_f.txt')

    T_4FilterPlate = np.loadtxt(outdir+'final/4K_plate_f.txt')
    T_4BottomPlate = np.loadtxt(outdir+'final/4K_bottom.txt')
    T_4strap = np.loadtxt(outdir+'final/4K_HS.txt')
    T_4shell = np.loadtxt(outdir+'final/4K_shell.txt')

    T_1Plate = np.loadtxt(outdir+'final/1K_plate_f.txt')
    T_1BottomPlate = np.loadtxt(outdir+'final/1K_bottom.txt')
    T_1strap = np.loadtxt(outdir+'final/1K_HS.txt')
    T_1shell = np.loadtxt(outdir+'final/1K_Shell.txt')
    T_1drPlate = np.loadtxt(outdir+'final/1K_drPlate.txt')

    T_100Plate = np.loadtxt(outdir+'final/100mK_plate_f.txt')
    T_100BottomPlate = np.loadtxt(outdir+'final/100mK_bottom.txt')
    T_100strap = np.loadtxt(outdir+'final/100mK_HS.txt')
    T_100shell = np.loadtxt(outdir+'final/100mK_Shell.txt')
    T_100drPlate = np.loadtxt(outdir+'final/100mK_drPlate.txt')

    for y in range(opt_num):
        T4ott_file = outdir+'final/4K_opt_tube_top_f'+str(y)+'.txt'
        T4otb_file = outdir+'final/4K_opt_tube_bottom_f'+str(y)+'.txt'
        T_ot_top[y,:,:] = np.loadtxt(T4ott_file)
        T_ot_bottom[y,:,:] = np.loadtxt(T4otb_file)
        T1ott_file = outdir+'final/1K_opt_tube_top_'+str(y)+'.txt'
        T1otb_file = outdir+'final/1K_opt_tube_bottom_'+str(y)+'.txt'
        T_ot_top1[y,:,:] = np.loadtxt(T1ott_file)
        T_ot_bottom1[y,:,:] = np.loadtxt(T1otb_file)
        T100ott_file = outdir+'final/100mK_opt_tube_top_'+str(y)+'.txt'
        T100otb_file = outdir+'final/100mK_opt_tube_bottom_'+str(y)+'.txt'
        T_ot_top100[y,:,:] = np.loadtxt(T100ott_file)
        T_ot_bottom100[y,:,:] = np.loadtxt(T100otb_file)


ratio = np.ones(13)

mechanicalload = np.zeros(stage_num)
radiationload = np.zeros(stage_num)
filtersload = np.zeros((opt_num, stage_num))
wiringload = np.zeros(stage_num)
optload_top = np.zeros((3, opt_num))
optload_bottom = np.zeros((3, opt_num))
t1 = time.time()

Topt_top = np.zeros((3, opt_num))
Topt_bottom = np.zeros((3,opt_num))

TmaxS = np.array([])
TmaxP = np.array([])

while np.any(np.greater(ratio, 1e-11)):

    if tf >= 200 and (t[-1]/3600.)%4 == 0:
        T0 = 290.
        for i in range(opt_num):
            T80_fil = 0.
            T40_fil = 0.
            for j in range(ne):
                T80_fil += np.mean(T_80FilterPlate[Opt_FilterPlate_80_cont[i][j]])
                T40_fil += np.mean(T_40FilterPlate[Opt_FilterPlate_40_cont[i][j]])
            T80_fil = T80_fil/ne
            T40_fil = T40_fil/ne
            T4_fil = np.mean(T_ot_top[i,-1,:])
            T1_fil = np.mean(T_ot_top1[i,-1,:])
            T100_fil = np.mean(T_ot_top100[i,-1,:])
            call(['mpirun', '-np', '4', 'python', 'src/cryolib/Thermalization.py', \
            filtersfile, str(80), str(T0), str(T80_fil), \
            str(T40_fil), str(T4_fil), str(T1_fil), str(T100_fil), str(i)])
            T1 = (np.mean(T_80shell)+\
                  np.mean(T_80FilterPlate[np.nonzero(T_80FilterPlate)]))/2.
            T2 = (np.mean(T_40shell)+\
                 np.mean(T_40FilterPlate[np.nonzero(T_40FilterPlate)])+\
                 np.mean(T_40BottomPlate[np.nonzero(T_40BottomPlate)]))/3.
            T3 = (np.mean(T_4shell)+\
                 np.mean(T_4FilterPlate[np.nonzero(T_4FilterPlate)])+\
                 np.mean(T_4BottomPlate[np.nonzero(T_4BottomPlate)])+\
                 np.mean(T_ot_top)+np.mean(T_ot_bottom))/5.
            T4 = (np.mean(T_1shell)+\
                 np.mean(T_1Plate[np.nonzero(T_1Plate)])+\
                 np.mean(T_1BottomPlate[np.nonzero(T_1BottomPlate)])+\
                 np.mean(T_ot_top1)+np.mean(T_ot_bottom1))/5.
            T5 = (np.mean(T_100shell)+\
                 np.mean(T_100Plate[np.nonzero(T_100Plate)])+\
                 np.mean(T_100BottomPlate[np.nonzero(T_100BottomPlate)])+\
                 np.mean(T_ot_top100)+np.mean(T_ot_bottom100))/5.
            TLoad = np.array([T1, T2, T3, T4, T5])
            Topt_top[0,i] = np.mean(T_ot_top)
            Topt_top[1,i] = np.mean(T_ot_top1)
            Topt_top[2,i] = np.mean(T_ot_top100)
            Topt_bottom[0,i] = np.mean(T_ot_bottom)
            Topt_bottom[1,i] = np.mean(T_ot_bottom1)
            Topt_bottom[2,i] = np.mean(T_ot_bottom100)

        mechanicalload = LS.Loading(TLoad, loadingfile)[0]
        wiringload = LS.Loading(TLoad, loadingfile)[1]
        radiationload = LS.Loading(TLoad, loadingfile)[2]
        filtersload = LS.Loading(TLoad, loadingfile)[3]

        mat = []
        mat.append(m_4ott.strip())
        mat.append(m_1ott.strip())
        mat.append(m_100ott.strip())

        rad = np.append(d_4ott/2., np.append(d_1ott/2., d_100ott/2.))
        length = np.append(l_4ott,np.append(l_1ott, l_100ott))
        optload_top = LS.OPload(opt_num, rad, Topt_top, length, mat)

        mat = []
        mat.append(m_4otb.strip())
        mat.append(m_1otb.strip())
        mat.append(m_100otb.strip())

        rad = np.append(d_4otb/2., np.append(d_1otb/2., d_100otb/2.))
        length = np.append(l_4otb,np.append(l_1otb, l_100otb))
        optload_bottom = LS.OPload(opt_num, rad, Topt_bottom, length, mat)

    filter80 = filtersload[:,0]
    filter40 = filtersload[:,1]
    filter4 = filtersload[:,2]
    filter1 = filtersload[:,3]
    filter100 = filtersload[:,3]
    radiation40 = radiationload[0]
    radiation80 = radiationload[1]
    radiation4 = radiationload[2]
    radiation1 = radiationload[3]
    radiation100 = radiationload[4]
    mechanical80 = mechanicalload[0]+wiringload[0]
    mechanical40 = mechanicalload[1]+wiringload[1]
    mechanical4 = mechanicalload[2]+wiringload[2]
    mechanical1 = mechanicalload[3]+wiringload[3]
    mechanical100 = mechanicalload[4]+wiringload[4]

    for i in range(stage_num):

        if stage_n[i+1] == 80.:

            print '80K Building Matrix'

            nx = np.shape(T_80FilterPlate)[1]
            ny = np.shape(T_80FilterPlate)[0]

            dx = d_80fp/(nx-3)
            dy = d_80fp/(ny-3)

            m1 = matrixbuilder.strap(T_80strap, m_80hs, l_80hs, dt)
            strapmatrix80 = m1.matrix(plate=True, Tplate=T_80FilterPlate, platematerial=m_80fp, dx=dx, \
                       dy=dy, contactplate=Strap_FilterPlate_80_cont, shell=True, Tshell=T_80shell)

            Tswitch40cold = np.zeros(len(rad_1))
            for k in range(len(rad_1)):
                Tswitch40cold[k] = np.mean(T_40FilterPlate[SwitchCold_FilterPlate_40_cont[k]])

            m2 = matrixbuilder.filterplate(T_80FilterPlate, m_80fp, d_80fp, t_80fp, dx, dy, dt)
            platematrix80 = m2.matrix(strap=True, Tstrap=T_80strap, strapmaterial=m_80hs, \
                       contactstrap=Strap_FilterPlate_80_cont, shell=True, Tshell=T_80shell, \
                       shellmaterial=m_80shell, contactshell=Shell_Plate_80_cont, switch=True, \
                       Tswitch=Tswitch40cold, contactswitch=Switch_FilterPlate_80_cont, condswitch=value_1)

            m3 = matrixbuilder.shell(T_80shell, m_80shell, dt, d_80shell/2., t_80shell, l_80shell)
            shellmatrix80 = m3.matrix(strap=True, Tstrap = T_80strap, plate=True, Tplate=T_80FilterPlate, \
                       contactplate=Shell_Plate_80_cont, platematerial=m_80fp, dx1=dx, dy1=dy)

        if stage_n[i+1] == 40.:

            print '40K Building Matrix'

            nx = np.shape(T_40FilterPlate)[1]
            ny = np.shape(T_40FilterPlate)[0]

            dx1 = d_40fp/(nx-3)
            dy1 = d_40fp/(ny-3)

            nx = np.shape(T_40BottomPlate)[1]
            ny = np.shape(T_40BottomPlate)[0]

            dx2 = d_40bp/(nx-3)
            dy2 = d_40bp/(ny-3)

            m1 = matrixbuilder.strap(T_40strap, m_40hs, l_40hs, dt)
            strapmatrix40 = m1.matrix(plate=True, Tplate=T_40FilterPlate, platematerial=m_40fp, dx=dx1, \
                       dy=dy1, contactplate=Strap_FilterPlate_40_cont, shell=True, Tshell=T_40shell, \
                       bottom=True, Tbottom = T_40BottomPlate)

            Tswitch80 = np.zeros(len(rad_1))
            for k in range(len(rad_1)):
                Tswitch80[k] = np.mean(T_80FilterPlate[Switch_FilterPlate_80_cont[k]])

            Tswitch4 = np.zeros(len(rad_2))
            for k in range(len(rad_2)):
                Tswitch4[k] = np.mean(T_4FilterPlate[Switch_FilterPlate_4_cont[k]])

            Tswitch = np.append(Tswitch80, Tswitch4)
            switchcont = list(SwitchCold_FilterPlate_40_cont)
            value = list(value_1)
            for j in range(len(value_2)):
                switchcont.append(SwitchHot_FilterPlate_40_cont[j])
                value.append(value_2[j])

            m2 = matrixbuilder.filterplate(T_40FilterPlate, m_40fp, d_40fp, t_40fp, dx1, dy1, dt)
            platematrix40 = m2.matrix(strap=True, Tstrap=T_40strap, strapmaterial=m_40hs, \
                       contactstrap=Strap_FilterPlate_40_cont,shell=True, Tshell=T_40shell, \
                       shellmaterial=m_40shell, contactshell=Shell_FilterPlate_40_cont,\
                       bottom=True, Tbottom = T_40BottomPlate, switch=True, \
                       Tswitch=Tswitch, contactswitch=switchcont, condswitch=value)

            m3 = matrixbuilder.shell(T_40shell, m_40shell, dt, d_40shell/2., t_40shell, l_40shell)
            shellmatrix40 = m3.matrix(strap=True, Tstrap = T_40strap, plate=True, Tplate=T_40FilterPlate, \
                       contactplate=Shell_FilterPlate_40_cont, platematerial=m_40fp, dx1=dx1, dy1=dy1,\
                       bottom=True, Tbottom = T_40BottomPlate, contactbottom=Shell_BottomPlate_40_cont, \
                       bottommaterial=m_40bp, dx2=dx2, dy2=dy2)

            m4 = matrixbuilder.bottomplate(T_40BottomPlate, m_40bp, d_40bp/2., t_40bp, dx2, dy2, dt)
            bottommatrix40 = m4.matrix(strap=True, Tstrap=T_40strap, plate=True, Tplate=T_40FilterPlate, \
                       shell=True, Tshell=T_40shell, shellmaterial=m_40shell, contactshell=Shell_BottomPlate_40_cont)

        if stage_n[i+1] == 4.:

            print '4K Building Matrix'

            nx = np.shape(T_4FilterPlate)[1]
            ny = np.shape(T_4FilterPlate)[0]

            dx1 = d_4fp/(nx-3)
            dy1 = d_4fp/(ny-3)

            nx = np.shape(T_4BottomPlate)[1]
            ny = np.shape(T_4BottomPlate)[0]

            dx2 = d_4bp/(nx-3)
            dy2 = d_4bp/(ny-3)

            m1 = matrixbuilder.strap(T_4strap, m_4hs, l_4hs, dt)
            strapmatrix4 = m1.matrix(plate=True, Tplate=T_4FilterPlate, platematerial=m_4fp, dx=dx1, \
                       dy=dy1, contactplate=Strap_FilterPlate_4_cont, shell=True, Tshell=T_4shell, \
                       bottom=True, Tbottom=T_4BottomPlate, OTT=True, Tott=T_ot_top, OTB=True, Totb=T_ot_bottom)

            Tswitch40hot = np.zeros(len(rad_2))
            for k in range(len(rad_2)):
                Tswitch40hot[k] = np.mean(T_40FilterPlate[SwitchHot_FilterPlate_40_cont[k]])

            m2 = matrixbuilder.filterplate(T_4FilterPlate, m_4fp, d_4fp, t_4fp, dx1, dy1, dt)
            platematrix4 = m2.matrix(strap=True, Tstrap=T_4strap, strapmaterial=m_4hs, \
                       contactstrap=Strap_FilterPlate_4_cont,shell=True, Tshell=T_4shell, \
                       shellmaterial=m_4shell, contactshell=Shell_FilterPlate_4_cont,\
                       bottom=True, Tbottom = T_4BottomPlate, OTT=True, Tott=T_ot_top, ottmaterial=m_4ott,\
                       OTB=True, Totb=T_ot_bottom, \
                       otbmaterial=m_4otb, contactot=Opt_FilterPlate_4_cont,
                       switch=True,\
                       Tswitch=Tswitch40hot, contactswitch=Switch_FilterPlate_4_cont, \
                       condswitch=value_2)

            m3 = matrixbuilder.shell(T_40shell, m_4shell, dt, d_4shell/2., t_4shell, l_4shell)
            shellmatrix4 = m3.matrix(strap=True, Tstrap = T_4strap, plate=True, Tplate=T_4FilterPlate, \
                       contactplate=Shell_FilterPlate_4_cont, platematerial=m_4fp, dx1=dx1, dy1=dy1,\
                       bottom=True, Tbottom = T_4BottomPlate, contactbottom=Shell_BottomPlate_4_cont, \
                       bottommaterial=m_4bp, dx2=dx2, dy2=dy2, OTT=True, Tott=T_ot_top, OTB=True, Totb=T_ot_bottom)

            m4 = matrixbuilder.bottomplate(T_4BottomPlate, m_4bp, d_4bp/2., t_4bp, dx2, dy2, dt)
            bottommatrix4 = m4.matrix(strap=True, Tstrap=T_4strap, plate=True, Tplate=T_4FilterPlate, \
                       shell=True, Tshell=T_4shell, shellmaterial=m_4shell, contactshell=Shell_BottomPlate_4_cont, \
                       OTT=True, Tott=T_ot_top, OTB=True, Totb=T_ot_bottom)

            m5 = matrixbuilder.opticstube(T_ot_top, d_4ott/2., opt_num, m_4ott, l_4ott, \
                         t_4ott, dt, 'top')
            top_optmatrix4 = m5.matrix(straps=True, Tstrap=T_4strap, plate=True, Tplate=T_4FilterPlate, \
                       contactplate=Opt_FilterPlate_4_cont, platematerial=m_4fp, shell=True, Tshell=T_4shell,\
                       dx=dx1, dy=dy1, \
                       bottom=True, Tbottom=T_4BottomPlate)

            m6 = matrixbuilder.opticstube(T_ot_bottom, d_4otb/2., opt_num, m_4otb, l_4otb, \
                         t_4otb, dt, 'bottom')
            bottom_optmatrix4 = m6.matrix(straps=True, Tstrap=T_4strap, plate=True, Tplate=T_4FilterPlate, \
                       contactplate=Opt_FilterPlate_4_cont, platematerial=m_4fp, shell=True, Tshell=T_4shell,\
                       dx=dx1, dy=dy1, bottom=True, Tbottom=T_4BottomPlate)

        if stage_n[i+1] == 1.:

            print '1K Building Matrix'

            nx = np.shape(T_1Plate)[1]
            ny = np.shape(T_1Plate)[0]

            dx1 = d_1p/(nx-3)
            dy1 = d_1p/(ny-3)

            nx = np.shape(T_1BottomPlate)[1]
            ny = np.shape(T_1BottomPlate)[0]

            dx2 = d_1bp/(nx-3)
            dy2 = d_1bp/(ny-3)

            nx = np.shape(T_1drPlate)[1]
            ny = np.shape(T_1drPlate)[0]

            dxdr = d_1drp/(nx-3)
            dydr = d_1drp/(ny-3)


            m1 = matrixbuilder.strapDR(T_1strap, m_1hs, l_1hs, dt)
            strapmatrix1 = m1.matrix(plate=True, Tplate=T_1Plate, platematerial=m_1p, dxp=dx1, \
                       dyp=dy1, contactplate=Strap_Plate_1_cont, shell=True, Tshell=T_1shell, \
                       bottom=True, Tbottom=T_1BottomPlate, OTT=True, Tott=T_ot_top1, \
                       OTB=True, Totb=T_ot_bottom1, DRplate=True, TplateDR=T_1drPlate, \
                       platematerialDR=m_1drp, dxdr=dxdr, \
                       dydr=dydr, contactplateDR=Strap_drPlate_1_cont)

            #1K Copper Plate
            m2 = matrixbuilder.filterplate(T_1Plate, m_1p, d_1p, t_1p, dx1, dy1, dt)
            platematrix1 = m2.matrix(strap=True, Tstrap=T_1strap, strapmaterial=m_1hs, \
                       contactstrap=Strap_Plate_1_cont,shell=True, Tshell=T_1shell, \
                       shellmaterial=m_1shell, contactshell=Shell_Plate_1_cont,\
                       bottom=True, Tbottom = T_1BottomPlate, OTT=True, Tott=T_ot_top1, ottmaterial=m_1ott,\
                       OTB=True, Totb=T_ot_bottom1, \
                       otbmaterial=m_1otb, contactot=Opt_Plate_1_cont)

            matrix_drplate = sparse.lil_matrix((np.size(T_1Plate),np.size(T_1drPlate)))

            platematrix1 = sparse.hstack((platematrix1, matrix_drplate))

            m3 = matrixbuilder.shell(T_1shell, m_1shell, dt, d_1shell/2., t_1shell, l_1shell)
            shellmatrix1 = m3.matrix(strap=True, Tstrap = T_1strap, plate=True, Tplate=T_1Plate, \
                       contactplate=Shell_Plate_1_cont, platematerial=m_1p, dx1=dx1, dy1=dy1,\
                       bottom=True, Tbottom = T_1BottomPlate, contactbottom=Shell_BottomPlate_1_cont, \
                       bottommaterial=m_1bp, dx2=dx2, dy2=dy2, OTT=True, Tott=T_ot_top1, OTB=True, Totb=T_ot_bottom1)

            matrix_drplate = sparse.lil_matrix((np.size(T_1shell)+2*np.shape(T_1shell)[1],np.size(T_1drPlate)))

            shellmatrix1 = sparse.hstack((shellmatrix1, matrix_drplate))

            m4 = matrixbuilder.bottomplate(T_1BottomPlate, m_1bp, d_1bp/2., t_1bp, dx2, dy2, dt)
            bottommatrix1 = m4.matrix(strap=True, Tstrap=T_1strap, plate=True, Tplate=T_1Plate, \
                       shell=True, Tshell=T_1shell, shellmaterial=m_1shell, contactshell=Shell_BottomPlate_1_cont, \
                       OTT=True, Tott=T_ot_top1, OTB=True, Totb=T_ot_bottom1)

            matrix_drplate = sparse.lil_matrix((np.size(T_1BottomPlate),np.size(T_1drPlate)))

            bottommatrix1 = sparse.hstack((bottommatrix1, matrix_drplate))

            m5 = matrixbuilder.opticstube(T_ot_top1, d_1ott/2., opt_num, m_1ott, l_1ott, \
                         t_1ott, dt, 'top')
            top_optmatrix1 = m5.matrix(straps=True, Tstrap=T_1strap, plate=True, Tplate=T_1Plate, \
                       contactplate=Opt_Plate_1_cont, platematerial=m_1p, shell=True, Tshell=T_1shell,\
                       dx=dx1, dy=dy1, \
                       bottom=True, Tbottom=T_1BottomPlate)

            matrix_drplate = sparse.lil_matrix((np.size(T_ot_top1)+opt_num*2*\
                                    np.shape(T_ot_top1)[2],np.size(T_1drPlate)))

            top_optmatrix1 = sparse.hstack((top_optmatrix1, matrix_drplate))

            m6 = matrixbuilder.opticstube(T_ot_bottom1, d_1otb/2., opt_num, m_1otb, l_1otb, \
                         t_1otb, dt, 'bottom')
            bottom_optmatrix1 = m6.matrix(straps=True, Tstrap=T_1strap, plate=True, Tplate=T_1Plate, \
                       contactplate=Opt_Plate_1_cont, platematerial=m_1p, shell=True, Tshell=T_1shell,\
                       dx=dx1, dy=dy1, bottom=True, Tbottom=T_1BottomPlate)

            matrix_drplate = sparse.lil_matrix((np.size(T_ot_bottom1)+opt_num*2*\
                               np.shape(T_ot_bottom1)[2],np.size(T_1drPlate)))

            bottom_optmatrix1 = sparse.hstack((bottom_optmatrix1, matrix_drplate))

            Tswitch4 = np.mean(T_4drPlate[Switch_drPlate_41_cont[0]])

            Tswitch100 = np.mean(T_100drPlate[Switch_drPlate_100_cont[0]])

            Tswitch = np.append(Tswitch4, Tswitch100)
            switchcont = list(Switch_drPlate_14_cont)
            value = list(value_1dr)
            switchcont.append(Switch_drPlate_1100_cont[0])
            value.append(value_100dr[0])

            m7 = matrixbuilder.DRplate(T_1drPlate, m_1drp, d_1drp/2., t_1drp, dxdr, dydr, dt)
            drplatematrix1 = m7.matrix(strap=True, Tstrap=T_1strap, strapmaterial=m_1hs, \
                       contactstrap=Strap_drPlate_1_cont, plateCopp=True, \
                       TplateCopp=T_1Plate,shell=True, Tshell=T_1shell, \
                       bottom=True, Tbottom=T_1BottomPlate, OTT=True, \
                       Tott=T_ot_top1, OTB=True, Totb=T_ot_bottom1,
                       switch=True, Tswitch=Tswitch, contactswitch=switchcont, condswitch=value)

        if stage_n[i+1] == .1:

            print '0.1K Building Matrix'

            nx = np.shape(T_100Plate)[1]
            ny = np.shape(T_100Plate)[0]

            dx1 = d_100p/(nx-3)
            dy1 = d_100p/(ny-3)

            nx = np.shape(T_100BottomPlate)[1]
            ny = np.shape(T_100BottomPlate)[0]

            dx2 = d_100bp/(nx-3)
            dy2 = d_100bp/(ny-3)

            nx = np.shape(T_100drPlate)[1]
            ny = np.shape(T_100drPlate)[0]

            dxdr = d_100drp/(nx-3)
            dydr = d_100drp/(ny-3)


            m1 = matrixbuilder.strapDR(T_100strap, m_100hs, l_100hs, dt)
            strapmatrix100 = m1.matrix(plate=True, Tplate=T_100Plate, platematerial=m_100p, dxp=dx1, \
                       dyp=dy1, contactplate=Strap_Plate_100_cont, shell=True, Tshell=T_100shell, \
                       bottom=True, Tbottom=T_100BottomPlate, OTT=True, Tott=T_ot_top100, \
                       OTB=True, Totb=T_ot_bottom100, DRplate=True, TplateDR=T_100drPlate, \
                       platematerialDR=m_100drp, dxdr=dxdr, \
                       dydr=dydr, contactplateDR=Strap_drPlate_100_cont)

            #1K Copper Plate
            m2 = matrixbuilder.filterplate(T_100Plate, m_100p, d_100p, t_100p, dx1, dy1, dt)
            platematrix100 = m2.matrix(strap=True, Tstrap=T_100strap, strapmaterial=m_100hs, \
                       contactstrap=Strap_Plate_100_cont,shell=True, Tshell=T_100shell, \
                       shellmaterial=m_100shell, contactshell=Shell_Plate_100_cont,\
                       bottom=True, Tbottom = T_100BottomPlate, OTT=True, Tott=T_ot_top100, ottmaterial=m_100ott,\
                       OTB=True, Totb=T_ot_bottom100, \
                       otbmaterial=m_100otb, contactot=Opt_Plate_100_cont)

            matrix_drplate = sparse.lil_matrix((np.size(T_100Plate),np.size(T_100drPlate)))

            platematrix100 = sparse.hstack((platematrix100, matrix_drplate))

            m3 = matrixbuilder.shell(T_100shell, m_100shell, dt, d_100shell/2., t_100shell, l_100shell)
            shellmatrix100 = m3.matrix(strap=True, Tstrap = T_100strap, plate=True, Tplate=T_100Plate, \
                       contactplate=Shell_Plate_100_cont, platematerial=m_100p, dx1=dx1, dy1=dy1,\
                       bottom=True, Tbottom = T_100BottomPlate, contactbottom=Shell_BottomPlate_100_cont, \
                       bottommaterial=m_100bp, dx2=dx2, dy2=dy2, OTT=True, Tott=T_ot_top100, OTB=True, Totb=T_ot_bottom100)

            matrix_drplate = sparse.lil_matrix((np.size(T_100shell)+2*np.shape(T_100shell)[1],np.size(T_100drPlate)))

            shellmatrix100 = sparse.hstack((shellmatrix100, matrix_drplate))

            m4 = matrixbuilder.bottomplate(T_100BottomPlate, m_100bp, d_100bp/2., t_100bp, dx2, dy2, dt)
            bottommatrix100 = m4.matrix(strap=True, Tstrap=T_100strap, plate=True, Tplate=T_100Plate, \
                       shell=True, Tshell=T_100shell, shellmaterial=m_100shell, contactshell=Shell_BottomPlate_100_cont, \
                       OTT=True, Tott=T_ot_top100, OTB=True, Totb=T_ot_bottom100)

            matrix_drplate = sparse.lil_matrix((np.size(T_100BottomPlate),np.size(T_100drPlate)))

            bottommatrix100 = sparse.hstack((bottommatrix100, matrix_drplate))

            m5 = matrixbuilder.opticstube(T_ot_top100, d_100ott/2., opt_num, m_100ott, l_100ott, \
                         t_100ott, dt, 'top')
            top_optmatrix100 = m5.matrix(straps=True, Tstrap=T_100strap, plate=True, Tplate=T_100Plate, \
                       contactplate=Opt_Plate_100_cont, platematerial=m_100p, shell=True, Tshell=T_100shell,\
                       dx=dx1, dy=dy1, \
                       bottom=True, Tbottom=T_100BottomPlate)

            matrix_drplate = sparse.lil_matrix((np.size(T_ot_top100)+opt_num*2*\
                                    np.shape(T_ot_top100)[2],np.size(T_100drPlate)))

            top_optmatrix100 = sparse.hstack((top_optmatrix100, matrix_drplate))

            m6 = matrixbuilder.opticstube(T_ot_bottom100, d_100otb/2., opt_num, m_100otb, l_100otb, \
                         t_100otb, dt, 'bottom')
            bottom_optmatrix100 = m6.matrix(straps=True, Tstrap=T_100strap, plate=True, Tplate=T_100Plate, \
                       contactplate=Opt_Plate_100_cont, platematerial=m_100p, shell=True, Tshell=T_100shell,\
                       dx=dx1, dy=dy1, bottom=True, Tbottom=T_100BottomPlate)

            matrix_drplate = sparse.lil_matrix((np.size(T_ot_bottom100)+opt_num*2*\
                               np.shape(T_ot_bottom100)[2],np.size(T_100drPlate)))

            bottom_optmatrix100 = sparse.hstack((bottom_optmatrix100, matrix_drplate))

            Tswitch1 = np.mean(T_1drPlate[Switch_drPlate_1100_cont[0]])

            m7 = matrixbuilder.DRplate(T_100drPlate, m_100drp, d_100drp/2., t_100drp, dxdr, dydr, dt)
            drplatematrix100 = m7.matrix(strap=True, Tstrap=T_100strap, strapmaterial=m_100hs, \
                       contactstrap=Strap_drPlate_100_cont, plateCopp=True, \
                       TplateCopp=T_100Plate,shell=True, Tshell=T_100shell, \
                       bottom=True, Tbottom=T_100BottomPlate, OTT=True, \
                       Tott=T_ot_top100, OTB=True, Totb=T_ot_bottom100,
                       switch=True, Tswitch=Tswitch1, contactswitch=Switch_drPlate_100_cont,\
                       condswitch=value_100dr)


    print '4K DR Plate'
    nx = np.shape(T_4drPlate)[1]
    ny = np.shape(T_4drPlate)[0]

    dxdr = d_4drp/(nx-3)
    dydr = d_4drp/(ny-3)

    Tswitch1 = np.mean(T_1drPlate[Switch_drPlate_14_cont[0]])

    m1 = matrixbuilder.DRplate(T_4drPlate, m_4drp, d_4drp/2., t_4drp, dxdr, dydr, dt)
    mat4dr = m1.matrix(switch=True, Tswitch=Tswitch1,\
                                 contactswitch=Switch_drPlate_41_cont,\
                                 condswitch=value_1dr)

    #Building 80K Matrix
    mat80k = sparse.vstack((strapmatrix80, platematrix80))
    mat80k = sparse.vstack((mat80k, shellmatrix80))

    #Building 40K Matrix
    mat40k = sparse.vstack((strapmatrix40, platematrix40))
    mat40k = sparse.vstack((mat40k, shellmatrix40))
    mat40k = sparse.vstack((mat40k, bottommatrix40))

    #Building 4K Matrix
    mat4k = sparse.vstack((strapmatrix4, platematrix4))
    mat4k = sparse.vstack((mat4k, shellmatrix4))
    mat4k = sparse.vstack((mat4k, bottommatrix4))
    mat4k = sparse.vstack((mat4k, top_optmatrix4))
    mat4k = sparse.vstack((mat4k, bottom_optmatrix4))

    #Building 1K Matrix
    mat1k = sparse.vstack((strapmatrix1, platematrix1))
    mat1k = sparse.vstack((mat1k, shellmatrix1))
    mat1k = sparse.vstack((mat1k, bottommatrix1))
    mat1k = sparse.vstack((mat1k, top_optmatrix1))
    mat1k = sparse.vstack((mat1k, bottom_optmatrix1))
    mat1k = sparse.vstack((mat1k, drplatematrix1))

    #Building 0.1K Matrix
    mat100mk = sparse.vstack((strapmatrix100, platematrix100))
    mat100mk = sparse.vstack((mat100mk, shellmatrix100))
    mat100mk = sparse.vstack((mat100mk, bottommatrix100))
    mat100mk = sparse.vstack((mat100mk, top_optmatrix100))
    mat100mk = sparse.vstack((mat100mk, bottom_optmatrix100))
    mat100mk = sparse.vstack((mat100mk, drplatematrix100))

    #Building Switches matrices

    nx = np.shape(T_80FilterPlate)[1]
    ny = np.shape(T_80FilterPlate)[0]

    dx1 = d_80fp/(nx-3)
    dy1 = d_80fp/(ny-3)

    nx = np.shape(T_40FilterPlate)[1]
    ny = np.shape(T_40FilterPlate)[0]

    dx2 = d_40fp/(nx-3)
    dy2 = d_40fp/(ny-3)

    sw1 = matrixbuilder.switch(len(rad_1), T_80FilterPlate, m_80fp, t_80fp, T_40FilterPlate, \
                 m_40fp, t_40fp, Switch_FilterPlate_80_cont, \
                 SwitchCold_FilterPlate_40_cont, dx1, dy1, dx2, dy2, value_1)

    matrix80switch, matrix40switchCold = sw1.matrix(dt)

    nx = np.shape(T_40FilterPlate)[1]
    ny = np.shape(T_40FilterPlate)[0]

    dx1 = d_40fp/(nx-3)
    dy1 = d_40fp/(ny-3)

    nx = np.shape(T_4FilterPlate)[1]
    ny = np.shape(T_4FilterPlate)[0]

    dx2 = d_4fp/(nx-3)
    dy2 = d_4fp/(ny-3)

    sw1 = matrixbuilder.switch(len(rad_2), T_40FilterPlate, m_40fp, t_40fp, T_4FilterPlate, \
                 m_4fp, t_4fp, SwitchHot_FilterPlate_40_cont, \
                 Switch_FilterPlate_4_cont, dx1, dy1, dx2, dy2, value_2)

    matrix40switchHot, matrix4switch = sw1.matrix(dt)

    mat80switch = sparse.vstack((sparse.lil_matrix((np.size(T_80strap),\
                                 np.shape(matrix80switch)[1])), matrix80switch))

    mat80switch = sparse.vstack((mat80switch,sparse.lil_matrix((np.size(T_80shell)+\
                                 2*(np.shape(T_80shell)[1]),\
                                 np.shape(matrix80switch)[1]))))

    mat80switch = sparse.hstack((sparse.lil_matrix((np.shape(mat80switch)[0],\
                                 np.size(T_40strap))), mat80switch))

    mat80switch = sparse.hstack((mat80switch, sparse.lil_matrix((np.shape(mat80switch)[0],\
                                 np.size(T_40shell)+2*np.shape(T_40shell)[1]+np.size(T_40BottomPlate)\
                                 ))))

    mat80switch = sparse.hstack((mat80switch, sparse.lil_matrix((np.shape(mat80switch)[0],\
                                 np.size(T_4strap)+np.size(T_4FilterPlate)+\
                                 np.size(T_4shell)+2*np.shape(T_4shell)[1]+\
                                 np.size(T_4BottomPlate)+np.size(T_ot_top)+\
                                 np.size(T_ot_bottom)+4*opt_num*np.shape(T_ot_top)[2]))\
                                 ))

    mat80 = sparse.hstack((mat80k, mat80switch))

    mat40switchCold = sparse.vstack((sparse.lil_matrix((np.size(T_40strap),\
                                    np.shape(matrix40switchCold)[1])), matrix40switchCold))

    mat40switchCold = sparse.vstack((mat40switchCold,sparse.lil_matrix((np.size(T_40shell)+\
                                 2*np.shape(T_40shell)[1]+\
                                 np.size(T_40BottomPlate),\
                                 np.shape(matrix40switchCold)[1]))))

    mat40switchCold = sparse.hstack((sparse.lil_matrix((np.shape(mat40switchCold)[0], \
                                np.size(T_80strap))),mat40switchCold))

    mat40switchCold = sparse.hstack((mat40switchCold, sparse.lil_matrix((np.shape(mat40switchCold)[0], \
                                 np.size(T_80shell)+2*np.shape(T_80shell)[1]))))

    mat40switchHot = sparse.vstack((sparse.lil_matrix((np.size(T_40strap),\
                                 np.shape(matrix40switchHot)[1])), matrix40switchHot))

    mat40switchHot = sparse.vstack((mat40switchHot,sparse.lil_matrix((np.size(T_40shell)+\
                                 2*np.shape(T_40shell)[1]+\
                                 np.size(T_40BottomPlate),\
                                 np.shape(matrix40switchHot)[1]))))

    mat40switchHot = sparse.hstack((sparse.lil_matrix((np.shape(mat40switchHot)[0], \
                                 np.size(T_4strap))),mat40switchHot))

    mat40switchHot = sparse.hstack((mat40switchHot, sparse.lil_matrix((np.shape(mat40switchHot)[0], \
                                 np.size(T_4shell)+2*np.shape(T_4shell)[1]+\
                                 np.size(T_4BottomPlate)+np.size(T_ot_top)+\
                                 np.size(T_ot_bottom)+4*opt_num*np.shape(T_ot_top)[2]))))

    mat40 = sparse.hstack((mat40switchCold, mat40k))

    mat40 = sparse.hstack((mat40, mat40switchHot))


    mat4switch = sparse.vstack((sparse.lil_matrix((np.size(T_4strap),\
                                 np.shape(matrix4switch)[1])), matrix4switch))

    mat4switch = sparse.vstack((mat4switch,sparse.lil_matrix((np.size(T_4shell)+\
                                 np.shape(T_4shell)[1]*2+\
                                 np.size(T_4BottomPlate)+np.size(T_ot_top)+\
                                 np.size(T_ot_bottom)+4*opt_num*np.shape(T_ot_top)[2],\
                                 np.shape(matrix4switch)[1]))))

    mat4switch = sparse.hstack((sparse.lil_matrix((np.shape(mat4switch)[0],\
                                 np.size(T_80strap)+np.size(T_80FilterPlate)+\
                                 np.size(T_80shell)+np.shape(T_80shell)[1]*2+\
                                 np.size(T_40strap))),\
                                 mat4switch))

    mat4switch = sparse.hstack((mat4switch, sparse.lil_matrix((np.shape(mat4switch)[0],\
                                 np.size(T_40shell)+2*np.shape(T_40shell)[1]+\
                                 np.size(T_40BottomPlate)))))

    mat4 = sparse.hstack((mat4switch, mat4k))


    final804 = sparse.vstack((mat80, mat40))
    final804 = sparse.vstack((final804, mat4))

    #Buiding 1K and 0.1K switch matrices

    nx = np.shape(T_1drPlate)[1]
    ny = np.shape(T_1drPlate)[0]

    dx1 = d_1drp/(nx-3)
    dy1 = d_1drp/(ny-3)

    nx = np.shape(T_100drPlate)[1]
    ny = np.shape(T_100drPlate)[0]

    dx2 = d_100drp/(nx-3)
    dy2 = d_100drp/(ny-3)

    nx = np.shape(T_4drPlate)[1]
    ny = np.shape(T_4drPlate)[0]

    dx3 = d_4drp/(nx-3)
    dy3 = d_4drp/(ny-3)

    sw1 = matrixbuilder.switch(1, T_1drPlate, m_1drp, t_1drp, T_100drPlate, \
                 m_100drp, t_100drp, Switch_drPlate_1100_cont, \
                 Switch_drPlate_100_cont, dx1, dy1, dx2, dy2, value_100dr)
    sw2 = matrixbuilder.switch(1, T_1drPlate, m_1drp, t_1drp, T_4drPlate, \
                m_4drp, t_4drp, Switch_drPlate_14_cont, \
                Switch_drPlate_41_cont, dx1, dy1, dx3, dy3, value_1dr)

    matrix1switchHot, matrix100switch = sw1.matrix(dt)
    matrix1switchCold, matrix4switchdr = sw2.matrix(dt)

    mat1switch100 = sparse.lil_matrix((np.shape(mat1k)[0]-np.size(T_1drPlate), np.shape(matrix1switchHot)[1]))
    mat1switch4 = sparse.lil_matrix((np.shape(mat1k)[0]-np.size(T_1drPlate),np.shape(matrix1switchCold)[1]))

    mat1switch4 = sparse.vstack((mat1switch4,matrix1switchCold))
    mat1switch100 = sparse.vstack((mat1switch100,matrix1switchHot))

    matsp = sparse.lil_matrix((np.shape(mat1k)[0], np.shape(mat100mk)[1]-np.size(T_1drPlate)))
    mat1switch100 = sparse.hstack((matsp, mat1switch100))

    #mat1switch4 = sparse.lil_matrix((np.shape(mat1k)[0],np.shape(mat4dr)[1]))

    mat1 = sparse.hstack((mat1k, mat1switch100))
    mat1 = sparse.hstack((mat1, mat1switch4))

    mat100switch1 = sparse.lil_matrix((np.shape(mat100mk)[0]-np.size(T_100drPlate), \
                                       np.shape(matrix100switch)[1]))
    mat100switch1 = sparse.vstack((mat100switch1, matrix100switch))

    matsp = sparse.lil_matrix((np.shape(mat100mk)[0], np.shape(mat1k)[1]-np.shape(matrix1switchHot)[1]))
    matsp1 = sparse.lil_matrix((np.shape(mat100mk)[0], np.shape(mat4dr)[1]))

    mat100 = sparse.hstack((matsp, mat100switch1))
    mat100 = sparse.hstack((mat100, mat100mk))
    mat100 = sparse.hstack((mat100, matsp1))

    matsp = sparse.lil_matrix((np.shape(mat4dr)[0], np.shape(mat100mk)[1]))

    mat4switchdr = sparse.lil_matrix((np.shape(mat4dr)[0], np.shape(mat1k)[1]-np.size(T_1drPlate)))
    mat4switchdr = sparse.hstack((mat4switchdr, matrix4switchdr))

    mat4drf = sparse.hstack((mat4switchdr, matsp))
    mat4drf = sparse.hstack((mat4drf, mat4dr))

    final1100 = sparse.vstack((mat1, mat100))
    final1100 = sparse.vstack((final1100, mat4drf))

    print 'Matrices Built'

    #Building Temperature Array
    #80K
    T80strap_s = np.reshape(T_80strap, np.size(T_80strap))

    index80_fp = np.where(T_80FilterPlate == 0)

    T80fp_s = np.reshape(T_80FilterPlate, np.size(T_80FilterPlate))
    T1 = T_80FilterPlate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T80fp_s1 = np.reshape(T1, np.size(T1))

    T80shell_s = np.reshape(T_80shell, np.size(T_80shell))
    T80shell_s = np.append(T_80shell[0,:], np.append(T80shell_s, \
                           T_80shell[-1,:]))

    T80_s = np.append(T80strap_s, np.append(T80fp_s1, T80shell_s))

    #40K
    T40strap_s = np.reshape(T_40strap, np.size(T_40strap))

    index40_fp = np.where(T_40FilterPlate == 0)

    T40fp_s = np.reshape(T_40FilterPlate, np.size(T_40FilterPlate))
    T1 = T_40FilterPlate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T40fp_s1 = np.reshape(T1, np.size(T1))

    T40shell_s = np.reshape(T_40shell, np.size(T_40shell))
    T40shell_s = np.append(T_40shell[0,:], np.append(T40shell_s, \
                           T_40shell[-1,:]))

    index40_bp = np.where(T_40BottomPlate == 0)
    index40_bp_nz = np.where(T_40BottomPlate != 0)

    T40bp_s = np.reshape(T_40BottomPlate, np.size(T_40BottomPlate))
    T1 = T_40BottomPlate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T40bp_s1 = np.reshape(T1, np.size(T1))

    T40_s = np.append(T40strap_s, np.append(T40fp_s1, np.append(T40shell_s,\
    T40bp_s1)))

    #4K
    T4strap_s = np.reshape(T_4strap, np.size(T_4strap))

    index4_fp = np.where(T_4FilterPlate == 0)

    T4fp_s = np.reshape(T_4FilterPlate, np.size(T_4FilterPlate))
    T1 = T_4FilterPlate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T4fp_s1 = np.reshape(T1, np.size(T1))

    T4shell_s = np.reshape(T_4shell, np.size(T_4shell))
    T4shell_s = np.append(T_4shell[0,:], np.append(T4shell_s, \
                           T_4shell[-1,:]))

    index4_bp = np.where(T_4BottomPlate == 0)
    index4_bp_nz = np.where(T_4BottomPlate != 0)

    T4bp_s = np.reshape(T_4BottomPlate, np.size(T_4BottomPlate))
    T1 = T_4BottomPlate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T4bp_s1 = np.reshape(T1, np.size(T1))

    T_ot_top_s = np.array([])
    T_ot_bottom_s = np.array([])

    for k in range(opt_num):
        T4opt = np.reshape(T_ot_top[k], np.size(T_ot_top[k]))
        T4opt = np.append(T_ot_top[k,0,:], np.append(T4opt, \
                               T_ot_top[k,-1,:]))
        T_ot_top_s = np.append(T_ot_top_s, T4opt)

        T4opt = np.reshape(T_ot_bottom[k], np.size(T_ot_bottom[k]))
        T4opt = np.append(T_ot_bottom[k,0,:], np.append(T4opt, \
                               T_ot_bottom[k,-1,:]))
        T_ot_bottom_s = np.append(T_ot_bottom_s, T4opt)

    T4_s = np.append(T4strap_s, np.append(T4fp_s1, np.append(T4shell_s,\
    np.append(T4bp_s1, np.append(T_ot_top_s, T_ot_bottom_s)))))

    Tfinal804 = np.append(T80_s, np.append(T40_s, T4_s))

    #1K
    T1strap_s = np.reshape(T_1strap, np.size(T_1strap))

    index1_fp = np.where(T_1Plate == 0)

    T1p_s = np.reshape(T_1Plate, np.size(T_1Plate))
    T1 = T_1Plate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T1p_s1 = np.reshape(T1, np.size(T1))

    T1shell_s = np.reshape(T_1shell, np.size(T_1shell))
    T1shell_s = np.append(T_1shell[0,:], np.append(T1shell_s, \
                           T_1shell[-1,:]))

    index1_bp = np.where(T_1BottomPlate == 0)
    index1_bp_nz = np.where(T_1BottomPlate != 0)

    T1bp_s = np.reshape(T_1BottomPlate, np.size(T_1BottomPlate))
    T1 = T_1BottomPlate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T1bp_s1 = np.reshape(T1, np.size(T1))

    T_ot_top1_s = np.array([])
    T_ot_bottom1_s = np.array([])

    for k in range(opt_num):
        T1opt = np.reshape(T_ot_top1[k], np.size(T_ot_top1[k]))
        T1opt = np.append(T_ot_top1[k,0,:], np.append(T1opt, \
                               T_ot_top1[k,-1,:]))
        T_ot_top1_s = np.append(T_ot_top1_s, T1opt)

        T1opt = np.reshape(T_ot_bottom1[k], np.size(T_ot_bottom1[k]))
        T1opt = np.append(T_ot_bottom1[k,0,:], np.append(T1opt, \
                               T_ot_bottom1[k,-1,:]))
        T_ot_bottom1_s = np.append(T_ot_bottom1_s, T1opt)

    index1_dr = np.where(T_1drPlate == 0)
    index1_dr_nz = np.where(T_1drPlate != 0)

    T1dr_s = np.reshape(T_1drPlate, np.size(T_1drPlate))
    T1 = T_1drPlate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T1dr_s1 = np.reshape(T1, np.size(T1))

    T1_ss = np.append(T1strap_s, np.append(T1p_s1, np.append(T1shell_s,\
    np.append(T1bp_s1, np.append(T_ot_top1_s, T_ot_bottom1_s)))))

    T1_s = np.append(T1_ss, T1dr_s1)


    #100mK
    T100strap_s = np.reshape(T_100strap, np.size(T_100strap))

    index100_fp = np.where(T_100Plate == 0)

    T100p_s = np.reshape(T_100Plate, np.size(T_100Plate))
    T1 = T_100Plate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T100p_s1 = np.reshape(T1, np.size(T1))

    T100shell_s = np.reshape(T_100shell, np.size(T_100shell))
    T100shell_s = np.append(T_100shell[0,:], np.append(T100shell_s, \
                           T_100shell[-1,:]))

    index100_bp = np.where(T_100BottomPlate == 0)
    index100_bp_nz = np.where(T_100BottomPlate != 0)

    T100bp_s = np.reshape(T_100BottomPlate, np.size(T_100BottomPlate))
    T1 = T_100BottomPlate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T100bp_s1 = np.reshape(T1, np.size(T1))

    T_ot_top100_s = np.array([])
    T_ot_bottom100_s = np.array([])

    for k in range(opt_num):
        T100opt = np.reshape(T_ot_top100[k], np.size(T_ot_top100[k]))
        T100opt = np.append(T_ot_top100[k,0,:], np.append(T100opt, \
                               T_ot_top100[k,-1,:]))
        T_ot_top100_s = np.append(T_ot_top100_s, T100opt)

        T100opt = np.reshape(T_ot_bottom100[k], np.size(T_ot_bottom100[k]))
        T100opt = np.append(T_ot_bottom100[k,0,:], np.append(T100opt, \
                               T_ot_bottom100[k,-1,:]))
        T_ot_bottom100_s = np.append(T_ot_bottom100_s, T100opt)

    index100_dr = np.where(T_100drPlate == 0)
    index100_dr_nz = np.where(T_100drPlate != 0)

    T100dr_s = np.reshape(T_100drPlate, np.size(T_100drPlate))
    T1 = T_100drPlate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T100dr_s1 = np.reshape(T1, np.size(T1))

    T100_ss = np.append(T100strap_s, np.append(T100p_s1, np.append(T100shell_s,\
    np.append(T100bp_s1, np.append(T_ot_top100_s, T_ot_bottom100_s)))))

    T100_s = np.append(T100_ss, T100dr_s1)

    index4_dr = np.where(T_4drPlate == 0)
    index4_dr_nz = np.where(T_4drPlate != 0)

    T4dr_s = np.reshape(T_4drPlate, np.size(T_4drPlate))
    T1 = T_4drPlate.copy()
    ar = np.ma.array(T1, mask=T1 < .2)

    for shift in (-1,1):
        for axis in (0,1):
            ar_shifted=np.roll(ar,shift=shift,axis=axis)
            idx=~ar_shifted.mask * ar.mask
            ar[idx]=ar_shifted[idx]

    T4dr_s1 = np.reshape(T1, np.size(T1))

    Tfinaldr = np.append(T1_s, np.append(T100_s, T4dr_s1))


    print 'Temperature Arrays built'

    def factor(k, dt, dens, C, d):
        return (k*dt/dens/C/d**2)

    #Define Load array
    #80K
    load80strap = np.zeros(len(T80strap_s))
    j=0
    for j in range(numbers_mc[0]):
        k_s1 = TC.get_conductivity(m_80hs[j], T_80strap[j,0])
        k_s2 = TC.get_conductivity(m_80hs[j], T_80strap[j,1])
        k_s = (3.*k_s1-k_s2)/2.
        C_s = HC.get_heat_cap(m_80hs[j], T_80strap[j,0])
        rho_s = DEN.get_density(m_80hs[j], T_80strap[j,0])
        #100. is given by the fact that the contact between the PTC and the strap
        #is bigger that the r_80hs which is the equivelent of the 5 small rods
        flux = CL.PT90(T_80strap[j,0], 1)/k_s/np.pi/r_80hs[j]**2/100.
        dxr = l_80hs[j]/n
        load = 2*factor(k_s, dt, rho_s, C_s, dxr)*dxr*flux
        load80strap[j*np.shape(T_80strap)[1]] = load

    #sys.exit()
    nx = np.shape(T_80FilterPlate)[1]
    ny = np.shape(T_80FilterPlate)[0]

    dx1 = d_80fp/(nx-3)
    dy1 = d_80fp/(ny-3)

    load80plate = np.zeros((nx, ny))
    for j in range(opt_num):
        for k in range(len(Opt_FilterPlate_80_cont[j])):
            T = np.mean(T_80FilterPlate[Opt_FilterPlate_80_cont[j][k]])
            C_s = HC.get_heat_cap(m_80fp, T)
            rho_s = DEN.get_density(m_80fp, T)
            load80plate[Opt_FilterPlate_80_cont[j][k]] = filter80[j]/\
                        len(Opt_FilterPlate_80_cont[j])/dx1/dy1/\
                        len(Opt_FilterPlate_80_cont[j][k])/C_s/rho_s*dt

    load80plate_s = np.reshape(load80plate, np.size(load80plate))

    load80shell = np.zeros((nr,ne))
    C_s = HC.get_heat_cap(m_80shell, np.mean(T_80shell))
    rho_s = DEN.get_density(m_80shell, np.mean(T_80shell))
    vol = np.pi*((d_80shell/2.)**2-((d_80shell/2.)-t_80shell)**2)*l_80shell
    load80shell[:,:] = dt*(radiation80+mechanical80)/vol/C_s/rho_s

    load80shell_s = np.reshape(load80shell, np.size(load80shell))
    load80shell_s = np.append(np.zeros(ne), np.append(load80shell_s, np.zeros(ne)))

    load80_s = np.append(load80strap, np.append(load80plate_s, load80shell_s))
    #40K
    load40strap = np.zeros(len(T40strap_s))
    for j in range(numbers_mc[1]):
        k_s1 = TC.get_conductivity(m_40hs[j], T_40strap[j,0])
        k_s2 = TC.get_conductivity(m_40hs[j], T_40strap[j,1])
        k_s = (3.*k_s1-k_s2)/2.
        C_s = HC.get_heat_cap(m_40hs[j], T_40strap[j,0])
        rho_s = DEN.get_density(m_40hs[j], T_40strap[j,0])
        flux = CL.DoubleStage(T_40strap[j,0], T_4strap[j,0], \
               int(cool[1][2:]), 1)[0]/k_s/np.pi/r_40hs[j]**2
        dxr = l_40hs[j]/n
        load = 2*factor(k_s, dt, rho_s, C_s, dxr)*dxr*flux
        load40strap[j*np.shape(T_40strap)[1]] = load

    nx = np.shape(T_40FilterPlate)[1]
    ny = np.shape(T_40FilterPlate)[0]

    dx1 = d_40fp/(nx-3)
    dy1 = d_40fp/(ny-3)

    load40plate = np.zeros((nx, ny))
    for j in range(opt_num):
        for k in range(len(Opt_FilterPlate_40_cont[j])):
            T = np.mean(T_40FilterPlate[Opt_FilterPlate_40_cont[j][k]])
            C_s = HC.get_heat_cap(m_40fp, T)
            rho_s = DEN.get_density(m_40fp, T)
            load40plate[Opt_FilterPlate_40_cont[j][k]] = filter40[j]/\
                        len(Opt_FilterPlate_40_cont[j])/dx1/dy1/\
                        len(Opt_FilterPlate_40_cont[j][k])/C_s/rho_s*dt

    load40plate_s = np.reshape(load40plate, np.size(load40plate))

    load40shell = np.zeros((nr,ne))
    C_s = HC.get_heat_cap(m_40shell, np.mean(T_40shell))
    rho_s = DEN.get_density(m_40shell, np.mean(T_40shell))
    vol = np.pi*((d_40shell/2.)**2-((d_40shell/2.)-t_40shell)**2)*l_40shell
    load40shell[:,:] = dt*(radiation40)/vol/C_s/rho_s

    load40shell_s = np.reshape(load40shell, np.size(load40shell))
    load40shell_s = np.append(np.zeros(ne), np.append(load40shell_s, np.zeros(ne)))

    nx = np.shape(T_40BottomPlate)[1]
    ny = np.shape(T_40BottomPlate)[0]

    dx1 = d_40bp/(nx-3)
    dy1 = d_40bp/(ny-3)

    load40bottom = np.zeros((nx,ny))
    C_s = HC.get_heat_cap(m_40bp, np.mean(T_40BottomPlate))
    rho_s = DEN.get_density(m_40bp, np.mean(T_40BottomPlate))
    load40bottom[index40_bp_nz] = mechanical40/dx1/dy1/len(index40_bp_nz)/C_s/rho_s*dt

    load40bottom_s = np.reshape(load40bottom, np.size(load40bottom))

    load40_s = np.append(load40strap, np.append(load40plate_s, \
    np.append(load40shell_s, load40bottom_s)))

    #4K
    load4strap = np.zeros(len(T4strap_s))
    for j in range(numbers_mc[1]):
        k_s1 = TC.get_conductivity(m_4hs[j], T_4strap[j,0])
        k_s2 = TC.get_conductivity(m_4hs[j], T_4strap[j,1])
        k_s = (3.*k_s1-k_s2)/2.
        C_s = HC.get_heat_cap(m_4hs[j], T_4strap[j,0])
        rho_s = DEN.get_density(m_4hs[j], T_4strap[j,0])
        flux = CL.DoubleStage(T_40strap[j,0], T_4strap[j,0], \
               int(cool[2][2:]), 1)[1]/k_s/np.pi/r_4hs[j]**2
        dxr = l_4hs[j]/n
        load = 2*factor(k_s, dt, rho_s, C_s, dxr)*dxr*flux
        load4strap[j*np.shape(T_4strap)[1]] = load


    nx = np.shape(T_4FilterPlate)[1]
    ny = np.shape(T_4FilterPlate)[0]

    load4plate = np.zeros((nx, ny))

    load4plate_s = np.reshape(load4plate, np.size(load4plate))

    load4shell = np.zeros((nr,ne))
    C_s = HC.get_heat_cap(m_4shell, np.mean(T_4shell))
    rho_s = DEN.get_density(m_4shell, np.mean(T_4shell))
    vol = np.pi*((d_4shell/2.)**2-((d_4shell/2.)-t_4shell)**2)*l_4shell
    load4shell[:,:] = dt*(radiation4)/vol/C_s/rho_s

    load4shell_s = np.reshape(load4shell, np.size(load4shell))
    load4shell_s = np.append(np.zeros(ne), np.append(load4shell_s, np.zeros(ne)))

    nx = np.shape(T_4BottomPlate)[1]
    ny = np.shape(T_4BottomPlate)[0]

    dx1 = d_4bp/(nx-3)
    dy1 = d_4bp/(ny-3)

    load4bottom = np.zeros((nx,ny))
    C_s = HC.get_heat_cap(m_4bp, np.mean(T_4BottomPlate))
    rho_s = DEN.get_density(m_4bp, np.mean(T_4BottomPlate))
    load4bottom[index4_bp_nz] = mechanical4/dx1/dy1/len(index4_bp_nz)/C_s/rho_s*dt

    load4bottom_s = np.reshape(load4bottom, np.size(load4bottom))

    load4ot_top = np.zeros(np.size(T_ot_top)+2*opt_num*np.shape(T_ot_top)[2])
    load4ot_bottom = np.zeros(np.size(T_ot_bottom)+2*opt_num*np.shape(T_ot_bottom)[2])

    for j in range(opt_num):
        vol = np.pi*((d_4ott/2.)**2-((d_4ott/2.)-t_4ott)**2)*l_4ott/nr
        C_s = HC.get_heat_cap(m_4ott, np.mean(T_ot_top[j,-1,:]))
        rho_s = DEN.get_density(m_4ott, np.mean(T_ot_top[j,-1,:]))
        ind_ot = ne*(nr+2)*j+ne
        load4ot_top[ind_ot:ind_ot+ne] += filter4[j]/vol/C_s/rho_s*dt
        ind_ot2 = ne*(nr+2)*j+ne+nr*ne
        load4ot_top[ind_ot:ind_ot2] += optload_top[0,j]/vol/C_s/rho_s*dt*nr
        vol = np.pi*((d_4otb/2.)**2-((d_4otb/2.)-t_4otb)**2)*l_4otb
        load4ot_bottom[ind_ot:ind_ot2] += optload_bottom[0,j]/vol/C_s/rho_s*dt

    load4_s = np.append(load4strap, np.append(load4plate_s, \
    np.append(load4shell_s, np.append(load4bottom_s, np.append(load4ot_top, \
    load4ot_bottom)))))

    loadfinal804 = np.append(load80_s, np.append(load40_s, load4_s))

    #1K
    load1strap = np.zeros(np.size(T1strap_s))

    nx = np.shape(T_1Plate)[1]
    ny = np.shape(T_1Plate)[0]

    load1plate = np.zeros((nx, ny))

    load1plate_s = np.reshape(load1plate, np.size(load1plate))

    load1shell = np.zeros((nr,ne))
    C_s = HC.get_heat_cap(m_1shell, np.mean(T_1shell))
    rho_s = DEN.get_density(m_1shell, np.mean(T_1shell))
    vol = np.pi*((d_1shell/2.)**2-((d_1shell/2.)-t_1shell)**2)*l_1shell
    load4shell[:,:] = dt*(radiation1)/vol/C_s/rho_s

    load1shell_s = np.reshape(load1shell, np.size(load1shell))
    load1shell_s = np.append(np.zeros(ne), np.append(load1shell_s, np.zeros(ne)))

    nx = np.shape(T_1BottomPlate)[1]
    ny = np.shape(T_1BottomPlate)[0]

    dx1 = d_1bp/(nx-3)
    dy1 = d_1bp/(ny-3)

    load1bottom = np.zeros((nx,ny))
    C_s = HC.get_heat_cap(m_1bp, np.mean(T_1BottomPlate))
    rho_s = DEN.get_density(m_1bp, np.mean(T_1BottomPlate))
    load1bottom[index1_bp_nz] = mechanical1/dx1/dy1/len(index1_bp_nz)/C_s/rho_s*dt

    load1bottom_s = np.reshape(load1bottom, np.size(load1bottom))

    load1ot_top = np.zeros(np.size(T_ot_top1)+2*opt_num*np.shape(T_ot_top1)[2])
    load1ot_bottom = np.zeros(np.size(T_ot_bottom1)+2*opt_num*np.shape(T_ot_bottom1)[2])

    for j in range(opt_num):
        vol = np.pi*((d_1ott/2.)**2-((d_1ott/2.)-t_1ott)**2)*l_1ott/nr
        C_s = HC.get_heat_cap(m_1ott, np.mean(T_ot_top1[j,-1,:]))
        rho_s = DEN.get_density(m_1ott, np.mean(T_ot_top1[j,-1,:]))
        ind_ot = ne*(nr+2)*j+ne
        load1ot_top[ind_ot:ind_ot+ne] = filter1[j]/vol/C_s/rho_s*dt
        ind_ot2 = ne*(nr+2)*j+ne+nr*ne
        load1ot_top[ind_ot:ind_ot2] += optload_top[1,j]/vol/C_s/rho_s*dt*nr
        vol = np.pi*((d_1otb/2.)**2-((d_1otb/2.)-t_1otb)**2)*l_1otb
        load1ot_bottom[ind_ot:ind_ot2] += optload_bottom[1,j]/vol/C_s/rho_s*dt

    load1_ss = np.append(load1strap, np.append(load1plate_s, \
    np.append(load1shell_s, np.append(load1bottom_s, np.append(load1ot_top, \
    load1ot_bottom)))))

    load1dr_s = np.zeros(np.size(T_1drPlate))

    load1_s = np.append(load1_ss, load1dr_s)

    #0.1K
    load100strap = np.zeros(np.size(T100strap_s))

    nx = np.shape(T_100Plate)[1]
    ny = np.shape(T_100Plate)[0]

    load100plate = np.zeros((nx, ny))

    load100plate_s = np.reshape(load100plate, np.size(load100plate))

    load100shell = np.zeros((nr,ne))
    C_s = HC.get_heat_cap(m_100shell, np.mean(T_100shell))
    rho_s = DEN.get_density(m_100shell, np.mean(T_100shell))
    vol = np.pi*((d_100shell/2.)**2-((d_100shell/2.)-t_100shell)**2)*l_100shell
    load4shell[:,:] = dt*(radiation100)/vol/C_s/rho_s

    load100shell_s = np.reshape(load100shell, np.size(load100shell))
    load100shell_s = np.append(np.zeros(ne), np.append(load100shell_s, np.zeros(ne)))

    nx = np.shape(T_100BottomPlate)[1]
    ny = np.shape(T_100BottomPlate)[0]

    dx1 = d_100bp/(nx-3)
    dy1 = d_100bp/(ny-3)

    load100bottom = np.zeros((nx,ny))
    C_s = HC.get_heat_cap(m_1bp, np.mean(T_100BottomPlate))
    rho_s = DEN.get_density(m_1bp, np.mean(T_100BottomPlate))
    load1bottom[index100_bp_nz] = mechanical100/dx1/dy1/len(index100_bp_nz)/C_s/rho_s*dt

    load100bottom_s = np.reshape(load100bottom, np.size(load100bottom))
    load100bottom_s[0] = 300.


    load100ot_top = np.zeros(np.size(T_ot_top100)+2*opt_num*np.shape(T_ot_top100)[2])
    load100ot_bottom = np.zeros(np.size(T_ot_bottom100)+2*opt_num*np.shape(T_ot_bottom100)[2])

    for j in range(opt_num):
        vol = np.pi*((d_100ott/2.)**2-((d_100ott/2.)-t_100ott)**2)*l_100ott/nr
        C_s = HC.get_heat_cap(m_100ott, np.mean(T_ot_top100[j,-1,:]))
        rho_s = DEN.get_density(m_100ott, np.mean(T_ot_top100[j,-1,:]))
        ind_ot = ne*(nr+2)*j+ne
        load100ot_top[ind_ot:ind_ot+ne] = filter100[j]/vol/C_s/rho_s*dt
        ind_ot2 = ne*(nr+2)*j+ne+nr*ne
        load100ot_top[ind_ot:ind_ot2] += optload_top[2,j]/vol/C_s/rho_s*dt*nr
        vol = np.pi*((d_100otb/2.)**2-((d_100otb/2.)-t_100otb)**2)*l_100otb
        load100ot_bottom[ind_ot:ind_ot2] += optload_bottom[2,j]/vol/C_s/rho_s*dt

    load100_ss = np.append(load100strap, np.append(load100plate_s, \
    np.append(load100shell_s, np.append(load100bottom_s, np.append(load100ot_top, \
    load100ot_bottom)))))

    load100dr_s = np.zeros(np.size(T_100drPlate))

    load100_s = np.append(load100_ss, load100dr_s)

    #4K DR Plate
    nx = np.shape(T_4drPlate)[1]
    ny = np.shape(T_4drPlate)[0]

    dx1 = d_4drp/(nx-3)
    dy1 = d_4drp/(ny-3)
    load4drplate = np.zeros((ny,nx))

    cp = CL.DoubleStage(np.mean(T_4drPlate[PTC_drPlate_41_cont]), np.mean(T_4drPlate[PTC_drPlate_41_cont]), \
           int(cool[i][2:]), 1)[1]

    C_s = HC.get_heat_cap(m_4drp, np.mean(T_4drPlate[PTC_drPlate_41_cont]))
    rho_s = DEN.get_density(m_4drp, np.mean(T_4drPlate[PTC_drPlate_41_cont]))

    vol = dx1*dy1*len(PTC_drPlate_41_cont[0])*t_4drp

    load4drplate[PTC_drPlate_41_cont] = cp/C_s/rho_s/vol*dt

    load4drplate_s = np.reshape(load4drplate, np.size(load4drplate))

    loadfinal1100 = np.append(load1_s, np.append(load100_s, load4drplate_s))

    finaltemp804 = Tfinal804+loadfinal804
    finaltemp1100 = Tfinaldr+loadfinal1100
    print 'Load Array built'

    temp_imp = linalg.spsolve(sparse.csr_matrix(final804), finaltemp804)

    T80_f = temp_imp[:np.size(T80_s)]
    T40_f = temp_imp[np.size(T80_s):np.size(T80_s)+np.size(T40_s)]
    T4_f = temp_imp[np.size(T80_s)+np.size(T40_s):]

    T_80strap_f = T80_f[:np.size(T80strap_s)]
    T_80strap_f = np.reshape(T_80strap_f, (np.shape(T_80strap)[0], \
                             np.shape(T_80strap)[1]))

    T_80FilterPlate_f = T80_f[np.size(T80strap_s):np.size(T80strap_s)+\
                              np.size(T80fp_s)]
    T_80FilterPlate_f = np.reshape(T_80FilterPlate_f, \
                   (np.shape(T_80FilterPlate)[0], np.shape(T_80FilterPlate)[1]))

    T_80FilterPlate_f[index80_fp] = 0.

    T_80shell_f = T80_f[np.size(T80strap_s)+np.size(T80fp_s):]
    T_80shell_f = np.reshape(T_80shell_f[ne:-ne], (np.shape(T_80shell)[0], \
                              np.shape(T_80shell)[1]))

    T_40strap_f = T40_f[:np.size(T40strap_s)]
    T_40strap_f = np.reshape(T_40strap_f, (np.shape(T_40strap)[0], \
                             np.shape(T_40strap)[1]))

    T_40FilterPlate_f = T40_f[np.size(T40strap_s):np.size(T40strap_s)+\
                              np.size(T40fp_s)]
    T_40FilterPlate_f = np.reshape(T_40FilterPlate_f, \
                   (np.shape(T_40FilterPlate)[0], np.shape(T_40FilterPlate)[1]))

    T_40FilterPlate_f[index40_fp] = 0.

    T_40shell_f = T40_f[np.size(T40strap_s)+np.size(T40fp_s):\
                     np.size(T40strap_s)+np.size(T40fp_s)+np.size(T40shell_s)]
    T_40shell_f = np.reshape(T_40shell_f[ne:-ne], (np.shape(T_40shell)[0], \
                              np.shape(T_80shell)[1]))

    T_40BottomPlate_f = T40_f[np.size(T40strap_s)+np.size(T40fp_s)+\
                              np.size(T40shell_s):]
    T_40BottomPlate_f = np.reshape(T_40BottomPlate_f, \
                   (np.shape(T_40BottomPlate)[0], np.shape(T_40BottomPlate)[1]))

    T_40BottomPlate_f[index40_bp] = 0.

    T_4strap_f = T4_f[:np.size(T4strap_s)]
    T_4strap_f = np.reshape(T_4strap_f, (np.shape(T_4strap)[0], \
                             np.shape(T_4strap)[1]))

    T_4FilterPlate_f = T4_f[np.size(T4strap_s):np.size(T4strap_s)+\
                              np.size(T4fp_s)]
    T_4FilterPlate_f = np.reshape(T_4FilterPlate_f, \
                   (np.shape(T_4FilterPlate)[0], np.shape(T_4FilterPlate)[1]))

    T_4FilterPlate_f[index4_fp] = 0.

    T_4shell_f = T4_f[np.size(T4strap_s)+np.size(T4fp_s):\
                     np.size(T4strap_s)+np.size(T4fp_s)+np.size(T4shell_s)]
    T_4shell_f = np.reshape(T_4shell_f[ne:-ne], (np.shape(T_4shell)[0], \
                              np.shape(T_4shell)[1]))

    T_4BottomPlate_f = T4_f[np.size(T4strap_s)+np.size(T4fp_s)+\
                              np.size(T4shell_s):np.size(T4strap_s)+\
                              np.size(T4fp_s)+np.size(T4shell_s)+np.size(T4bp_s)]
    T_4BottomPlate_f = np.reshape(T_4BottomPlate_f, \
                   (np.shape(T_4BottomPlate)[0], np.shape(T_4BottomPlate)[1]))

    T_4BottomPlate_f[index4_bp] = 0.

    T_ot_top_f = T_ot_top.copy()
    T_ot_bottom_f = T_ot_bottom.copy()

    T_ot_top_ss = T4_f[np.size(T4strap_s)+np.size(T4fp_s)+np.size(T4shell_s)+\
                       np.size(T4bp_s):np.size(T4strap_s)+np.size(T4fp_s)+\
                       np.size(T4shell_s)+np.size(T4bp_s)+np.size(T_ot_top_s)]
    T_ot_bott_ss = T4_f[np.size(T4strap_s)+np.size(T4fp_s)+\
                       np.size(T4shell_s)+np.size(T4bp_s)+np.size(T_ot_top_s):]
    T_ot_top_ss  = np.reshape(T_ot_top_ss, (opt_num, nr+2, ne))
    T_ot_bott_ss  = np.reshape(T_ot_bott_ss, (opt_num, nr+2, ne))

    for j in range(opt_num):
        T_ot_top_f[j,:,:] = T_ot_top_ss[j,1:-1,:]
        T_ot_bottom_f[j,:,:] = T_ot_bott_ss[j,1:-1,:]

    temp_imp = linalg.spsolve(sparse.csr_matrix(final1100), finaltemp1100)

    # ind_z = np.where(temp_imp > 290.)
    # temp_imp[ind_z] = 290.

    T1_f = temp_imp[:np.size(T1_s)]
    T100_f = temp_imp[np.size(T1_s):np.size(T1_s)+np.size(T100_s)]
    T4dr_f = temp_imp[np.size(T1_s)+np.size(T100_s):]

    T_1strap_f = T1_f[:np.size(T1strap_s)]
    T_1strap_f = np.reshape(T_1strap_f, (np.shape(T_1strap)[0], \
                             np.shape(T_1strap)[1]))

    T_1Plate_f = T1_f[np.size(T1strap_s):np.size(T1strap_s)+\
                              np.size(T1p_s)]
    T_1Plate_f = np.reshape(T_1Plate_f, \
                   (np.shape(T_1Plate)[0], np.shape(T_1Plate)[1]))

    T_1Plate_f[index1_fp] = 0.

    T_1shell_f = T1_f[np.size(T1strap_s)+np.size(T1p_s):\
                     np.size(T1strap_s)+np.size(T1p_s)+np.size(T1shell_s)]
    T_1shell_f = np.reshape(T_1shell_f[ne:-ne], (np.shape(T_1shell)[0], \
                              np.shape(T_1shell)[1]))

    T_1BottomPlate_f = T1_f[np.size(T1strap_s)+np.size(T1p_s)+\
                              np.size(T1shell_s):np.size(T1strap_s)+\
                              np.size(T1p_s)+np.size(T1shell_s)+np.size(T1bp_s)]
    T_1BottomPlate_f = np.reshape(T_1BottomPlate_f, \
                   (np.shape(T_1BottomPlate)[0], np.shape(T_1BottomPlate)[1]))

    T_1BottomPlate_f[index1_bp] = 0.

    T_ot_top1_f = T_ot_top1.copy()
    T_ot_bottom1_f = T_ot_bottom1.copy()

    T_ot_top1_ss = T1_f[np.size(T1strap_s)+np.size(T1p_s)+np.size(T1shell_s)+\
                       np.size(T1bp_s):np.size(T1strap_s)+np.size(T1p_s)+\
                       np.size(T1shell_s)+np.size(T1bp_s)+np.size(T_ot_top1_s)]
    T_ot_bott1_ss = T1_f[np.size(T1strap_s)+np.size(T1p_s)+\
                       np.size(T1shell_s)+np.size(T1bp_s)+np.size(T_ot_top1_s):np.size(T1_ss)]
    T_ot_top1_ss  = np.reshape(T_ot_top1_ss, (opt_num, nr+2, ne))
    T_ot_bott1_ss  = np.reshape(T_ot_bott1_ss, (opt_num, nr+2, ne))

    for j in range(opt_num):
        T_ot_top1_f[j,:,:] = T_ot_top1_ss[j,1:-1,:]
        T_ot_bottom1_f[j,:,:] = T_ot_bott1_ss[j,1:-1,:]

    T_1drPlate_f = T1_f[np.size(T1_ss):]
    T_1drPlate_f = np.reshape(T_1drPlate_f, \
                   (np.shape(T_1drPlate)[0], np.shape(T_1drPlate)[1]))

    T_1drPlate_f[index1_dr] = 0.

    T_100strap_f = T100_f[:np.size(T100strap_s)]
    T_100strap_f = np.reshape(T_100strap_f, (np.shape(T_100strap)[0], \
                             np.shape(T_100strap)[1]))

    T_100Plate_f = T100_f[np.size(T100strap_s):np.size(T100strap_s)+\
                              np.size(T100p_s)]
    T_100Plate_f = np.reshape(T_100Plate_f, \
                   (np.shape(T_100Plate)[0], np.shape(T_100Plate)[1]))

    T_100Plate_f[index100_fp] = 0.

    T_100shell_f = T100_f[np.size(T100strap_s)+np.size(T100p_s):\
                     np.size(T100strap_s)+np.size(T100p_s)+np.size(T100shell_s)]
    T_100shell_f = np.reshape(T_100shell_f[ne:-ne], (np.shape(T_100shell)[0], \
                              np.shape(T_100shell)[1]))

    T_100BottomPlate_f = T100_f[np.size(T100strap_s)+np.size(T100p_s)+\
                              np.size(T100shell_s):np.size(T100strap_s)+\
                              np.size(T1p_s)+np.size(T100shell_s)+np.size(T100bp_s)]
    T_100BottomPlate_f = np.reshape(T_100BottomPlate_f, \
                   (np.shape(T_100BottomPlate)[0], np.shape(T_100BottomPlate)[1]))

    T_100BottomPlate_f[index100_bp] = 0.

    T_ot_top100_f = T_ot_top1.copy()
    T_ot_bottom100_f = T_ot_bottom1.copy()

    T_ot_top100_ss = T100_f[np.size(T100strap_s)+np.size(T100p_s)+np.size(T100shell_s)+\
                       np.size(T100bp_s):np.size(T100strap_s)+np.size(T100p_s)+\
                       np.size(T100shell_s)+np.size(T100bp_s)+np.size(T_ot_top100_s)]
    T_ot_bott100_ss = T100_f[np.size(T100strap_s)+np.size(T100p_s)+\
                       np.size(T100shell_s)+np.size(T100bp_s)+np.size(T_ot_top100_s):np.size(T100_ss)]
    T_ot_top100_ss  = np.reshape(T_ot_top100_ss, (opt_num, nr+2, ne))
    T_ot_bott100_ss  = np.reshape(T_ot_bott100_ss, (opt_num, nr+2, ne))

    for j in range(opt_num):
        T_ot_top100_f[j,:,:] = T_ot_top100_ss[j,1:-1,:]
        T_ot_bottom100_f[j,:,:] = T_ot_bott100_ss[j,1:-1,:]

    T_100drPlate_f = T100_f[np.size(T100_ss):]
    T_100drPlate_f = np.reshape(T_100drPlate_f, \
                   (np.shape(T_100drPlate)[0], np.shape(T_100drPlate)[1]))

    T_100drPlate_f[index100_dr] = 0.

    T_4drPlate_f = np.reshape(T4dr_f, (np.shape(T_4drPlate)[0], np.shape(T_4drPlate)[0]))
    T_4drPlate_f[index4_dr] = 0.

    # ar = np.ma.array(T_1drPlate_f, mask = T_1drPlate_f<.2)
    # plt.imshow(ar)
    # plt.colorbar()
    # plt.show()

    print 'Test'

    print '1K DR Loads'

    print 'Heat Strap Load'
    from src.cryolib import thermcont as RT
    l = np.zeros(4)
    for i in range(4):
        Tav = np.mean(T_1drPlate_f[Strap_drPlate_1_cont[i]])
        h = RT.ThermContRes(m_1p, m_1hs[i], np.pi*r_1hs[i]**2, \
                            Tav, T_1strap_f[i,0])
        l[i] = h *(-T_1strap_f[i,0]+Tav)

    l_i = np.zeros(4)
    for i in range(4):
        Tav = np.mean(T_1drPlate[Strap_drPlate_1_cont[i]])
        h = RT.ThermContRes(m_1p, m_1hs[i], np.pi*r_1hs[i]**2, \
                            Tav, T_1strap[i,0])
        l_i[i] = h *(-T_1strap[i,0]+Tav)

    print l
    print l_i
    print l.sum()

    Tswitch4 = np.mean(T_4drPlate_f[Switch_drPlate_41_cont[0]])

    Tswitch1 = np.mean(T_1drPlate_f[Switch_drPlate_14_cont[0]])

    Ts = (Tswitch4+Tswitch1)/2

    C_switch = -8.295E-08*Ts**3+2.433E-05*Ts**2+3.314E-03*Ts-5.164E-02

    ls = C_switch*(Tswitch4-Tswitch1)

    print 'Switch Load'
    print ls

    if np.abs(l.sum()) > np.abs(ls):
        sys.exit()


    tf += dt
    t = np.append(t, tf)

    T_max80 = np.array([np.amax(T_80shell_f), np.amax(T_80strap_f), \
                        np.amax(T_80FilterPlate_f)])
    T_min80 = np.array([np.amin(T_80shell_f[np.nonzero(T_80shell_f)]), \
                        np.amin(T_80strap_f[np.nonzero(T_80strap_f)]), \
                        np.amin(T_80FilterPlate_f[np.nonzero(T_80FilterPlate_f)])])

    T_max40 = np.array([np.amax(T_40shell_f), np.amax(T_40strap_f), \
                        np.amax(T_40FilterPlate_f), \
                        np.amax(T_40BottomPlate_f)])
    T_min40 = np.array([np.amin(T_40shell_f[np.nonzero(T_40shell_f)]), \
                        np.amin(T_40strap_f[np.nonzero(T_40strap_f)]), \
                        np.amin(T_40FilterPlate_f[np.nonzero(T_40FilterPlate_f)]),\
                        np.amin(T_40BottomPlate_f[np.nonzero(T_40BottomPlate_f)])])

    T_max4 = np.array([np.amax(T_4shell_f), np.amax(T_4strap_f), \
                       np.amax(T_4FilterPlate_f), np.amax(T_4BottomPlate_f), \
                       np.amax(T_ot_top_f), np.amax(T_ot_bottom_f)])
    T_min4 = np.array([np.amin(T_4shell_f[np.nonzero(T_4shell_f)]), \
                       np.amin(T_4strap_f[np.nonzero(T_4strap_f)]), \
                       np.amin(T_4FilterPlate_f[np.nonzero(T_4FilterPlate_f)]), \
                       np.amin(T_4BottomPlate_f[np.nonzero(T_4BottomPlate_f)]), \
                       np.amin(T_ot_top_f[np.nonzero(T_ot_top_f)]),\
                       np.amin(T_ot_bottom_f[np.nonzero(T_ot_bottom_f)])])

    T_max1 = np.array([np.amax(T_1shell_f), np.amax(T_1strap_f), \
                       np.amax(T_1Plate_f), np.amax(T_1BottomPlate_f), \
                       np.amax(T_ot_top1_f), np.amax(T_ot_bottom1_f), \
                       np.amax(T_1drPlate_f)])
    T_min1 = np.array([np.amin(T_1shell_f[np.nonzero(T_1shell_f)]), \
                       np.amin(T_1strap_f[np.nonzero(T_1strap_f)]), \
                       np.amin(T_1Plate_f[np.nonzero(T_1Plate_f)]), \
                       np.amin(T_1BottomPlate_f[np.nonzero(T_1BottomPlate_f)]), \
                       np.amin(T_ot_top1_f[np.nonzero(T_ot_top1_f)]),\
                       np.amin(T_ot_bottom1_f[np.nonzero(T_ot_bottom1_f)]),\
                       np.amin(T_1drPlate_f[np.nonzero(T_1drPlate_f)])])

    T_max100 = np.array([np.amax(T_100shell_f), np.amax(T_100strap_f), \
                       np.amax(T_100Plate_f), np.amax(T_100BottomPlate_f), \
                       np.amax(T_ot_top100_f), np.amax(T_ot_bottom100_f), \
                       np.amax(T_100drPlate_f)])
    T_min100 = np.array([np.amin(T_100shell_f[np.nonzero(T_100shell_f)]), \
                       np.amin(T_100strap_f[np.nonzero(T_100strap_f)]), \
                       np.amin(T_100Plate_f[np.nonzero(T_100Plate_f)]), \
                       np.amin(T_100BottomPlate_f[np.nonzero(T_100BottomPlate_f)]), \
                       np.amin(T_ot_top100_f[np.nonzero(T_ot_top100_f)]),\
                       np.amin(T_ot_bottom100_f[np.nonzero(T_ot_bottom100_f)]),\
                       np.amin(T_100drPlate_f[np.nonzero(T_100drPlate_f)])])

    T_max4dr = np.amax(T_4drPlate_f)
    T_min4dr = np.amin(T_4drPlate_f[np.nonzero(T_4drPlate_f)])

    print 'New cycle'
    print tf, 's'
    print tf/3600./24., 'days'
    print time.time()-t1

    print '80K Max'
    print T_max80
    print '80K Min'
    print T_min80
    print '40K Max'
    print T_max40
    print '40K Min'
    print T_min40
    print '4K Max'
    print T_max4
    print '4K Min'
    print T_min4
    print '1K Max'
    print T_max1
    print '1K Min'
    print T_min1
    print '100mK Max'
    print T_max100
    print '100mK Min'
    print T_min100
    print '4DR Plate Max'
    print T_max4dr
    print '4DR Plate Min'
    print T_min4dr

    TmaxS = np.append(TmaxS, np.amax(T_1strap_f))
    TmaxP = np.append(TmaxP, np.amax(T_1drPlate_f))


    fmt_f = '%10.12f'
    fmt = '%10.5f'
    #Writing files with data
    #80K files
    with open(outdir+'80K_Shell.txt', 'a+') as f80shell:
        np.savetxt(f80shell, (T_80shell_f) ,fmt = fmt)
    with open(outdir+'80K_HS.txt', 'a+') as f80hs:
        np.savetxt(f80hs, (T_80strap_f) ,fmt = fmt)
    with open(outdir+'80K_plate.txt', 'a+') as f80plate:
        np.savetxt(f80plate, (T_80FilterPlate_f) ,fmt = fmt)
    #File of the last passage
    with open(outdir+'final/80K_Shell_f.txt', 'w+') as f80shell:
        np.savetxt(f80shell, (T_80shell_f) ,fmt = fmt_f)
    with open(outdir+'final/80K_HS_f.txt', 'w+') as f80hs:
        np.savetxt(f80hs, (T_80strap_f) ,fmt = fmt_f)
    with open(outdir+'final/80K_plate_f.txt', 'w+') as f80plate:
        np.savetxt(f80plate, (T_80FilterPlate_f) ,fmt = fmt_f)

    #40K files
    with open(outdir+'40K_Shell.txt', 'a+') as f40shell:
        np.savetxt(f40shell, (T_40shell_f) ,fmt = fmt)
    with open(outdir+'40K_HS.txt', 'a+') as f40hs:
        np.savetxt(f40hs, (T_40strap_f) ,fmt = fmt)
    with open(outdir+'40K_plate.txt', 'a+') as f40plate:
        np.savetxt(f40plate, (T_40FilterPlate_f) ,fmt = fmt)
    with open(outdir+'40K_bottom.txt', 'a+') as f40bottom:
        np.savetxt(f40bottom, (T_40BottomPlate_f) ,fmt = fmt)
    #File of the last passage
    with open(outdir+'final/40K_Shell_f.txt', 'w+') as f40shell_f:
        np.savetxt(f40shell_f, (T_40shell_f) ,fmt = fmt_f)
    with open(outdir+'final/40K_HS_f.txt', 'w+') as f40hs_f:
        np.savetxt(f40hs_f, (T_40strap_f) ,fmt = fmt_f)
    with open(outdir+'final/40K_plate_f.txt', 'w+') as f40plate_f:
        np.savetxt(f40plate_f, (T_40FilterPlate_f) ,fmt = fmt_f)
    with open(outdir+'final/40K_bottom_f.txt', 'w+') as f40bottom_f:
        np.savetxt(f40bottom_f, (T_40BottomPlate_f) ,fmt = fmt_f)

    #4K files
    with open(outdir+'4K_plate.txt', 'a+') as f4plate:
        np.savetxt(f4plate, (T_4FilterPlate_f) ,fmt = fmt)
    with open(outdir+'4K_bottom.txt', 'a+') as f4bottom:
        np.savetxt(f4bottom, (T_4BottomPlate_f) ,fmt = fmt)
    with open(outdir+'4K_HS.txt', 'a+') as f4hs:
        np.savetxt(f4hs, (T_4strap_f) ,fmt = fmt)
    with open(outdir+'4K_Shell.txt', 'a+') as f4shell:
        np.savetxt(f4shell, (T_4shell_f) ,fmt = fmt)
    for y in range(opt_num):
        Tott_file = outdir+'4K_opt_tube_top_'+str(y)+'.txt'
        Totb_file = outdir+'4K_opt_tube_bottom_'+str(y)+'.txt'
        with open(Tott_file, 'a+') as fott:
            np.savetxt(fott, (T_ot_top_f[y,:]) ,fmt = fmt)
        with open(Totb_file, 'a+') as fotb:
            np.savetxt(fotb, (T_ot_bottom_f[y,:]) ,fmt = fmt)
        Tott_file_f = outdir+'final/4K_opt_tube_top_f'+str(y)+'.txt'
        Totb_file_f = outdir+'final/4K_opt_tube_bottom_f'+str(y)+'.txt'
        with open(Tott_file_f, 'w+') as fott_f:
            np.savetxt(fott_f, (T_ot_top_f[y,:]) ,fmt = fmt_f)
        with open(Totb_file_f, 'w+') as fotb_f:
            np.savetxt(fotb_f, (T_ot_bottom_f[y,:]) ,fmt = fmt_f)
    with open(outdir+'final/4K_plate_f.txt', 'w+') as f4plate_f:
        np.savetxt(f4plate_f, (T_4FilterPlate_f) ,fmt = fmt_f)
    with open(outdir+'final/4K_bottom.txt', 'w+') as f4bottom_f:
        np.savetxt(f4bottom_f, (T_4BottomPlate_f) ,fmt = fmt_f)
    with open(outdir+'final/4K_HS.txt', 'w+') as f4hs_f:
        np.savetxt(f4hs_f, (T_4strap_f) ,fmt = fmt_f)
    with open(outdir+'final/4K_Shell.txt', 'w+') as f4shell_f:
        np.savetxt(f4shell_f, (T_4shell_f) ,fmt = fmt_f)

    #1K files
    with open(outdir+'1K_plate.txt', 'a+') as f1plate:
        np.savetxt(f1plate, (T_1Plate_f) ,fmt = fmt)
    with open(outdir+'1K_bottom.txt', 'a+') as f1bottom:
        np.savetxt(f1bottom, (T_1BottomPlate_f) ,fmt = fmt)
    with open(outdir+'1K_HS.txt', 'a+') as f1hs:
        np.savetxt(f1hs, (T_1strap_f) ,fmt = fmt)
    with open(outdir+'1K_Shell.txt', 'a+') as f1shell:
        np.savetxt(f1shell, (T_1shell_f) ,fmt = fmt)
    with open(outdir+'1K_drPlate.txt', 'a+') as f1dr:
        np.savetxt(f1dr, (T_1drPlate_f) ,fmt = fmt)
    for y in range(opt_num):
        Tott_file = outdir+'1K_opt_tube_top_'+str(y)+'.txt'
        Totb_file = outdir+'1K_opt_tube_bottom_'+str(y)+'.txt'
        with open(Tott_file, 'a+') as fott1:
            np.savetxt(fott1, (T_ot_top1_f[y,:]) ,fmt = fmt)
        with open(Totb_file, 'a+') as fotb1:
            np.savetxt(fotb1, (T_ot_bottom1_f[y,:]) ,fmt = fmt)
        Tott_file_f = outdir+'final/1K_opt_tube_top_'+str(y)+'.txt'
        Totb_file_f = outdir+'final/1K_opt_tube_bottom_'+str(y)+'.txt'
        with open(Tott_file_f, 'w+') as fott1_f:
            np.savetxt(fott1_f, (T_ot_top1_f[y,:]) ,fmt = fmt_f)
        with open(Totb_file_f, 'w+') as fotb1_f:
            np.savetxt(fotb1_f, (T_ot_bottom1_f[y,:]) ,fmt = fmt_f)
    with open(outdir+'final/1K_plate_f.txt', 'w+') as f1plate_f:
        np.savetxt(f1plate_f, (T_1Plate_f) ,fmt = fmt_f)
    with open(outdir+'final/1K_bottom.txt', 'w+') as f1bottom_f:
        np.savetxt(f1bottom_f, (T_1BottomPlate_f) ,fmt = fmt_f)
    with open(outdir+'final/1K_HS.txt', 'w+') as f1hs_f:
        np.savetxt(f1hs_f, (T_1strap_f) ,fmt = fmt_f)
    with open(outdir+'final/1K_Shell.txt', 'w+') as f1shell_f:
        np.savetxt(f1shell_f, (T_1shell_f) ,fmt = fmt_f)
    with open(outdir+'final/1K_drPlate.txt', 'w+') as f1dr:
        np.savetxt(f1dr, (T_1drPlate_f) ,fmt = fmt_f)

    #1K files
    with open(outdir+'100mK_plate.txt', 'a+') as f100plate:
        np.savetxt(f100plate, (T_100Plate_f) ,fmt = fmt)
    with open(outdir+'100mK_bottom.txt', 'a+') as f100bottom:
        np.savetxt(f100bottom, (T_100BottomPlate_f) ,fmt = fmt)
    with open(outdir+'100mK_HS.txt', 'a+') as f100hs:
        np.savetxt(f100hs, (T_100strap_f) ,fmt = fmt)
    with open(outdir+'100mK_Shell.txt', 'a+') as f100shell:
        np.savetxt(f100shell, (T_100shell_f) ,fmt = fmt)
    with open(outdir+'100mK_drPlate.txt', 'a+') as f100dr:
        np.savetxt(f100dr, (T_100drPlate_f) ,fmt = fmt)
    for y in range(opt_num):
        Tott_file = outdir+'100mK_opt_tube_top_'+str(y)+'.txt'
        Totb_file = outdir+'100mK_opt_tube_bottom_'+str(y)+'.txt'
        with open(Tott_file, 'a+') as fott100:
            np.savetxt(fott100, (T_ot_top100_f[y,:]) ,fmt = fmt)
        with open(Totb_file, 'a+') as fotb100:
            np.savetxt(fotb100, (T_ot_bottom100_f[y,:]) ,fmt = fmt)
        Tott_file_f = outdir+'final/100mK_opt_tube_top_'+str(y)+'.txt'
        Totb_file_f = outdir+'final/100mK_opt_tube_bottom_'+str(y)+'.txt'
        with open(Tott_file_f, 'w+') as fott100_f:
            np.savetxt(fott100_f, (T_ot_top100_f[y,:]) ,fmt = fmt_f)
        with open(Totb_file_f, 'w+') as fotb100_f:
            np.savetxt(fotb100_f, (T_ot_bottom100_f[y,:]) ,fmt = fmt_f)
    with open(outdir+'final/100mK_plate_f.txt', 'w+') as f100plate_f:
        np.savetxt(f100plate_f, (T_100Plate_f) ,fmt = fmt_f)
    with open(outdir+'final/100mK_bottom.txt', 'w+') as f100bottom_f:
        np.savetxt(f100bottom_f, (T_100BottomPlate_f) ,fmt = fmt_f)
    with open(outdir+'final/100mK_HS.txt', 'w+') as f100hs_f:
        np.savetxt(f100hs_f, (T_100strap_f) ,fmt = fmt_f)
    with open(outdir+'final/100mK_Shell.txt', 'w+') as f100shell_f:
        np.savetxt(f100shell_f, (T_100shell_f) ,fmt = fmt_f)
    with open(outdir+'final/100mK_drPlate.txt', 'w+') as f100dr_f:
        np.savetxt(f100dr_f, (T_100drPlate_f) ,fmt = fmt_f)

    with open(outdir+'4drPlate.txt', 'a+') as f4dr:
        np.savetxt(f4dr, (T_4drPlate_f) ,fmt = fmt)
    with open(outdir+'final/4drPlate.txt', 'w+') as f4dr_f:
        np.savetxt(f4dr_f, (T_4drPlate_f) ,fmt = fmt_f)

    T_80FP_ratio = T_80FilterPlate[np.nonzero(T_80FilterPlate)]/\
                   T_80FilterPlate_f[np.nonzero(T_80FilterPlate_f)]
    T_80s_ratio = T_80shell/T_80shell_f
    T_80st_ratio = T_80strap/T_80strap_f

    T_40FP_ratio = T_40FilterPlate[np.nonzero(T_40FilterPlate)]/\
                   T_40FilterPlate_f[np.nonzero(T_40FilterPlate_f)]
    T_40BP_ratio = T_40BottomPlate[np.nonzero(T_40BottomPlate)]/\
                   T_40BottomPlate_f[np.nonzero(T_40BottomPlate_f)]
    T_40s_ratio = T_40shell/T_40shell_f
    T_40st_ratio = T_40strap/T_40strap_f

    T_4FP_ratio = T_4FilterPlate[np.nonzero(T_4FilterPlate)]/\
                   T_4FilterPlate_f[np.nonzero(T_4FilterPlate_f)]
    T_4BP_ratio = T_4BottomPlate[np.nonzero(T_4BottomPlate)]/\
                   T_4BottomPlate_f[np.nonzero(T_4BottomPlate_f)]
    T_4s_ratio = T_4shell/T_4shell_f
    T_4st_ratio = T_4strap/T_4strap_f
    T_ott_ratio = T_ot_top/T_ot_top_f
    T_otb_ratio = T_ot_bottom/T_ot_bottom_f

    T_1FP_ratio = T_1Plate[np.nonzero(T_1Plate)]/\
                   T_1Plate_f[np.nonzero(T_1Plate_f)]
    T_1BP_ratio = T_1BottomPlate[np.nonzero(T_1BottomPlate)]/\
                   T_1BottomPlate_f[np.nonzero(T_1BottomPlate_f)]
    T_1s_ratio = T_1shell/T_1shell_f
    T_1st_ratio = T_1strap/T_1strap_f
    T_ott1_ratio = T_ot_top1/T_ot_top1_f
    T_otb1_ratio = T_ot_bottom1/T_ot_bottom1_f
    T_dr1_ratio = T_1drPlate[np.nonzero(T_1drPlate)]/\
                  T_1drPlate_f[np.nonzero(T_1drPlate_f)]

    T_100FP_ratio = T_100Plate[np.nonzero(T_100Plate)]/\
                   T_100Plate_f[np.nonzero(T_100Plate_f)]
    T_100BP_ratio = T_100BottomPlate[np.nonzero(T_100BottomPlate)]/\
                   T_100BottomPlate_f[np.nonzero(T_100BottomPlate_f)]
    T_100s_ratio = T_100shell/T_100shell_f
    T_100st_ratio = T_100strap/T_100strap_f
    T_ott100_ratio = T_ot_top100/T_ot_top100_f
    T_otb100_ratio = T_ot_bottom100/T_ot_bottom100_f
    T_dr100_ratio = T_100drPlate[np.nonzero(T_100drPlate)]/\
                  T_100drPlate_f[np.nonzero(T_100drPlate_f)]

    T_dr4_ratio = T_4drPlate[np.nonzero(T_4drPlate)]/\
                  T_4drPlate_f[np.nonzero(T_4drPlate_f)]

    ratio = np.abs(np.array([np.amax(T_80FP_ratio), np.amax(T_80s_ratio), \
                             np.amax(T_80st_ratio), np.amax(T_40FP_ratio), \
                             np.amax(T_40BP_ratio), np.amax(T_40s_ratio), \
                             np.amax(T_40st_ratio), np.amax(T_4FP_ratio), \
                             np.amax(T_4BP_ratio), np.amax(T_4s_ratio), \
                             np.amax(T_4st_ratio), np.amax(T_ott_ratio),
                             np.amax(T_otb_ratio), np.amax(T_1FP_ratio), \
                             np.amax(T_1BP_ratio), np.amax(T_1s_ratio), \
                             np.amax(T_1st_ratio), np.amax(T_ott1_ratio),
                             np.amax(T_otb1_ratio), np.amax(T_dr1_ratio),\
                             np.amax(T_1FP_ratio), \
                             np.amax(T_1BP_ratio), np.amax(T_1s_ratio), \
                             np.amax(T_1st_ratio), np.amax(T_ott1_ratio),
                             np.amax(T_otb1_ratio), np.amax(T_dr1_ratio),\
                             np.amax(T_dr4_ratio)]))-1.



    T_80FilterPlate = T_80FilterPlate_f.copy()
    T_80shell = T_80shell_f.copy()
    T_80strap = T_80strap_f.copy()

    T_40FilterPlate = T_40FilterPlate_f.copy()
    T_40BottomPlate = T_40BottomPlate_f.copy()
    T_40shell = T_40shell_f.copy()
    T_40strap = T_40strap_f.copy()

    T_4FilterPlate = T_4FilterPlate_f.copy()
    T_4BottomPlate = T_4BottomPlate_f.copy()
    T_4shell = T_4shell_f.copy()
    T_4strap = T_4strap_f.copy()
    T_ot_top = T_ot_top_f.copy()
    T_ot_bottom = T_ot_bottom_f.copy()

    T_1Plate = T_1Plate_f.copy()
    T_1BottomPlate = T_1BottomPlate_f.copy()
    T_1shell = T_1shell_f.copy()
    T_1strap = T_1strap_f.copy()
    T_ot_top1 = T_ot_top1_f.copy()
    T_ot_bottom1 = T_ot_bottom1_f.copy()
    T_1drPlate = T_1drPlate_f.copy()

    T_100Plate = T_100Plate_f.copy()
    T_100BottomPlate = T_100BottomPlate_f.copy()
    T_100shell = T_100shell_f.copy()
    T_100strap = T_100strap_f.copy()
    T_ot_top100 = T_ot_top100_f.copy()
    T_ot_bottom100 = T_ot_bottom100_f.copy()
    T_100drPlate = T_100drPlate_f.copy()

    T_4drPlate = T_4drPlate_f.copy()
