import numpy as np
import os

from scipy import spatial, interpolate

def PT90(T,n):

    if T >= 30.:
        return -n*(-7.17e-12*T**6+7.70e-09*T**5-3.35e-06*T**4+7.62e-04*T**3-9.83e-2*T**2+7.43*T-151.87)
    else:
        return 0.

def DoubleStage(T_1, T_2, cooler, n):
    # self.T_1 = T_1
    # self.T_2 = T_2
    # self.cooler = cooler

    string = '/PT'+str(cooler)+'.txt'

    T1, T2, L1, L2 = np.loadtxt(os.path.dirname(__file__)+string, unpack='True')
    T_41 = np.vstack((T1, T2)).T
    Q_41 = np.vstack((L1, L2)).T

    pT = [T_1, T_2]
    distance, index = spatial.KDTree(T_41).query(pT)

    if T_1 < 220 and T_2 < 210:
        method = 'linear'
    else:
        method = 'nearest'

    Q = np.zeros(2)
    if T_1 > np.min(T1) and T_2 > np.min(T2):
        Q[0] = -n*interpolate.griddata((T1, T2), L1, (T_1, T_2), method = method)
    else:
        Q[0] = np.min(L1)
    if T_2 > np.min(T2):
        Q[1] = -n*interpolate.griddata((T1, T2), L2, (T_1, T_2), method = method)
    else:
        Q[1] = np.min(L2)

    if cooler == 410 and np.isnan(Q[1]) == True:
        Q[1] = n*(-1.63845E-09*T_2**4 + 1.18863E-06*T_2**3 - 1.35096E-03*T_2**2 + 8.16012E-01*T_2 - 2.43701E+00)

    return Q

def DoubleStageMin(cooler):

    string = '/PT'+str(cooler)+'.txt'

    T1, T2, L1, L2 = np.loadtxt(os.path.dirname(__file__)+string, unpack='True')
    T_41 = np.vstack((T1, T2)).T
    Q_41 = np.vstack((L1, L2)).T

    Tmin = np.array([np.min(T_41[:,0]), np.min(T_41[:,1])])

    return Tmin

def LNLatent(n, dt):

    l = 5900 #J/mol

    return n*l

# def LNLatentHeat(n_l, T, V, dt):
#
#     ratio = 684.
#     rho_l = 808 #Kg/m^3
#     rho_g = 1.251 #Kg/cm^3
#     l = 5900 #J/mol
#
#     entalphy =
#
#     n_g = rho_l*V-n_l
#
#     V_free = n_l/rho_l
#     V_gas = V-V_free
#
#     n_gas_u = V_gas*rho_g
#
#     if V_gas >= 0.1*V:
#         Q =
