import numpy as np
import os
import scipy

from scipy.integrate import quad as integrate

def heat_cap_model(material):
    param = np.loadtxt(os.path.dirname(__file__)+'/heatcapacity.txt', dtype = str)
    pmat = np.array([mat.lower() for mat in param[:,0]])
    idx = (pmat == str.strip(material.lower()))

    mtype = param[idx,1][0]
    Tlow = (param[idx,3].astype(float))[0]
    Thigh = (param[idx,4].astype(float))[0]
    p = (param[idx,5:].astype(float))[0]

    if mtype == 'NIST':

      def model(T):
        k = 0
        for i in range(9):
          k += p[i]*np.log10(T)**i
        return 10**k
      return model, Tlow, Thigh

def get_heat_cap(material, T):
  model, Tlow, Thigh = heat_cap_model(material)

  if type(T) in [list, np.ndarray]:
    T = np.array(T)

    if (T < Tlow).sum():
        return model(Tlow)
    elif (T > Thigh).sum():
        return model(Thigh)
    else:
        return model(T)
      #print 'WARNING: Temperatures are outside model\'s range of validity: ' \
      #      + '%.3f - %.3f K' %(Tlow,Thigh)

  else:
     if (T < Tlow):
         return model(Tlow)
     elif (T > Thigh):
         return model(Thigh)
     else:
         return model(T)

def integrated_heat_cap(material, T1, T2):

    model, Tlow, Thigh = heat_cap_model(material)

    if type(T1) in [list, np.ndarray]:
      T1 = np.array(T1)

      if (T1 < Tlow).sum() or (T1 > Thigh).sum():
        pass
        #print 'WARNING: Low temperatures are outside model\'s range of ' \
        #      + 'validity: %.3f - %.3f K' %(Tlow,Thigh)

      if type(T2) in [list, np.ndarray]:
        T2 = np.array(T2)

        if (T2 < Tlow).sum() or (T2 > Thigh).sum():
          pass
          #print 'WARNING: High temperatures are outside model\'s range of ' \
          #      + 'validity: %.3f - %.3f K' %(Tlow,Thigh)

        K = np.zeros_like(T1)

        for i in range(len(T1)):
          K[i] = integrate(model, T1[i], T2[i],  epsabs = 0.0, epsrel = 1E-7)[0]

        return K

      else:
        K = np.zeros_like(T1)

        for i in range(len(T1)):
          K[i] = integrate(model, T1[i], T2,  epsabs = 0.0, epsrel = 1E-7)[0]

        return K

    elif type(T2) in [list, np.ndarray]:
      T2 = np.array(T2)

      if (T2 < Tlow).sum() or (T2 > Thigh).sum():
        pass
        #print 'WARNING: High temperatures are outside model\'s range of ' \
        #      + 'validity: %.3f - %.3f K' %(Tlow,Thigh)

      K = np.zeros_like(T2)

      for i in range(len(T2)):
        K[i] = integrate(model, T1, T2[i],  epsabs = 0.0, epsrel = 1E-7)[0]

      return K

    else:
      if T1 < Tlow or T1 > Thigh:
        pass
        #print 'WARNING: Low Temperature is outside model\'s range of validity: ' \
        #      + '%.3f - %.3f K' %(Tlow,Thigh)

      if T2 < Tlow or T2 > Thigh:
        pass
        #print 'WARNING: High Temperature is outside model\'s range of ' \
        #      + 'validity: %.3f - %.3f K' %(Tlow,Thigh)

      return integrate(model, T1, T2,  epsabs = 0.0, epsrel = 1E-7)[0]
