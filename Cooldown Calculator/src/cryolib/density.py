import numpy as np
import os

### FUNCTIONS ###

def density_model(material):

  material = material.strip()
  param = np.loadtxt(os.path.dirname(__file__)+'/density.txt', dtype = str)
  pmat = np.array([mat.lower() for mat in param[:,0]])
  idx = (pmat == material.lower())

  mtype = param[idx,1][0]
  Tlow = (param[idx,3].astype(float))[0]
  Thigh = (param[idx,4].astype(float))[0]
  p = (param[idx,5:].astype(float))[0]

  if mtype.lower() == 'constant':

    def model(T):
      return p[0]

    return model, Tlow, Thigh

def get_density(material, T):
  model, Tlow, Thigh = density_model(material)

  if type(T) in [list, np.ndarray]:
    T = np.array(T)

    if (T < Tlow).sum() or (T > Thigh).sum():
      pass
      #print 'WARNING: Temperatures are outside model\'s range of validity: ' \
      #      + '%.3f - %.3f K' %(Tlow,Thigh)

  elif T < Tlow or T > Thigh:
    pass
    #print 'WARNING: Temperature is outside model\'s range of validity: ' \
    #      '%.3f - %.3f K' %(Tlow,Thigh)

  return model(T)
