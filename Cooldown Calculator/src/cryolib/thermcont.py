import numpy as np
import os

from src.cryolib import conductivity as TC

def microhardnesscoeff(Hb):

    H0 = 1e9*3.178

    c1 = H0*(4.0-5.77*(Hb/H0)+4.0*(Hb/H0)**2-0.61*(Hb/H0)**3) #In Pa
    c2 = -0.37+0.442*Hb/c1

    return c1, c2

def microhardnesscont(c1, c2, sigma, m, P):

    return P/((P/1.62/c1/(sigma/m)**c2)**(1/(1+0.071*c2)))

def ThermContRes(mat1, mat2, area, T1, T2):

    param = np.loadtxt(os.path.dirname(__file__)+'/thermcont.txt', dtype = str)
    pmat1 = np.array([mat1.lower() for mat1 in param[:,0]])
    pmat2 = np.array([mat2.lower() for mat2 in param[:,0]])

    idx1 = (pmat1 == str.strip(mat1.lower()))
    idx2 = (pmat2 == str.strip(mat2.lower()))

    mtype1 = param[idx1,1][0]
    p1 = (param[idx1,3:].astype(float))[0]

    mtype2 = param[idx2,1][0]
    p2 = (param[idx2,3:].astype(float))[0]

    if p1[1]==0 and p1[2] == 0:
        c11 = microhardnesscoeff(p1[4])[0]
        c12 = microhardnesscoeff(p1[4])[1]
    else:
        c11 = p1[1]*1e9
        c12 = p1[2]

    if p2[1]==0 and p2[2] == 0:
        c21 = microhardnesscoeff(p2[4])[0]
        c22 = microhardnesscoeff(p2[4])[1]
    else:
        c21 = p2[1]*1e9
        c22 = p2[2]

    sigma = np.sqrt(p1[0]**2+p2[0]**2)*1e-6
    m = np.sqrt(p1[3]**2+p2[3]**2)

    pres = 1e5/area #Force is 1e5N need to be modified to be variable for multiple appications

    Hc1 = microhardnesscont(c11, c12, p1[0], p1[3], pres)
    Hc2 = microhardnesscont(c21, c22, p1[0], p1[3], pres)

    Hc = np.amin(np.array([Hc1, Hc2]))

    k1 = TC.get_conductivity(mat1, T1)
    k2 = TC.get_conductivity(mat2, T2)

    k = k1*k2/(k1+k2)

    return 1.25*area*k*m/sigma*(pres/Hc)**0.95
