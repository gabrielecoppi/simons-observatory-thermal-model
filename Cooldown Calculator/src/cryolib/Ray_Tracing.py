#!/usr/bin/python -u
import numpy as np
import os
import shutil
import sys

import warnings
warnings.filterwarnings('ignore')

import filters
from mpi4py import MPI
# MPI stuff

comm = MPI.COMM_WORLD
rank = comm.rank
nproc = comm.size
comm.Barrier()
# Initialize filter stack
cfgfp = sys.argv[1]
stack = filters.OpticalStack.from_config(cfgfp)

# Compute geometric transfer factors

stack.compute_transfer_factors(comm)

F = stack.transfer_factors.copy()

outdir = 'stacks/'+stack.id+'/'

fmt = '%10.12f'

with file(outdir+'Ray_Tracing_Shape.txt', 'w') as outfile1:
    np.savetxt(outfile1, F.shape,fmt = fmt)

with file(outdir+'Ray_Tracing.txt', 'w') as outfile2:
    outfile2.write('# Array shape: {0}\n'.format(F.shape))

    for f in range(len(F)):
        np.savetxt(outfile2, F[f],fmt = fmt)

        outfile2.write('# New slice\n')
