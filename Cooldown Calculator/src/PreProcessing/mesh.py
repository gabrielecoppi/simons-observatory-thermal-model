import numpy as np
import os
import sys


class plate(object):

    def __init__(self, radius):

        self.radius = radius

    def meshing(self, nx, ny, name):

        dx = 2*self.radius/nx
        dy = 2*self.radius/ny

        nn = nx+3

        x = np.linspace(-self.radius-dy, self.radius+dx, nn)
        y = np.linspace(self.radius+dx, -self.radius-dy, nn)

        # x = np.linspace(-self.radius, self.radius, nx)
        # y = np.linspace(self.radius, -self.radius, ny)
        #
        # dx = np.abs(x[1]-x[0])
        # dy = np.abs(y[1]-y[0])
        #
        # x = np.append(-self.radius-dx, np.append(x, self.radius+dx))
        # y = np.append(+self.radius+dy, np.append(y, -self.radius-dy))

        mesh_mat = np.zeros((nn, nn))

        xx, yy = np.meshgrid(x,y)
        index = np.where((np.sqrt(xx**2+yy**2)<=self.radius))

        mesh_mat[index] =1.

        with open('Mesh/'+name, 'w+') as fmesh:
            np.savetxt(fmesh, (mesh_mat))

        return mesh_mat

class holedplate(object):

    def __init__(self, radius, thickness, holenumbers, holeradii, holepositions):

        #Plates properties
        self.radius = radius
        self.thickness = thickness

        #Holes Properties
        self.holenumbers = holenumbers
        self.holeradii = holeradii
        self.holepositions = holepositions


    def meshing(self, nx, ny, name, semplification2D = True, \
                thickelements = None):

        if isinstance(self.holeradii, float):
            holerad = np.ones(self.holenumbers)*self.holeradii
        else:
            holerad = self.holeradii

        dx = 2*self.radius/nx
        dy = 2*self.radius/ny

        nn = nx+3

        x = np.linspace(-self.radius-dy, self.radius+dx, nn)
        y = np.linspace(self.radius+dx, -self.radius-dy, nn)
        #
        # x = np.append(-self.radius-dx, np.append(x, self.radius+dx))
        # y = np.append(+self.radius+dy, np.append(y, -self.radius-dy))

        mesh_mat = np.zeros((nn, nn))

        xx, yy = np.meshgrid(x,y)
        index = np.where((np.sqrt(xx**2+yy**2)<=self.radius))

        mesh_mat[index] =1.

        cond = np.zeros(((nn),(nn)))
        for k in range(self.holenumbers):
            index = np.where(np.sqrt((xx-self.holepositions[k][0])**2\
                           +(yy-self.holepositions[k][1])**2) <= holerad[k])
            mesh_mat[index] = 0.

        with open('Mesh/'+name, 'w+') as fmesh:
            np.savetxt(fmesh, (mesh_mat))

        if semplification2D == True:
            return mesh_mat
