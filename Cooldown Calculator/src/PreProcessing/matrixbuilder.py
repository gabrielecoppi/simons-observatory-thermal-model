import numpy as np
from scipy import sparse
from scipy import interpolate

from mpmath import *

from src.cryolib import thermcont as RT
from src.cryolib import conductivity as TC
from src.cryolib import heatcapacity as HC
from src.cryolib import density as DEN


def factor(dt, T, dens, d, material):
    k = TC.get_conductivity(material, T)
    C = HC.get_heat_cap(material, T)
    return (k*dt/dens/C/d**2)

def factorLoad(dt, T, dens, material):
    C = HC.get_heat_cap(material, T)
    return dt/dens/C


class strap(object):

    def __init__(self, Tstrap, material, length, timestep):

        self.Tstrap = Tstrap
        self.material = material
        self.length = length
        self.timestep = timestep

     def factorS(self, dt, T1, T2, dens, d, material):
        k1 = TC.get_conductivity(material, T1)
        k2 = TC.get_conductivity(material, T2)
        k = (k1+k2)/2.
        C = HC.get_heat_cap(material, T1)
        return (k*dt/dens/C/d**2)

    def matrix(self, plate=False, Tplate=None, platematerial=None, dx=None, \
               dy=None, contactplate=None, shell=False, Tshell=None, \
               bottom=False, Tbottom=None, OTT=False, Tott=None, OTB=False, Totb=None):

        strap_shape = np.shape(self.Tstrap)

        coeff_strap = np.zeros((strap_shape[0]*strap_shape[1], \
                                strap_shape[0]*strap_shape[1]))

        if plate == True:
            coeff_plate = np.zeros((np.size(self.Tstrap), \
                                             np.size(Tplate)))

        for i in range(len(self.Tstrap)):

            dens = DEN.get_density(self.material[i], np.mean(self.Tstrap[i]))

            dxr = self.length[i]/(strap_shape[1]-1)

            if plate==True:
                Tav = np.mean(Tplate[contactplate[i]])
                h = RT.ThermContRes(platematerial, self.material, dx*\
                                    dy*len(contactplate[i][0]), \
                                    Tav, self.Tstrap[i,-1])

            coeff1 = np.zeros((strap_shape[1],strap_shape[1]))
            coeff2 = coeff1.copy()
            coeff3 = coeff1.copy()
            coeff_1 = np.zeros(strap_shape[1]-2)
            coeff_2 = np.zeros(strap_shape[1]-2)
            coeff_3 = np.zeros(strap_shape[1]-2)

            coeff_1[:] = -self.factorS(self.timestep, self.Tstrap[i, 1:-1], self.Tstrap[i, 2:],\
                          dens, dxr, self.material[i])
            coeff_2[:] = -self.factorS(self.timestep, self.Tstrap[i, 1:-1], self.Tstrap[i, :-2],\
                          dens, dxr, self.material[i])
            coeff_3 = 1-coeff_1-coeff_2

            coeff1[1:-1, 2:] = np.diag(coeff_1)
            coeff2[1:-1, :-2] = np.diag(coeff_2)
            coeff3[1:-1, 1:-1] = np.diag(coeff_3)

            coeff = coeff1+coeff2+coeff3
            coeff[0,0] = 1+2*factor(self.timestep, self.Tstrap[i,0], dens, dxr,\
                         self.material[i])
            coeff[0,1] = -2*factor(self.timestep, self.Tstrap[i,1], dens, dxr,\
                         self.material[i])
            coeff[-1,-2] = -2.*factor(self.timestep, self.Tstrap[i,-2], dens, dxr, \
                         self.material[i])
            coeff[-1,-1] = 1+2.*factor(self.timestep, self.Tstrap[i,-1], dens,  dxr,\
                         self.material[i])

            k_s = TC.get_conductivity(self.material[i], self.Tstrap[i,-1])
            fact = 2*factor(self.timestep, self.Tstrap[i,-1], dens, \
                          dxr, self.material[i])*dxr/dx/dy/len(contactplate[i][0])**2\
                         /k_s*h
            if plate==True:
                coeff[-1,-1] += fact*len(contactplate[i][0])

            #
            # xx = fmul(2, factor(self.timestep, self.Tstrap[i,-1], dens, \
            #               dxr, self.material[i]))
            # xx = fmul(xx, dxr)
            # xx = fdiv(xx, dx)
            # xx = fdiv(xx, dy)
            # xx = fdiv(xx, len(contactplate[i][0]))
            # xx = fdiv(xx, k_s)
            # xx = fmul(xx, h)
            #print x
            # print xx
            # print fsub(x,xx, exact=True)

            coeff_strap[i*strap_shape[1]:(i+1)*strap_shape[1], \
                        i*strap_shape[1]:(i+1)*strap_shape[1]] = coeff

            if plate==True:
                ind_con = len(Tplate)*contactplate[i][0]+contactplate[i][1]
                #print ind_con
                ind = (i+1)*strap_shape[1]-1
                #print ind
                coeff_plate[ind,ind_con] = -fact

        coeff_strap = sparse.csr_matrix(coeff_strap)

        coeff_plate = sparse.csr_matrix(coeff_plate)

        if plate == True:
            coeff_strap = sparse.hstack((coeff_strap, coeff_plate))

        if shell==True:
            coeff_shell = sparse.lil_matrix((np.size(self.Tstrap),\
                         np.size(Tshell)+2*np.shape(Tshell)[1]))
            coeff_strap = sparse.hstack((coeff_strap, coeff_shell))
        if bottom==True:
            coeff_bottom = sparse.lil_matrix((np.size(self.Tstrap), \
                                              np.size(Tbottom)))
            coeff_strap = sparse.hstack((coeff_strap, coeff_bottom))
        if OTT==True:
            coeff_ott = sparse.lil_matrix((np.size(self.Tstrap), np.size(Tott)\
                        +2*np.shape(Tott)[2]*np.shape(Tott)[0]))
            coeff_strap = sparse.hstack((coeff_strap, coeff_ott))
        if OTB==True:
            coeff_otb = sparse.lil_matrix((np.size(self.Tstrap), np.size(Totb)\
                        +2*np.shape(Totb)[2]*np.shape(Totb)[0]))
            coeff_strap = sparse.hstack((coeff_strap,coeff_otb))

        return coeff_strap

class strapDR(object):

    def __init__(self, Tstrap, material, length, timestep):

        self.Tstrap = Tstrap
        self.material = material
        self.length = length
        self.timestep = timestep

     def factorS(self, dt, T1, T2, dens, d, material):
        k1 = TC.get_conductivity(material, T1)
        k2 = TC.get_conductivity(material, T2)
        k = (k1+k2)/2.
        C = HC.get_heat_cap(material, T1)
        return (k*dt/dens/C/d**2)

    def matrix(self, plate=False, Tplate=None, platematerial=None, dxp=None, \
               dyp=None, contactplate=None, shell=False, Tshell=None, \
               bottom=False, Tbottom=None, OTT=False, Tott=None, OTB=False, Totb=None,\
               DRplate=False, TplateDR=None, platematerialDR=None, dxdr=None, \
               dydr=None, contactplateDR=None):

        strap_shape = np.shape(self.Tstrap)

        coeff_strap = np.zeros((strap_shape[0]*strap_shape[1], \
                                strap_shape[0]*strap_shape[1]))

        if plate == True:
            coeff_plate = sparse.lil_matrix((np.size(self.Tstrap), \
                                             np.size(Tplate)))
        if DRplate == True:
            coeff_plateDR = sparse.lil_matrix((np.size(self.Tstrap), \
                                             np.size(TplateDR)))

        for i in range(len(self.Tstrap)):

            dens = DEN.get_density(self.material[i], np.mean(self.Tstrap[i]))

            dxr = self.length[i]/(strap_shape[1]-1)

            if plate == True:
                Tav = np.mean(Tplate[contactplate[i]])
                hp = RT.ThermContRes(platematerial, self.material, dxp*\
                                    dyp*len(contactplate[i][0]), \
                                    Tav, self.Tstrap[i,-1])
            if DRplate == True:
                Tav = np.mean(TplateDR[contactplateDR[i]])
                hdr = RT.ThermContRes(platematerialDR, self.material, dxdr*\
                                    dydr*len(contactplateDR[i][0]), \
                                    Tav, self.Tstrap[i,0])

            coeff1 = np.zeros((strap_shape[1],strap_shape[1]))
            coeff2 = coeff1.copy()
            coeff3 = coeff1.copy()
            coeff_1 = np.zeros(strap_shape[1]-2)
            coeff_2 = np.zeros(strap_shape[1]-2)
            coeff_3 = np.zeros(strap_shape[1]-2)

            coeff_1[:] = -self.factorS(self.timestep, self.Tstrap[i, 1:-1], self.Tstrap[i, 2:],\
                          dens, dxr, self.material[i])
            coeff_2[:] = -self.factorS(self.timestep, self.Tstrap[i, 1:-1], self.Tstrap[i, :-2],\
                          dens, dxr, self.material[i])
            coeff_3 = 1-coeff_1-coeff_2

            coeff1[1:-1, 2:] = np.diag(coeff_1)
            coeff2[1:-1, :-2] = np.diag(coeff_2)
            coeff3[1:-1, 1:-1] = np.diag(coeff_3)

            coeff1[1:-1, 2:] = np.diag(coeff_1)
            coeff2[1:-1, :-2] = np.diag(coeff_1)
            coeff3[1:-1, 1:-1] = np.diag(coeff_2)

            coeff = coeff1+coeff2+coeff3
            coeff[0,0] = 1+2*factor(self.timestep, self.Tstrap[i,0], dens, dxr,\
                         self.material[i])
            coeff[0,1] = -2*factor(self.timestep, self.Tstrap[i,1], dens, dxr,\
                         self.material[i])
            coeff[-1,-2] = -2*factor(self.timestep, self.Tstrap[i,-2], dens, dxr, \
                         self.material[i])
            coeff[-1,-1] = 1+2*factor(self.timestep, self.Tstrap[i,-1], dens,  dxr,\
                         self.material[i])
            if plate == True:
                k_s = TC.get_conductivity(self.material[i], self.Tstrap[i,-1])
                coeff[-1,-1] += 2*factor(self.timestep, self.Tstrap[i,-1], dens, \
                              dxr, self.material[i])*dxr/dxp/dyp/len(contactplate[i][0])\
                             /k_s*hp
            if DRplate == True:
                k_s = TC.get_conductivity(self.material[i], self.Tstrap[i,0])
                coeff[0,0] += -2*factor(self.timestep, self.Tstrap[i,0], dens, \
                              dxr, self.material[i])*dxr/dxdr/dydr/len(contactplateDR[i][0])\
                             /k_s*hdr

            coeff_strap[i*strap_shape[1]:(i+1)*strap_shape[1], \
                        i*strap_shape[1]:(i+1)*strap_shape[1]] = coeff

            if plate==True:
                ind_con = len(Tplate)*contactplate[i][0]+contactplate[i][1]
                k_s = TC.get_conductivity(self.material[i], self.Tstrap[i,-1])
                coeff_plate[(i+1)*strap_shape[1]-1,ind_con] = -2*factor(\
                                  self.timestep, self.Tstrap[i,-1], dens, \
                                  dxr, self.material[i])*dxr/dxp/dyp/\
                                  k_s*hp/len(contactplate[i][0])**2
            # print 'Tets'
            if DRplate==True:
                ind_con = len(TplateDR)*contactplateDR[i][0]+contactplateDR[i][1]
                k_s = TC.get_conductivity(self.material[i], self.Tstrap[i,0])
                coeff_plateDR[i*strap_shape[1],ind_con] = 2*factor(\
                                  self.timestep, self.Tstrap[i,0], dens, \
                                  dxr, self.material[i])*dxr/dxdr/dydr/\
                                  k_s*hdr/len(contactplateDR[i][0])**2

        coeff_strap = sparse.csr_matrix(coeff_strap)

        if plate == True:
            coeff_strap = sparse.hstack((coeff_strap, coeff_plate))

        if shell==True:
            coeff_shell = sparse.lil_matrix((np.size(self.Tstrap),\
                         np.size(Tshell)+2*np.shape(Tshell)[1]))
            coeff_strap = sparse.hstack((coeff_strap, coeff_shell))
        if bottom==True:
            coeff_bottom = sparse.lil_matrix((np.size(self.Tstrap), \
                                              np.size(Tbottom)))
            coeff_strap = sparse.hstack((coeff_strap, coeff_bottom))
        if OTT==True:
            coeff_ott = sparse.lil_matrix((np.size(self.Tstrap), np.size(Tott)\
                        +2*np.shape(Tott)[2]*np.shape(Tott)[0]))
            coeff_strap = sparse.hstack((coeff_strap, coeff_ott))
        if OTB==True:
            coeff_otb = sparse.lil_matrix((np.size(self.Tstrap), np.size(Totb)\
                        +2*np.shape(Totb)[2]*np.shape(Totb)[0]))
            coeff_strap = sparse.hstack((coeff_strap,coeff_otb))
        if DRplate == True:
            # print np.shape(coeff_plateDR)
            # print np.shape(coeff_strap)
            coeff_strap = sparse.hstack((coeff_strap, coeff_plateDR))
            # print np.shape(coeff_strap)

        # print np.shape(coeff_strap)
        return coeff_strap

class filterplate(object):

    def __init__(self, Tplate, material, radius, thickness, dx, dy, timestep):

        self.Tplate = Tplate
        self.material = material
        self.radius = radius
        self.thickness = thickness
        self.dx = dx
        self.dy = dy
        self.timestep = timestep

    def matrix(self, strap=False, Tstrap=None, strapmaterial=None, \
               contactstrap=None, shell=False, Tshell=None, shellmaterial=None,\
               contactshell=None, bottom=False, Tbottom=None, OTT=False, \
               Tott=None, ottmaterial=None, OTB=False, Totb=None, otbmaterial=None, \
               contactot=None, switch=False, Tswitch=None, contactswitch=None, condswitch=None):

        plate_shape = np.shape(self.Tplate)

        dens = DEN.get_density(self.material, np.mean(self.Tplate))

        index = np.where(self.Tplate == 0)

        T_shape = np.reshape(self.Tplate, np.size(self.Tplate))
        index1, = np.where(T_shape!=0)
        T1 = self.Tplate.copy()
        ar = np.ma.array(T1, mask=T1 < .2)

        #This cycle shifts the outer element of one cell. This means that the
        #load from the outer cell is zero
        for shift in (-1,1):
            for axis in (0,1):
                ar_shifted=np.roll(ar,shift=shift,axis=axis)
                idx=~ar_shifted.mask * ar.mask
                ar[idx]=ar_shifted[idx]

        T_shape1 = np.reshape(T1, np.size(T1))


        a = np.ones(len(T_shape)) #Main Diagonal
        b1 = np.zeros(len(T_shape)-1) #+1 Diagonal
        b2 = np.zeros(len(T_shape)-1) #-1 Diagonal
        c1 = np.zeros(len(T_shape)-plate_shape[0]) #+nx Diagonal
        c2 = np.zeros(len(T_shape)-plate_shape[0]) #-nx Diagonal

        a[index1] = (1+2*factor(self.timestep, T_shape1[index1], dens, self.dx, self.material) +\
                     2*factor(self.timestep, T_shape1[index1], dens, self.dy, self.material))
        b1[index1-1] = -factor(self.timestep, T_shape1[index1], dens, self.dx, self.material)
        b2[index1] = -factor(self.timestep, T_shape1[index1], dens, self.dx, self.material)
        c1[index1-(plate_shape[0])] = -factor(self.timestep, T_shape1[index1], \
                                        dens, self.dy, self.material)
        c2[index1] = -factor(self.timestep, T_shape1[index1], dens, self.dy, self.material)

        if shell == True:
            coeff_shell = np.zeros((plate_shape[0]*plate_shape[1],\
                                         np.size(Tshell)+2*np.shape(Tshell)[1]))
            for i in range(len(contactshell)):
                Tav = np.mean(self.Tplate[contactshell[i]])
                h = RT.ThermContRes(shellmaterial, self.material, self.dx*self.dy*len(contactshell[i][0]), \
                                    Tshell[-1,i], Tav)

                ind_con = len(self.Tplate)*contactshell[i][0]+contactshell[i][1]
                a[ind_con] += factorLoad(self.timestep, T_shape1[ind_con], dens, \
                                         self.material)/self.thickness\
                                         /self.dx/self.dy*h/\
                                         len(contactshell[i][0])

                var = -factorLoad(self.timestep,T_shape1[ind_con], dens, \
                self.material)/self.thickness/self.dx/self.dy*h/\
                len(contactshell[i][0])
                coeff_shell[ind_con, np.shape(Tshell)[1]+i] += var

            coeff_shell = sparse.csr_matrix(coeff_shell)

        if strap == True:
            coeff_strap = np.zeros((plate_shape[0]*plate_shape[1],\
                                             np.size(Tstrap)))

            for i in range(len(contactstrap)):
                Tav = np.mean(self.Tplate[contactstrap[i]])
                h = RT.ThermContRes(strapmaterial[i], self.material, self.dx*\
                                    self.dy*len(contactstrap[i][0]), \
                                    Tstrap[i,-1], Tav)

                ind_con = len(self.Tplate)*contactstrap[i][0]+contactstrap[i][1]
                a[ind_con] += factorLoad(self.timestep, T_shape1[ind_con], dens, \
                              self.material)/self.thickness/self.dx/self.dy/\
                              len(contactstrap[i][0])*h

                # arr = factorLoad(self.timestep, T_shape1[ind_con], dens, \
                #                  self.material)/self.thickness/self.dx/self.dy/\
                #                  len(contactstrap[i][0])**2*h
                # row = np.repeat(ind_con, len(ind_con))
                # column = np.tile(ind_con, len(ind_con))
                # z = np.repeat(arr, len(ind_con))
                # matsp = sparse.coo_matrix((z, (row,column)), shape = (len(T_shape), len(T_shape)))

                off = (i+1)*np.shape(Tstrap)[1]-1
                coeff_strap[ind_con,off] += \
                                  -factorLoad(self.timestep, T_shape1[ind_con], \
                                  dens, self.material)/self.thickness/self.dx/\
                                  self.dy/len(contactstrap[i][0])*h

            coeff_strap = sparse.csr_matrix(coeff_strap)

        if OTT == True:
            coeff_ott = np.zeros((plate_shape[0]*plate_shape[1], \
                                np.size(Tott)+2*np.shape(Tott)[2]*\
                                np.shape(Tott)[0]))

            for i in range(np.shape(Tott)[0]):
                for j in range(np.shape(Tott)[2]):
                    Tav = np.mean(self.Tplate[contactot[i][j]])
                    h = RT.ThermContRes(ottmaterial, self.material, self.dx*\
                                        self.dy*len(contactot[i][j][0]), \
                                        Tott[i,0,j], Tav)

                    ind_con = len(self.Tplate)*contactot[i][j][0]+contactot[i][j][1]
                    a[ind_con] += factorLoad(self.timestep, T_shape1[ind_con], dens, \
                                  self.material)/self.thickness/self.dx/self.dy/\
                                  len(contactot[i][j][0])*h

                    off = np.shape(Tott)[2]*(2*i+1)+i*np.shape(Tott)[2]*\
                          np.shape(Tott)[1]+j
                    # print 'Value'
                    # print ind_con, off

                    coeff_ott[ind_con,off] += \
                                      -factorLoad(self.timestep, T_shape1[ind_con], \
                                      dens, self.material)/self.thickness/self.dx/\
                                      self.dy/len(contactot[i][j][0])*h

            coeff_ott = sparse.csr_matrix(coeff_ott)

        if OTB == True:
            coeff_otb = np.zeros((plate_shape[0]*plate_shape[1], \
                                np.size(Totb)+2*np.shape(Totb)[2]*\
                                np.shape(Totb)[0]))

            for i in range(np.shape(Totb)[0]):
                for j in range(np.shape(Totb)[2]):
                    Tav = np.mean(self.Tplate[contactot[i][j]])
                    h = RT.ThermContRes(otbmaterial, self.material, self.dx*\
                                        self.dy*len(contactot[i][j][0]), \
                                        Totb[i,-1,j], Tav)

                    ind_con = len(self.Tplate)*contactot[i][j][0]+contactot[i][j][1]
                    a[ind_con] += factorLoad(self.timestep, T_shape1[ind_con], dens, \
                                  self.material)/self.thickness/self.dx/self.dy/\
                                  len(contactot[i][j][0])*h

                    off = np.shape(Totb)[2]*(2*i)+(i+1)*np.shape(Totb)[2]*\
                          np.shape(Totb)[1]+j

                    coeff_otb[ind_con,off] += \
                                      -factorLoad(self.timestep, T_shape1[ind_con], \
                                      dens, self.material)/self.thickness/self.dx/\
                                      self.dy/len(contactot[i][j][0])*h

            coeff_otb = sparse.csr_matrix(coeff_otb)

        if switch == True:

            num = np.size(Tswitch)
            for i in range(num):

                if len(condswitch) >1:
                    Tav = np.mean(self.Tplate[contactswitch[i]])
                else:
                    Tav = np.mean(self.Tplate[contactswitch[i]])
                Ts = (Tswitch[i]+Tav)/2.
                if isinstance(condswitch[i], float):
                    C_switch = condswitch[i]
                else:
                    temp_switch_max = np.amax(condswitch[i][:,0])
                    c_switch_min = np.amin(condswitch[i][:,1])
                    temp_switch_min = np.amin(condswitch[i][:,0])
                    temp_arr = condswitch[i][:,0]
                    c_arr = condswitch[i][:,1]
                    if Ts >= temp_switch_max:
                        C_switch = -8.295E-08*Ts**3+2.433E-05*Ts**2+3.314E-03*Ts-5.164E-02
                    elif Ts <= temp_switch_min:
                        C_switch = c_switch_min
                    else:
                        f = interpolate.interp1d(temp_arr, c_arr)
                        C_switch = f((Ts))

                if len(condswitch) >1:
                    ind_mat = len(self.Tplate)*contactswitch[i][0]+contactswitch[i][1]
                    area = self.dx*self.dy*len(contactswitch[i][0])
                else:
                    ind_mat = len(self.Tplate)*contactswitch[i][0]+contactswitch[i][1]
                    area = self.dx*self.dy*len(contactswitch[i][0])
                a[ind_mat] += factorLoad(self.timestep, T_shape1[ind_mat], dens, \
                                 self.material)/self.thickness/area*C_switch

        m1 = sparse.diags(a, 0)
        m2 = sparse.diags(b2, 1)
        m3 = sparse.diags(b1, -1)
        m4 = sparse.diags(c1, -(plate_shape[1]))
        m5 = sparse.diags(c2, (plate_shape[1]))
        m = m1+m2+m3+m4+m5#+matsp

        if bottom ==True:
            coeff_bottom = sparse.lil_matrix((plate_shape[0]*plate_shape[1], \
                                    np.shape(Tbottom)[0]*np.shape(Tbottom)[0]))

        if strap == False:
            coeff_strap = np.zeros((plate_shape[0]*plate_shape[1],\
                                             np.size(Tstrap)))
            coeff_strap = sparse.csr_matrix(coeff_strap)

        coeff_plate = sparse.hstack((coeff_strap, m))
        if shell == True:
            coeff_plate = sparse.hstack((coeff_plate, coeff_shell))
        if bottom == True:
            coeff_plate = sparse.hstack((coeff_plate, coeff_bottom))
        if OTT == True:
            coeff_plate = sparse.hstack((coeff_plate, coeff_ott))
        if OTB == True:
            coeff_plate = sparse.hstack((coeff_plate, coeff_otb))

        return coeff_plate

class DRplate(object):

    def __init__(self, TplateDR, material, radius, thickness, dx, dy, timestep):

        self.TplateDR = TplateDR
        self.material = material
        self.radius = radius
        self.thickness = thickness
        self.dx = dx
        self.dy = dy
        self.timestep = timestep

    def matrix(self, strap=False, Tstrap=None, strapmaterial=None, \
               contactstrap=None, plateCopp=False, TplateCopp=None,shell=False, \
               Tshell=None, \
               bottom=False, Tbottom=None, OTT=False, \
               Tott=None, OTB=False, Totb=None,
               switch=False, Tswitch=None, contactswitch=None, condswitch=None):

        plate_shape = np.shape(self.TplateDR)

        dens = DEN.get_density(self.material, np.mean(self.TplateDR))

        index = np.where(self.TplateDR == 0)

        T_shape = np.reshape(self.TplateDR, np.size(self.TplateDR))
        index1, = np.where(T_shape!=0)
        T1 = self.TplateDR.copy()
        ar = np.ma.array(T1, mask=T1 < .2)

        #This cycle shifts the outer element of one cell. This means that the
        #load from the outer cell is zero
        for shift in (-1,1):
            for axis in (0,1):
                ar_shifted=np.roll(ar,shift=shift,axis=axis)
                idx=~ar_shifted.mask * ar.mask
                ar[idx]=ar_shifted[idx]

        T_shape1 = np.reshape(T1, np.size(T1))


        a = np.ones(len(T_shape)) #Main Diagonal
        b1 = np.zeros(len(T_shape)-1) #+1 Diagonal
        b2 = np.zeros(len(T_shape)-1) #-1 Diagonal
        c1 = np.zeros(len(T_shape)-plate_shape[0]) #+nx Diagonal
        c2 = np.zeros(len(T_shape)-plate_shape[0]) #-nx Diagonal

        a[index1] = (1+2*factor(self.timestep, T_shape1[index1], dens, self.dx, self.material) +\
                     2*factor(self.timestep, T_shape1[index1], dens, self.dy, self.material))
        b1[index1-1] = -factor(self.timestep, T_shape1[index1], dens, self.dx, self.material)
        b2[index1] = -factor(self.timestep, T_shape1[index1], dens, self.dx, self.material)
        c1[index1-(plate_shape[0])] = -factor(self.timestep, T_shape1[index1], \
                                        dens, self.dy, self.material)
        c2[index1] = -factor(self.timestep, T_shape1[index1], dens, self.dy, self.material)

        if shell == True:
            coeff_shell = sparse.lil_matrix((plate_shape[0]*plate_shape[1],\
                                         np.size(Tshell)+2*np.shape(Tshell)[1]))

        if plateCopp == True:
            coeff_plateCopp = sparse.lil_matrix((plate_shape[0]*plate_shape[1],\
                                         np.size(TplateCopp)))

        if strap == True:
            coeff_strap = np.zeros((plate_shape[0]*plate_shape[1],\
                                             np.size(Tstrap)))

            for i in range(len(contactstrap)):
                Tav = np.mean(self.TplateDR[contactstrap[i]])
                h = RT.ThermContRes(strapmaterial[i], self.material, self.dx*\
                                    self.dy*len(contactstrap[i][0]), \
                                    Tstrap[i,0], Tav)

                ind_con = len(self.TplateDR)*contactstrap[i][0]+contactstrap[i][1]
                a[ind_con] += factorLoad(self.timestep, T_shape1[ind_con], dens, \
                              self.material)/self.thickness/self.dx/self.dy/\
                              len(contactstrap[i][0])*h

                off = i*np.shape(Tstrap)[1]
                coeff_strap[ind_con,off] += \
                                  -factorLoad(self.timestep, T_shape1[ind_con], \
                                  dens, self.material)/self.thickness/self.dx/\
                                  self.dy/len(contactstrap[i][0])*h

            coeff_strap = sparse.csr_matrix(coeff_strap)

        if OTT == True:
            coeff_ott = sparse.lil_matrix((plate_shape[0]*plate_shape[1], \
                                np.size(Tott)+2*np.shape(Tott)[2]*\
                                np.shape(Tott)[0]))

        if OTB == True:
            coeff_otb = sparse.lil_matrix((plate_shape[0]*plate_shape[1], \
                                np.size(Totb)+2*np.shape(Totb)[2]*\
                                np.shape(Totb)[0]))

        if switch == True:

            num = np.size(Tswitch)

            for i in range(num):
                if len(condswitch) >1:
                    Tav = np.mean(self.TplateDR[contactswitch[i]])
                    Ts = (Tswitch[i]+Tav)/2.
                else:
                    Tav = np.mean(self.TplateDR[contactswitch[i]])
                    Ts = (Tswitch+Tav)/2.

                if isinstance(condswitch[i], float):
                    C_switch = condswitch[i]
                else:
                    temp_switch_max = np.amax(condswitch[i][:,0])
                    c_switch_min = np.amin(condswitch[i][:,1])
                    temp_switch_min = np.amin(condswitch[i][:,0])
                    temp_arr = condswitch[i][:,0]
                    c_arr = condswitch[i][:,1]
                    if Ts >= temp_switch_max:
                        C_switch = -8.295E-08*Ts**3+2.433E-05*Ts**2+3.314E-03*Ts-5.164E-02
                    elif Ts <= temp_switch_min:
                        C_switch = c_switch_min
                    else:
                        f = interpolate.interp1d(temp_arr, c_arr)
                        C_switch = f((Ts))

                if len(condswitch) >1:
                    ind_mat = len(self.TplateDR)*contactswitch[i][0]+contactswitch[i][1]
                else:
                    ind_mat = len(self.TplateDR)*contactswitch[i][0]+contactswitch[i][1]

                area = self.dx*self.dy*len(contactswitch[i][0])
                a[ind_mat] += factorLoad(self.timestep, T_shape1[ind_mat], dens, \
                                 self.material)/self.thickness/area*C_switch
                # print 'DR Load'
                # print factorLoad(self.timestep, T_shape1[ind_mat], dens, \
                #                  self.material)/self.thickness/area*C_switch

        m1 = sparse.diags(a, 0)
        m2 = sparse.diags(b2, 1)
        m3 = sparse.diags(b1, -1)
        m4 = sparse.diags(c1, -(plate_shape[1]))
        m5 = sparse.diags(c2, (plate_shape[1]))
        m = m1+m2+m3+m4+m5#+matsp

        if bottom ==True:
            coeff_bottom = sparse.lil_matrix((plate_shape[0]*plate_shape[1], \
                                    np.shape(Tbottom)[0]*np.shape(Tbottom)[0]))

        if strap == True:
            coeff_plate = sparse.hstack((coeff_strap, coeff_plateCopp))
        if shell == True:
            coeff_plate = sparse.hstack((coeff_plate, coeff_shell))
        if bottom == True:
            coeff_plate = sparse.hstack((coeff_plate, coeff_bottom))
        if OTT == True:
            coeff_plate = sparse.hstack((coeff_plate, coeff_ott))
        if OTB == True:
            coeff_plate = sparse.hstack((coeff_plate, coeff_otb))
        if plateCopp == True:
            coeff_plate = sparse.hstack((coeff_plate, m))
        # if strap == True:
        #     coeff_plate = sparse.hstack((coeff_strap, m))
        if strap == False:
            coeff_plate = m

        #coeff_plate = sparse.hstack((coeff_strap, m))

        return coeff_plate

class shell(object):

    def __init__(self, Tshell, material, timestep, radius, thickness, length):

        self.Tshell = Tshell
        self.material = material
        self.timestep = timestep
        self.radius = radius
        self.thickness = thickness
        self.length = length

    def fact_nediag(self, T, d, dens):
        return self.timestep*TC.get_conductivity(self.material, T)/\
               dens/HC.get_heat_cap(self.material, T)/d**2/self.radius**2

    def fact_nrdiag(self, T, d, dens):
        return self.timestep*TC.get_conductivity(self.material, T)/\
               dens/HC.get_heat_cap(self.material, T)/d**2

    def matrix(self, strap=False, Tstrap = None, plate=False, Tplate=None, \
               contactplate=None, platematerial=None, dx1=None, dy1=None, \
               bottom=False, Tbottom=None, contactbottom=None, \
               bottommaterial=None, dx2=None, dy2=None, OTT=False, Tott=None, OTB=False, Totb=None):

        nr = np.shape(self.Tshell)[0]
        ne = np.shape(self.Tshell)[1]
        l = nr*ne
        l_arr = np.arange(0, l, 1)

        Tshape = np.reshape(self.Tshell, np.size(self.Tshell))

        dens = DEN.get_density(self.material, np.mean(self.Tshell))
        area = np.pi*(self.radius**2-(self.radius-self.thickness)**2)/ne

        index1 = np.where(l_arr%ne !=0) #-1 and +1 diagonal
        index2 = np.where((l_arr+1)%ne==0)#-ne+1 and ne-1 diagonal

        diag1 = np.zeros(l)
        diag1[index1] = -self.fact_nediag(Tshape[index1], 2*np.pi/ne, dens)
        diag1 = np.append(np.zeros(ne-1), np.append(diag1, np.zeros(ne)))

        diag2 = np.zeros(l)
        diag2[index2] = -self.fact_nediag(Tshape[index2], 2*np.pi/ne, dens)
        diag2 = np.append(np.zeros(1), np.append(diag2, np.zeros(ne)))

        diag3 = -self.fact_nrdiag(Tshape, self.length/nr , dens)
        diag3_inf = np.append(diag3, np.zeros(ne))
        diag3_sup = np.append(np.zeros(ne), diag3)

        diag = 1+2*self.fact_nediag(Tshape, 2*np.pi/ne, dens)+\
               2*self.fact_nrdiag(Tshape, self.length/nr , dens)

        if plate == True:
            plate_shape = np.shape(Tplate)
            coeff_plate = np.zeros(((nr+2)*(ne), plate_shape[0]*\
                                             plate_shape[1]))

            for i in range(len(contactplate)):
                Tav = np.mean(Tplate[contactplate[i]])
                h_k = RT.ThermContRes(platematerial, self.material, dx1*dy1*len(contactplate[i][0]), Tav, \
                                      self.Tshell[0,i])

                diag[i] += factorLoad(self.timestep, self.Tshell[0,i], dens, \
                                     self.material)/dx1/dy1/len(contactplate[i][0])\
                                    /self.length/nr*h_k

                ind_cont = len(Tplate)*contactplate[i][0]+contactplate[i][1]
                coeff_plate[ne+i, ind_cont] += -factorLoad(self.timestep, \
                                       self.Tshell[0,i], dens, self.material)\
                                      /self.length/nr*h_k/dx1/dy1/\
                                      len(contactplate[i][0])**2

            coeff_plate = sparse.csr_matrix(coeff_plate)
        if bottom == True:
            bottom_shape = np.shape(Tbottom)
            coeff_bottom = np.zeros(((nr+2)*(ne), bottom_shape[0]*\
                                             bottom_shape[1]))

            for i in range(len(contactbottom)):
                Tav = np.mean(Tbottom[contactbottom[i]])

                h_k = RT.ThermContRes(bottommaterial, self.material, area, Tav, \
                                      self.Tshell[-1, i])
                diag[-ne+i] += factorLoad(self.timestep, self.Tshell[-1,i], dens,\
                                       self.material)/self.length/nr*h_k/dx2/dy2/\
                                       len(contactbottom[i][0])
                ind_cont = len(Tbottom)*contactbottom[i][0]+contactbottom[i][1]
                coeff_bottom[ne*(nr)+i,ind_cont] += -factorLoad(self.timestep,\
                                       self.Tshell[-1,i], dens, self.material)\
                                      /self.length/nr*h_k/dx2/dy2/\
                                      len(contactplate[i][0])**2

            coeff_bottom = sparse.csr_matrix(coeff_bottom)


        diag = np.append(np.ones(ne), np.append(diag, np.ones(ne)))

        m1 = sparse.diags(diag, 0)
        m2 = sparse.diags(diag1, -1)
        m3 = sparse.diags(diag1, 1)
        m4 = sparse.diags(diag2, ne-1)
        m5 = sparse.diags(diag2, -ne+1)
        m6 = sparse.diags(diag3_inf, -ne)
        m7 = sparse.diags(diag3_sup, ne)

        m = m1+m2+m3+m4+m5+m6+m7

        if strap == True:
            coeff_strap = sparse.lil_matrix(((nr+2)*(ne), np.size(Tstrap)))

        if plate == True:
            coeff_pl = sparse.hstack((coeff_strap, coeff_plate))
            coeff_shell = sparse.hstack((coeff_pl, m))

        if bottom == True:
            coeff_shell = sparse.hstack((coeff_shell, coeff_bottom))

        if OTT==True:
            coeff_ott = sparse.lil_matrix(((nr+2)*(ne), np.size(Tott)\
                        +2*np.shape(Tott)[2]*np.shape(Tott)[0]))
            coeff_shell = sparse.hstack((coeff_shell, coeff_ott))
        if OTB==True:
            coeff_otb = sparse.lil_matrix(((nr+2)*(ne), np.size(Totb)\
                        +2*np.shape(Totb)[2]*np.shape(Totb)[0]))
            coeff_shell = sparse.hstack((coeff_shell,coeff_otb))


        return coeff_shell

class bottomplate(object):

    def __init__(self, Tbottom, material, radius, thickness, dx, dy, timestep):

        self.Tbottom = Tbottom
        self.material = material
        self.radius = radius
        self.thickness = thickness
        self.dx = dx
        self.dy = dy
        self.timestep = timestep

    def matrix(self, strap=False, Tstrap=None, plate=False, Tplate=None, \
               shell=False, Tshell=None, shellmaterial=None, contactshell=None, \
               OTT=False, Tott=None, OTB=False, Totb=None):

        bottom_shape = np.shape(self.Tbottom)

        dens = DEN.get_density(self.material, np.mean(self.Tbottom))

        index = np.where(self.Tbottom == 0)

        T_shape = np.reshape(self.Tbottom, np.size(self.Tbottom))
        T1 = self.Tbottom.copy()
        ar = np.ma.array(T1, mask=T1 < .2)

        #This cycle shifts the outer element of one cell. This means that the
        #load from the outer cell is zero
        for shift in (-1,1):
            for axis in (0,1):
                ar_shifted=np.roll(ar,shift=shift,axis=axis)
                idx=~ar_shifted.mask * ar.mask
                ar[idx]=ar_shifted[idx]

        T_shape1 = np.reshape(T1, np.size(T1))
        index1, = np.where(T_shape!=0)

        a = np.ones(len(T_shape)) #Main Diagonal
        b1 = np.zeros(len(T_shape)-1) #+1 Diagonal
        b2 = np.zeros(len(T_shape)-1) #-1 Diagonal
        c1 = np.zeros(len(T_shape)-bottom_shape[0]) #+nx Diagonal
        c2 = np.zeros(len(T_shape)-bottom_shape[0]) #-nx Diagonal

        a[index1] = (1+2*factor(self.timestep, T_shape1[index1], dens, self.dx, self.material) +\
                     2*factor(self.timestep, T_shape1[index1], dens, self.dy, self.material))
        b1[index1-1] = -factor(self.timestep, T_shape1[index1], dens, self.dx, self.material)
        b2[index1] = -factor(self.timestep, T_shape1[index1], dens, self.dx, self.material)
        c1[index1-(bottom_shape[0])] = -factor(self.timestep, T_shape1[index1], \
                                        dens, self.dy, self.material)
        c2[index1] = -factor(self.timestep, T_shape1[index1], dens, self.dy, self.material)

        if shell == True:
            coeff_shell = np.zeros((bottom_shape[0]*bottom_shape[1],\
                                             np.size(Tshell)+2*np.shape(Tshell)[1]))
            for i in range(len(contactshell)):
                Tav = np.mean(self.Tbottom[contactshell[i]])
                h = RT.ThermContRes(shellmaterial, self.material, self.dx*self.dy*\
                     len(contactshell[i][0]), Tshell[-1,i], Tav)

                ind_con = len(self.Tbottom)*contactshell[i][0]+contactshell[i][1]
                a[ind_con] += factorLoad(self.timestep, T_shape1[ind_con], dens, \
                                         self.material)/self.thickness\
                                         /self.dx/self.dy*h/\
                                         len(contactshell[i][0])
                coeff_shell[ind_con,np.size(Tshell)+i] \
                                     += -factorLoad(self.timestep, \
                                         T_shape1[ind_con], dens, \
                                         self.material)/self.thickness\
                                         /self.dx/self.dy*h/\
                                         len(contactshell[i][0])

            coeff_shell = sparse.csr_matrix(coeff_shell)

        m1 = sparse.diags(a, 0)
        m2 = sparse.diags(b2, 1)
        m3 = sparse.diags(b1, -1)
        m4 = sparse.diags(c1, -(bottom_shape[1]))
        m5 = sparse.diags(c2, (bottom_shape[1]))
        m = m1+m2+m3+m4+m5

        if strap == True:
            coeff_strap = sparse.lil_matrix((bottom_shape[0]*bottom_shape[1],\
                                             np.size(Tstrap)))

        if plate ==True:
            coeff_plate = sparse.lil_matrix((bottom_shape[0]*bottom_shape[1], \
                                    np.shape(Tplate)[0]*np.shape(Tplate)[0]))

        coeff_bottom = sparse.hstack((coeff_strap, coeff_plate))
        coeff_bottom = sparse.hstack((coeff_bottom, coeff_shell))
        coeff_bottom = sparse.hstack((coeff_bottom, m))

        if OTT==True:
            coeff_ott = sparse.lil_matrix((np.size(self.Tbottom), np.size(Tott)\
                        +2*np.shape(Tott)[2]*np.shape(Tott)[0]))
            coeff_bottom = sparse.hstack((coeff_bottom, coeff_ott))
        if OTB==True:
            coeff_otb = sparse.lil_matrix((np.size(self.Tbottom), np.size(Totb)\
                        +2*np.shape(Totb)[2]*np.shape(Totb)[0]))
            coeff_bottom = sparse.hstack((coeff_bottom,coeff_otb))

        return coeff_bottom

class opticstube(object):

    def __init__(self, temperature, radius, number, material, length, \
                 thickness, timestep, pos):

        self.temperature = temperature
        self.radius = radius
        self.timestep=timestep
        self.material = material
        self.length = length
        self.thickness = thickness
        self.number = number
        self.pos = pos


    def fact_nediag(self, T, d, dens):
        return self.timestep*TC.get_conductivity(self.material, T)/\
               dens/HC.get_heat_cap(self.material, T)/d**2/self.radius**2

    def fact_nrdiag(self, T, d, dens):
        return self.timestep*TC.get_conductivity(self.material, T)/\
               dens/HC.get_heat_cap(self.material, T)/d**2

    def matrix(self, straps=False, Tstrap = None, plate=False, Tplate=None, \
               contactplate=None, platematerial=None, shell=False, Tshell=None,\
               dx=None, dy=None, bottom=False, Tbottom=None):

        nr = np.shape(self.temperature[0])[0]
        ne = np.shape(self.temperature[0])[1]

        coeff_opt_sparse = sparse.lil_matrix((np.size(self.temperature)+2*ne*self.number, \
                    np.size(Tstrap)+np.size(Tplate)+(np.size(Tshell)+np.shape(Tshell)[1]*2)+\
                    np.size(Tbottom)+(np.size(self.temperature)+2*ne*self.number)*2))

        for i in range(self.number):
            nr = np.shape(self.temperature[i])[0]
            ne = np.shape(self.temperature[i])[1]
            l = nr*ne
            l_arr = np.arange(0, l, 1)

            Tshape = np.reshape(self.temperature[i], np.size(self.temperature[i]))

            dens = DEN.get_density(self.material, np.mean(self.temperature[i]))
            area = np.pi*(self.radius**2-(self.radius-self.thickness)**2)/ne

            index1 = np.where(l_arr%ne !=0) #-1 and +1 diagonal
            index2 = np.where((l_arr+1)%ne==0)#-ne+1 and ne-1 diagonal

            diag1 = np.zeros(l)
            diag1[index1] = -self.fact_nediag(Tshape[index1], 2*np.pi/ne, dens)
            diag1 = np.append(np.zeros(ne-1), np.append(diag1, np.zeros(ne)))

            diag2 = np.zeros(l)
            diag2[index2] = -self.fact_nediag(Tshape[index2], 2*np.pi/ne, dens)
            diag2 = np.append(np.zeros(1), np.append(diag2, np.zeros(ne)))

            diag3 = -self.fact_nrdiag(Tshape, self.length/nr , dens)
            diag3_inf = np.append(diag3, np.zeros(ne))
            diag3_sup = np.append(np.zeros(ne), diag3)

            diag = 1+2*self.fact_nediag(Tshape, 2*np.pi/ne, dens)+\
                   2*self.fact_nrdiag(Tshape, self.length/nr , dens)

            if plate == True:
                for j in range(len(contactplate[i])):
                    ind_cont = len(Tplate)*contactplate[i][j][0]+contactplate[i][j][1]
                    if self.pos.lower() == 'top':
                        temp = self.temperature[i,0,j]
                        ind_opt = np.shape(self.temperature)[2]*(2*i+1)+i*np.shape(self.temperature)[2]*\
                                  np.shape(self.temperature)[1]+j
                        diag_ind = j
                    if self.pos.lower() == 'bottom':
                        temp = self.temperature[i,-1,j]
                        offset = np.size(self.temperature)+np.shape(self.temperature)[2]*\
                                 np.shape(self.temperature)[0]*2
                        ind_opt = np.shape(self.temperature)[2]*(2*i)+(i+1)*np.shape(self.temperature)[2]*\
                                  np.shape(self.temperature)[1]+j
                        diag_ind = j+ne*(nr-1)
                        # print diag_ind
                    Tav = np.mean(Tplate[contactplate[i][j]])
                    h_k = RT.ThermContRes(platematerial, self.material, area, Tav, \
                                          temp)
                    diag[diag_ind] += factorLoad(self.timestep, temp, dens, \
                                         self.material)/dx/dy/len(contactplate[i][j][0])\
                                        /self.length/nr*h_k

                    arr = -factorLoad(self.timestep, \
                            temp, dens, self.material)\
                           /self.length/nr*h_k/dx/dy/\
                           len(contactplate[i][j][0])**2

                    column = ind_cont+np.size(Tstrap)
                    row = np.ones(len(ind_cont))*ind_opt
                    data = np.ones(len(ind_cont))*arr

                    coeff_opt_sparse += sparse.coo_matrix((data, (row, column)), \
                                        shape = (np.size(self.temperature)+2*ne*self.number, \
                                        np.size(Tstrap)+np.size(Tplate)+(np.size(Tshell)+np.shape(Tshell)[1]*2)+\
                                        np.size(Tbottom)+(np.size(self.temperature)+2*ne*self.number)*2))

            diag = np.append(np.ones(ne), np.append(diag, np.ones(ne)))

            m1 = sparse.diags(diag, 0)
            m2 = sparse.diags(diag1, -1)
            m3 = sparse.diags(diag1, 1)
            m4 = sparse.diags(diag2, ne-1)
            m5 = sparse.diags(diag2, -ne+1)
            m6 = sparse.diags(diag3_inf, -ne)
            m7 = sparse.diags(diag3_sup, ne)

            m = m1+m2+m3+m4+m5+m6+m7

            if i ==0:
                mat_diag = m
            else:
                mat_diag = sparse.block_diag((mat_diag,m))

            # if self.pos.lower() == 'top':
            #     off = np.size(Tstrap)+np.size(Tplate)+(np.size(Tshell)+\
            #           2*np.shape(Tshell)[1])+np.size(Tbottom)
            # elif self.pos.lower() == 'bottom':
            #     off = np.size(Tstrap)+np.size(Tplate)+(np.size(Tshell)+\
            #           2*np.shape(Tshell)[1])+\
            #           np.size(Tbottom)+np.size(self.temperature)+2*ne*self.number
            # print 'Test'
            # print i*(2+nr)*ne
            # print (i+1)*(2+nr)*ne-1
            # print off+i*(2+nr)*ne
            # print (i+1)*(2+nr)*ne-1+off
            # print m.toarray()
            # coeff_opt[i*(2+nr)*ne:(i+1)*(2+nr)*ne, off+i*(2+nr)*ne:(i+1)*(2+nr)*ne+off] = m.toarray()
        #print np.shape(mat_diag)
        if self.pos.lower() == 'top':
            spars1 = sparse.lil_matrix((np.shape(mat_diag)[0],np.size(Tstrap)+\
                     np.size(Tplate)+(np.size(Tshell)+2*np.shape(Tshell)[1])+\
                     np.size(Tbottom)))
            spars2 = sparse.lil_matrix((np.shape(mat_diag)[0], \
                                        np.shape(mat_diag)[1]))

            mat_diag = sparse.hstack((spars1, mat_diag))
            mat_diag = sparse.hstack((mat_diag, spars2))

        elif self.pos.lower() == 'bottom':
            spars1 = sparse.lil_matrix((np.shape(mat_diag)[0],np.size(Tstrap)+\
                     np.size(Tplate)+(np.size(Tshell)+2*np.shape(Tshell)[1])+\
                     np.size(Tbottom)+np.size(self.temperature)+2*ne*self.number))

            mat_diag = sparse.hstack((spars1, mat_diag))

        mat = mat_diag+coeff_opt_sparse

        return mat

class switch(object):

    def __init__(self, number, Tplate1, mat1, thick1, Tplate2, \
                 mat2, thick2, contact1, contact2, dx1, dy1, dx2, dy2, condswitch):

        self.number = number
        self.Tplate1 = Tplate1
        self.mat1 = mat1
        self.thick1 = thick1
        self.Tplate2 = Tplate2
        self.thick2 = thick2
        self.mat2 = mat2
        self.contact1 = contact1
        self.contact2 = contact2
        self.dx1 = dx1
        self.dy1 = dy1
        self.dx2 = dx2
        self.dy2 = dy2
        self.condswitch = condswitch

    def matrix(self, timestep):

        T_shape1 = np.reshape(self.Tplate1, np.size(self.Tplate1))
        T_shape2 = np.reshape(self.Tplate2, np.size(self.Tplate2))

        matrix1 = sparse.coo_matrix((np.size(self.Tplate1), np.size(self.Tplate2)))
        matrix2 = sparse.coo_matrix((np.size(self.Tplate2), np.size(self.Tplate1)))

        if self.number == 1:


            ind_mat1 = len(self.Tplate1)*self.contact1[0][0]+self.contact1[0][1]
            ind_mat2 = len(self.Tplate2)*self.contact2[0][0]+self.contact2[0][1]

            Tav1 = np.mean(self.Tplate1[self.contact1[0]])
            Tav2 = np.mean(self.Tplate2[self.contact2[0]])
            Ts = (Tav2+Tav1)/2.

            # print 'Test Temp'
            # print Ts

            if isinstance(self.condswitch, float):
                C_switch = self.condswitch
            else:
                if Ts >= np.max(self.condswitch[0][:,0]):
                    cond = -8.295E-08*Ts**3+2.433E-05*Ts**2+3.314E-03*Ts-5.164E-02
                elif Ts <= np.min(self.condswitch[0][:,0]):
                    cond = np.min(self.condswitch[0][:,1])
                else:
                    f = interpolate.interp1d(self.condswitch[0][:,0], self.condswitch[0][:,1])
                    cond = f((Ts))

            for i in range(len(ind_mat1)):
                dens = DEN.get_density(self.mat1, np.mean(self.Tplate1))
                arr = -factorLoad(timestep, \
                                      T_shape1[ind_mat1[i]], dens, \
                                      self.mat1)/self.thick1/self.dx1/self.dy1/\
                                      len(self.contact1[0][0])*cond/len(self.contact2[0][0])
                arr = np.ones(len(ind_mat2))*arr
                row = np.ones(len(ind_mat2))*ind_mat1[i]
                mat_spar1 = sparse.coo_matrix((arr, (row,ind_mat2)), \
                            shape = (np.size(self.Tplate1), np.size(self.Tplate2)))
                z = mat_spar1.toarray()
                # print 'On the 1K'
                # print z.sum()
                matrix1 += mat_spar1

            for i in range(len(ind_mat2)):
                dens = DEN.get_density(self.mat2, np.mean(self.Tplate2))
                arr = -factorLoad(timestep, \
                                      T_shape2[ind_mat2[i]], dens, \
                                      self.mat2)/self.thick2/self.dx2/self.dy2/\
                                      len(self.contact1[0][0])*cond/len(self.contact2[0][0])

                arr = np.ones(len(ind_mat1))*arr
                row = np.ones(len(ind_mat1))*ind_mat2[i]
                mat_spar2 = sparse.coo_matrix((arr, (row,ind_mat1)), \
                            shape = (np.size(self.Tplate2), np.size(self.Tplate1)))

                z = mat_spar2.toarray()
                # print 'On the 4K'
                # print z.sum()

                matrix2 += mat_spar2

        else:

            for i in range(self.number):

                # print '2 Switch'
                # print len(self.contact1[i][0])
                # print len(self.contact1[i][1])

                ind_mat1 = len(self.Tplate1)*self.contact1[i][0]+self.contact1[i][1]
                ind_mat2 = len(self.Tplate2)*self.contact2[i][0]+self.contact2[i][1]

                Tav1 = np.mean(self.Tplate1[self.contact1[i]])
                Tav2 = np.mean(self.Tplate2[self.contact2[i]])
                Ts = (Tav2+Tav1)/2.
                # print Ts

                if isinstance(self.condswitch[i], float):
                    C_switch = self.condswitch[i]
                else:
                    temp_switch_max = np.amax(self.condswitch[i][:,0])
                    temp_switch_min = np.amin(self.condswitch[i][:,0])
                    c_switch_min = np.amin(self.condswitch[i][:,1])
                    temp_arr = self.condswitch[i][:,0]
                    c_arr = self.condswitch[i][:,1]
                    if Ts >= temp_switch_max:
                        C_switch = -8.295E-08*Ts**3+2.433E-05*Ts**2+3.314E-03*Ts-5.164E-02
                    elif Ts <= temp_switch_min:
                        C_switch = c_switch_min
                    else:
                        f = interpolate.interp1d(temp_arr, c_arr)
                        C_switch = f((Ts))

                for j in range(len(ind_mat1)):
                    dens = DEN.get_density(self.mat1, np.mean(self.Tplate1))
                    arr = -factorLoad(timestep, \
                                      T_shape1[ind_mat1[i]], dens, \
                                      self.mat1)/self.thick1/self.dx1/self.dy1/\
                                      len(self.contact1[i][0])*C_switch/len(self.contact2[i][0])

                    arr = np.ones(len(ind_mat2))*arr
                    row = np.ones(len(ind_mat2))*ind_mat1[j]
                    mat_spar = sparse.coo_matrix((arr, (row,ind_mat2)), \
                                shape = (np.size(self.Tplate1), np.size(self.Tplate2)))
                    matrix1 += mat_spar

                for j in range(len(ind_mat2)):
                    dens = DEN.get_density(self.mat2, np.mean(self.Tplate2))
                    arr = -factorLoad(timestep, \
                                          T_shape2[ind_mat2[i]], dens, \
                                          self.mat2)/self.thick2/self.dx2/self.dy2/\
                                          len(self.contact1[i][0])*C_switch/len(self.contact2[i][0])

                    arr = np.ones(len(ind_mat1))*arr
                    row = np.ones(len(ind_mat1))*ind_mat2[j]
                    mat_spar = sparse.coo_matrix((arr, (row,ind_mat1)), \
                                shape = (np.size(self.Tplate2), np.size(self.Tplate1)))
                    matrix2 += mat_spar

        return matrix1, matrix2
