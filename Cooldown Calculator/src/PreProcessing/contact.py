import numpy as np
import os
import matplotlib.pyplot as plt
import time


#class for contacts between circumference and rod
class contactCRod(object):

    def __init__(self, radius1, radius2, pos2):

        self.radius1 = radius1
        self.radius2 = radius2

        self.pos2 = pos2

    def contact(self, matrix, name, plot=False):

        nx1 = np.shape(matrix)[1]
        ny1 = np.shape(matrix)[0]

        dx = 2*self.radius1/(nx1-3)
        dy = 2*self.radius1/(ny1-3)

        x = np.linspace(-self.radius1-dx, self.radius1+dx, nx1)
        y = np.linspace(self.radius1+dy, -self.radius1-dy, ny1)

        xx, yy = np.meshgrid(x,y)

        index = np.where((np.sqrt(xx**2+yy**2)<=self.radius1) & \
                         (np.sqrt((xx-self.pos2[0])**2+(yy-self.pos2[1])**2)\
                         <=self.radius2) & (matrix >= 0.1))
        if plot == True:
            mat = matrix.copy()
            fig = plt.figure()
            max_m = np.amax(matrix)
            mat[index] = max_m*2
            print max_m
            ar = np.ma.array(mat, mask = mat<=+1)
            plt.imshow(ar)
            fig.savefig('contacts/'+name+'.png')
            plt.close()

        return index

#class for contacts between a circumference and a ring
class contactCRing(object):

    def __init__(self, radius1, radius2, pos2):

        self.radius1 = radius1
        self.radius2 = radius2                      #External Diameter

        self.pos2 = pos2

    def contact(self, matrix, ne, name, plot=False):

        nx = np.shape(matrix)[1]
        ny = np.shape(matrix)[0]

        dx = 2*self.radius1/(nx-3)
        dy = 2*self.radius1/(ny-3)

        x = np.linspace(-self.radius1-dx, self.radius1+dx, nx)
        y = np.linspace(self.radius1+dy, -self.radius1-dy, ny)

        xx, yy = np.meshgrid(x,y)

        xmin = np.zeros(ne)
        ymin = np.zeros(ne)
        xmax = np.zeros(ne)
        ymax = np.zeros(ne)

        for k in range(ne-1):

            x1 = self.radius1*np.sin(2*np.pi/ne*k)
            x2 = self.radius1*np.sin(2*np.pi/ne*(k+1))
            x3 = self.radius2*np.sin(2*np.pi/ne*k)
            x4 = self.radius2*np.sin(2*np.pi/ne*(k+1))
            xmin[k] = np.amin(np.array([x1,x2,x3,x4]))
            xmax[k] = np.amax(np.array([x1,x2,x3,x4]))
            if 2*np.pi/ne*k < np.pi/2 and 2*np.pi/ne*(k+1) > np.pi/2:
                xmax[k] = self.radius1
            elif 2*np.pi/ne*k < 3./2.*np.pi and 2*np.pi/ne*(k+1) > 3./2.*np.pi:
                xmin[k] = -self.radius1

            y1 = self.radius1*np.cos(2*np.pi/ne*k)
            y2 = self.radius1*np.cos(2*np.pi/ne*(k+1))
            y3 = self.radius2*np.cos(2*np.pi/ne*k)
            y4 = self.radius2*np.cos(2*np.pi/ne*(k+1))
            ymin[k] = np.amin(np.array([y1,y2,y3,y4]))
            ymax[k] = np.amax(np.array([y1,y2,y3,y4]))

        x1 = self.radius1*np.sin(2*np.pi/ne*0)
        x3 = self.radius2*np.sin(2*np.pi/ne*0)
        y1 = self.radius1*np.cos(2*np.pi/ne*0)
        y3 = self.radius2*np.cos(2*np.pi/ne*0)

        xmin[-1] = np.amin(np.array([x1,x2,x3,x4]))
        xmax[-1] = np.amax(np.array([x1,x2,x3,x4]))
        ymin[-1] = np.amin(np.array([y1,y2,y3,y4]))
        ymax[-1] = np.amax(np.array([y1,y2,y3,y4]))

        index = []

        for i in range(ne):
            index_e = np.where((np.sqrt(xx**2+yy**2)<=self.radius1) & \
                                  (np.sqrt((xx-self.pos2[0])**2+\
                                  (yy-self.pos2[1])**2)>=self.radius2) &
                                  (xx>= xmin[i]) & (xx<=xmax[i]) & \
                                  (yy>= ymin[i]) & (yy<=ymax[i]))
            index.append(index_e)
            if plot == True:
                mat = matrix.copy()
                fig = plt.figure()
                max_m = np.amax(matrix)
                mat[index_e] = max_m*2
                ar = np.ma.array(mat, mask = mat<=1)
                plt.imshow(ar)
                fig.savefig('contacts/'+name+str(i)+'.png')
                plt.close()

        return index

#class for contacts between a circumference and a ring (inside the circumference)
class contactCRingInt(object):

    def __init__(self, radius, radius1, radius2, pos2):

        self.radius = radius
        self.radius1 = radius1
        self.radius2 = radius2                      #External Diameter

        self.pos2 = pos2

    def contact(self, matrix, ne, name, plot=False):

        nx = np.shape(matrix)[1]
        ny = np.shape(matrix)[0]

        dx = 2*self.radius/(nx-3)
        dy = 2*self.radius/(ny-3)

        x = np.linspace(-self.radius-dx, self.radius+dx, nx)
        y = np.linspace(self.radius+dy, -self.radius-dy, ny)

        xx, yy = np.meshgrid(x,y)

        xmin = np.zeros(ne)
        ymin = np.zeros(ne)
        xmax = np.zeros(ne)
        ymax = np.zeros(ne)

        for k in range(ne-1):

            x1 = self.radius1*np.sin(2*np.pi/ne*k)+self.pos2[0]
            x2 = self.radius1*np.sin(2*np.pi/ne*(k+1))+self.pos2[0]
            x3 = self.radius2*np.sin(2*np.pi/ne*k)+self.pos2[0]
            x4 = self.radius2*np.sin(2*np.pi/ne*(k+1))+self.pos2[0]
            xmin[k] = np.amin(np.array([x1,x2,x3,x4]))
            xmax[k] = np.amax(np.array([x1,x2,x3,x4]))
            if 2*np.pi/ne*k < np.pi/2 and 2*np.pi/ne*(k+1) > np.pi/2:
                xmax[k] = self.radius
            elif 2*np.pi/ne*k < 3./2.*np.pi and 2*np.pi/ne*(k+1) > 3./2.*np.pi:
                xmin[k] = -self.radius

            y1 = self.radius1*np.cos(2*np.pi/ne*k)+self.pos2[1]
            y2 = self.radius1*np.cos(2*np.pi/ne*(k+1))+self.pos2[1]
            y3 = self.radius2*np.cos(2*np.pi/ne*k)+self.pos2[1]
            y4 = self.radius2*np.cos(2*np.pi/ne*(k+1))+self.pos2[1]
            ymin[k] = np.amin(np.array([y1,y2,y3,y4]))
            ymax[k] = np.amax(np.array([y1,y2,y3,y4]))

        x1 = self.radius1*np.sin(2*np.pi/ne*0)+self.pos2[0]
        x3 = self.radius2*np.sin(2*np.pi/ne*0)+self.pos2[0]
        y1 = self.radius1*np.cos(2*np.pi/ne*0)+self.pos2[1]
        y3 = self.radius2*np.cos(2*np.pi/ne*0)+self.pos2[1]

        xmin[-1] = np.amin(np.array([x1,x2,x3,x4]))
        xmax[-1] = np.amax(np.array([x1,x2,x3,x4]))
        ymin[-1] = np.amin(np.array([y1,y2,y3,y4]))
        ymax[-1] = np.amax(np.array([y1,y2,y3,y4]))

        index = []

        for i in range(ne):

            index_e = np.where((np.sqrt(xx**2+yy**2)<=self.radius) & \
                                  (np.sqrt((xx-self.pos2[0])**2+\
                                  (yy-self.pos2[1])**2)<=self.radius1) & \
                                  (np.sqrt((xx-self.pos2[0])**2+\
                                  (yy-self.pos2[1])**2)>=self.radius2) &
                                  (xx>= xmin[i]) & (xx<=xmax[i]) & \
                                  (yy>= ymin[i]) & (yy<=ymax[i]))

            index.append(index_e)
            if plot==True:
                mat = matrix.copy()
                fig = plt.figure()
                max_m = np.amax(matrix)
                mat[index_e] = max_m*2
                ar = np.ma.array(mat, mask = mat<=+1)
                plt.imshow(ar)
                fig.savefig('contacts/'+name+str(i)+'.png')
                plt.close()

        return index
