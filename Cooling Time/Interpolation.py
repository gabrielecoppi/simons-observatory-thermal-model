import numpy as np
import matplotlib.pyplot as plt

from scipy import spatial, interpolate

import os

T1, T2, L1, L2 = np.loadtxt('PT410.txt', unpack='True') #Capacity Map of the PT415
T_415 = np.vstack((T1, T2)).T
Q_415 = np.vstack((L1, L2)).T

T11 = np.linspace(np.min(T1), np.max(T1), 300)
T22 = np.linspace(np.min(T2), np.max(T2), 300)

T111, T222 = np.meshgrid(T11, T22)

Q1 = np.zeros((len(T11), len(T22)))
Q2 = np.zeros((len(T11), len(T22)))

for i in range(len(T11)):
    for j in range(len(T22)):
        pT = [T11[i], T22[j]]
        distance, index = spatial.KDTree(T_415).query(pT)

        Q1[i][j] = L1[index]
        Q2[i][j] = L2[index]


Q1i = interpolate.griddata((T1, T2), L1, (T111, T222), method = 'cubic')
Q2i = interpolate.griddata((T1, T2), L2, (T111, T222), method = 'cubic')

Qf1 = Q1i-Q1
Qf2 = Q2i-Q2

# Qf11 = np.ma.array(Qf1, mask= np.abs(Qf1) <=60)
# Qf22 = np.ma.array(Qf2, mask= np.abs(Qf2) <=60)
