import numpy as np
import os
import shutil
import sys
import ConfigParser

from cryolib import HeatCapacity as HC
from cryolib import conductivity as TC
from cryolib import Ray_Tracing as RT
from cryolib import loading_sim as LS
from subprocess import call

from cooling import coolers as CL

from scipy import spatial, interpolate

from mpi4py import MPI

# MPI stuff

comm = MPI.COMM_WORLD
rank = comm.rank
nproc = comm.size
comm.Barrier()

conf_name = sys.argv[1]
filepath = 'config/'+conf_name
filtersfile = filepath+'/filters.cfg'
loadingfile = filepath+'/loading.cfg'
massesfile = filepath+'/masses.cfg'

model = ConfigParser.ConfigParser()
model.read(massesfile)
sections = model.sections()

M = np.zeros(len(sections)-1)
mat = [[] for k in range(len(sections)-1)]
i = 0
ns = len(sections)-1

Tmin = np.zeros(ns-1)
for sec in sections:
    options = model.options(sec)
    if sec.lower() == 'coolers':
        cl = len(options)
        cool = [[] for k in range(cl)]
        if ns == 6:
            cool[0] = str(model.get(sec,'80K Stage').split('#')[0])
            cool[1] = str(model.get(sec,'40K Stage').split('#')[0])
            cool[2] = str(model.get(sec,'4K Stage').split('#')[0])
            cool[3] = str(model.get(sec,'1K Stage').split('#')[0])
            cool[4] = str(model.get(sec,'100mK Stage').split('#')[0])
        elif ns ==5:
            cool[0] = str(model.get(sec,'40K Stage').split('#')[0])
            cool[1] = str(model.get(sec,'4K Stage').split('#')[0])
            cool[2] = str(model.get(sec,'1K Stage').split('#')[0])
            cool[3] = str(model.get(sec,'100mK Stage').split('#')[0])
    else:
        M[i] = float(model.get(sec,'Mass').split('#')[0])
        mat[i] = (model.get(sec,'Material').split('#')[0])
        i+=1

rho = 2700.
dt = 10.
Ti = np.ones([ns, 3])*300.
outdir = 'stacks/'+conf_name+'/'
if os.path.isfile(outdir+'40K_Temp.txt') == True:
    i = 0
    print 'ciao'
    if os.path.isfile(outdir+'80K_Temp.txt') == True:
        t1, T80_1, T80_2, T80_3 = np.loadtxt(outdir+'80K_Temp.txt', unpack = True)
        T80 = np.vstack((T80_1, T80_2, T80_3)).T
        i = 1
        Ti[i,:]=T80[-1]
    t1, T40_1, T40_2, T40_3 = np.loadtxt(outdir+'40K_Temp.txt', unpack = True)
    T40 = np.vstack((T40_1, T40_2, T40_3)).T
    t1, T4_1, T4_2, T4_3 = np.loadtxt(outdir+'4K_Temp.txt', unpack = True)
    T4 = np.vstack((T4_1, T4_2, T4_3)).T
    t1, T1_1, T1_2, T1_3 = np.loadtxt(outdir+'1K_Temp.txt', unpack = True)
    T1 = np.vstack((T1_1, T1_2, T1_3)).T
    t1, T100_1, T100_2, T100_3 = np.loadtxt(outdir+'100mK_Temp.txt', unpack = True)
    T100 = np.vstack((T100_1, T100_2, T100_3)).T

    Ti[0,:]=np.ones([3])*300.
    Ti[1+i,:]=T40[-1]
    Ti[2+i,:]=T4[-1]
    Ti[3+i,:]=T1[-1]
    Ti[4+i,:]=T100[-1]
    tf = t1[-1]
    t = np.array([])
    t = np.append(t, t1)

else:
    i = 0
    print 'New Temperature files created'
    if ns ==6:
        T80 = np.array([300., 300., 300.])
        i = 1
        Ti[i,:]=T80

    T40 = np.array([300., 300., 300.])
    T4 = np.array([300., 300., 300.])
    T1 = np.array([300., 300., 300.])
    T100 = np.array([300., 300., 300.])
    tf = 0.
    t = np.array([])
    t = np.append(t, tf)
    Ti = np.ones([ns, 3])*300.
    Ti[0,:]=np.ones([3])*300.
    Ti[1+i,:]=T40
    Ti[2+i,:]=T4
    Ti[3+i,:]=T1
    Ti[4+i,:]=T100

Tf = Ti

m_CH = [1., 1., 1., 1.]
m_HS = [25., 13., 10., 6.]

HeatLoad = np.ones(ns-1)*10000.
Q = np.zeros(ns-1)

print 'Time', tf, 's'
print '300K Stage', Tf[0,:]
if ns == 6:
    print '80K Stage', Tf[1,:]
    print '40K Stage', Tf[2,:]
    print '4K Stage', Tf[3,:]
    print '1K Stage', Tf[4,:]
    print '100mK Stage', Tf[5,:]
    Tmin[0] = 30.
    Tmin[1] = CL.DoubleStageMin(int(cool[1][2:]))[0]
    Tmin[2] = CL.DoubleStageMin(int(cool[2][2:]))[1]
    Tmin[3] = CL.DoubleStageMin(int(cool[3][2:]))[0]
    Tmin[4] = CL.DoubleStageMin(int(cool[4][2:]))[1]
elif ns ==5:
    print '40K Stage', Tf[1,:]
    print '4K Stage', Tf[2,:]
    print '1K Stage', Tf[3,:]
    print '100mK Stage', Tf[4,:]
    Tmin[0] = CL.DoubleStageMin(int(cool[0][2:]))[0]
    Tmin[1] = CL.DoubleStageMin(int(cool[1][2:]))[1]
    Tmin[2] = CL.DoubleStageMin(int(cool[2][2:]))[0]
    Tmin[3] = CL.DoubleStageMin(int(cool[3][2:]))[1]



outdir = 'stacks/'+conf_name+'/'
#Filter Transfer Factor Calculations
if os.path.isfile(outdir+'Ray_Tracing.txt') == True and os.path.isfile(outdir+'Ray_Tracing_shape.txt') == True:
    print 'Ray Tracing has been already done (Check config file for info)'
else:
    RT.Ray(comm, filtersfile)

count = 0

while np.any(np.greater(Tf[1:,2],Tmin)) == True:

    if np.any(np.greater(Ti[1:3,2], 100.)):

        if (t[-1]/3600.)%2 == 0 and tf >=100:

            T11 = str(Ti[0,2])
            if ns == 5:
                T22 = str(Ti[1,2])
                T33 = str(Ti[2,2])
                T44 = str(Ti[3,2])
                T55 = str(Ti[4,2])
                call(['mpirun', '-np', '4', 'python', 'cryolib/Thermalization.py', filtersfile, str(40), T11, T22, T33, T44, T55])

            elif ns == 6:
                T22 = str(Ti[1,2])
                T33 = str(Ti[2,2])
                T44 = str(Ti[3,2])
                T55 = str(Ti[4,2])
                T66 = str(Ti[5,2])
                call(['mpirun', '-np', '4', 'python', 'cryolib/Thermalization.py', filtersfile, str(80), T11, T22, T33, T44, T55, T66])

            Load = LS.Loading(Ti[1:,2], loadingfile)
            if ns == 5:
                with open(outdir+'Load.txt', 'a+') as load:
                    load.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[1,2], Tf[2,2], Tf[3,2], \
                                Tf[4,2], Load[0], Load[1], Load[2], Load[3]))
            elif ns ==6:
                with open(outdir+'Load.txt', 'a+') as load:
                    load.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[1,2], Tf[2,2], Tf[3,2], \
                                Tf[4,2], Tf[5,2], Load[0], Load[1], Load[2], Load[3], Load[4]))
        else:
            print
    else:
        if (t[-1]/900.)%2 == 0 and tf >=100:

            T11 = str(Ti[0,2])
            if ns == 5:
                T22 = str(Ti[1,2])
                T33 = str(Ti[2,2])
                T44 = str(Ti[3,2])
                T55 = str(Ti[4,2])
                call(['mpirun', '-np', '4', 'python', 'cryolib/Thermalization.py', filtersfile, str(40), T11, T22, T33, T44, T55])

            elif ns == 6:
                T22 = str(Ti[1,2])
                T33 = str(Ti[2,2])
                T44 = str(Ti[3,2])
                T55 = str(Ti[4,2])
                T66 = str(Ti[5,2])
                call(['mpirun', '-np', '4', 'python', 'cryolib/Thermalization.py', filtersfile, str(80), T11, T22, T33, T44, T55, T66])

            Load = LS.Loading(Ti[1:,2], loadingfile)
            if ns == 5:
                with open(outdir+'Load.txt', 'a+') as load:
                    load.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[1,2], Tf[2,2], Tf[3,2], \
                                Tf[4,2], Load[0], Load[1], Load[2], Load[3]))
            elif ns ==6:
                with open(outdir+'Load.txt', 'a+') as load:
                    load.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[1,2], Tf[2,2], Tf[3,2], \
                                Tf[4,2], Tf[5,2], Load[0], Load[1], Load[2], Load[3], Load[4]))
        else:
            print
    if 'Load' in locals():
        print
    else:
        if tf <= 3700.:
            Load = np.zeros(ns-1)
            print 'Test'
        else:
            print 'else'
            loadfile = np.loadtxt(outdir+'Load.txt')
            print len(loadfile[0])
            if len(loadfile[0])-1 == 8:
                Load = loadfile[-1,5:]
            elif len(loadfile[0])-1 == 10:
                Load = loadfile[-1,6:]

    print Ti
    for i in range(ns):
        if i == 0:
            Tf[i,:] = 300.
        else:
            if model.options(sections[0])[i-1].lower() == '80k stage':
                Q[i-1] = CL.PT90(Ti[i][0])
                R_k = .3
            elif model.options(sections[0])[i-1].lower() == '40k stage':
                Q[i-1] = CL.DoubleStage(Ti[i][0], Ti[i+1][0], int(cool[i-1][2:]))[0]
                Q[i] = CL.DoubleStage(Ti[i][0], Ti[i+1][0], int(cool[i-1][2:]))[1]
                R_k = .3
            elif model.options(sections[0])[i-1].lower() == '4k stage':
                R_k = .7
            elif model.options(sections[0])[i-1].lower() == '1k stage':
                Q[i-1] = CL.DoubleStage(Ti[i][0], Ti[i+1][0], int(cool[i-1][2:]))[0]
                Q[i] = CL.DoubleStage(Ti[i][0], Ti[i+1][0], int(cool[i-1][2:]))[1]
                R_k = 3.0
            elif model.options(sections[0])[i-1].lower() == '100mk stage':
                R_k = 3.0

            for k in range(3):

                if k == 0:
                    HeatLoad[i-1] = (Ti[i, k+1]-Ti[i, k])/R_k
                    fact = (dt/(HC.get_heat_cap('Copper_OFHC', Ti[i,k])*m_CH[i-1]))*(Q[i-1]+HeatLoad[i-1])

                    if Ti[i, k]+fact >= Tmin[i-1] and Ti[i, k]+fact <= Ti[i, k]:
                        Tf[i, k] = Ti[i, k]+fact
                    elif Ti[i, k]+fact >= Tmin[i-1] and Ti[i, k]+fact >= Ti[i, k]:
                        Tf[i,k] = Ti[i,k]
                    else:
                        Tf[i, k] = Tmin[i-1]

                if k == 1:
                    fact = (dt/(HC.get_heat_cap('Copper_OFHC', Ti[i,k])*m_HS[i-1]))*((Ti[i, k+1]-Ti[i, k])/R_k+(Ti[i, k-1]-Ti[i, k])/R_k)

                    if Ti[i, k]+fact <= Ti[i, k] and Ti[i, k]+fact >= Ti[i, k-1]:
                        Tf[i, k] = Ti[i, k]+fact
                    else:
                        Tf[i, k] = Ti[i, k]

                if k == 2:
                    fact = (dt/(HC.get_heat_cap(mat[i], Ti[i,k])*M[i]))*((Ti[i, k-1]-Ti[i, k])/R_k+Load[i-1])
                    if Ti[i, k]+fact <= Ti[i, k] and Ti[i, k]+fact >= Ti[i, k-1]:
                        Tf[i, k] = Ti[i,k]+fact
                    else:
                        Tf[i, k] = Ti[i, k]

    if np.all(np.abs(Tf-Ti) <=9e-9):
        count+=1

    if count == 10:
        dt = dt/10.
        count == 0

    if Ti[-3,2] <= 30. and dt >=5.:
        dt = dt/10.
    tf += dt

    print 'Time', tf/3600., 'h'
    print '300K Stage', Tf[0,:]
    if ns == 6:
        T80 = np.vstack((T80,Tf[1,:]))
        T40 = np.vstack((T40,Tf[2,:]))
        T4 = np.vstack((T4,Tf[3,:]))
        T1 = np.vstack((T1,Tf[4,:]))
        T100 = np.vstack((T100,Tf[5,:]))
        print '80K Stage', Tf[1,:]
        print '40K Stage', Tf[2,:]
        print '4K Stage', Tf[3,:]
        print '1K Stage', Tf[4,:]
        print '100mK Stage', Tf[5,:]
        with open(outdir+'80K_Temp.txt', 'a+') as f80:
            f80.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[1,0], Tf[1,1], Tf[1,2]))
        with open(outdir+'40K_Temp.txt', 'a+') as f40:
            f40.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[2,0], Tf[2,1], Tf[3,2]))
        with open(outdir+'4K_Temp.txt', 'a+') as f4:
            f4.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[3,0], Tf[3,1], Tf[3,2]))
        with open(outdir+'1K_Temp.txt', 'a+') as f1:
            f1.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[4,0], Tf[4,1], Tf[4,2]))
        with open(outdir+'100mK_Temp.txt', 'a+') as f100:
            f100.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[5,0], Tf[5,1], Tf[5,2]))
    elif ns ==5:
        T40 = np.vstack((T40,Tf[1,:]))
        T4 = np.vstack((T4,Tf[2,:]))
        T1 = np.vstack((T1,Tf[3,:]))
        T100 = np.vstack((T100,Tf[4,:]))
        print '40K Stage', Tf[1,:]
        print '4K Stage', Tf[2,:]
        print '1K Stage', Tf[3,:]
        print '100mK Stage', Tf[4,:]
        with open(outdir+'40K_Temp.txt', 'a+') as f40:
            f40.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[1,0], Tf[1,1], Tf[1,2]))
        with open(outdir+'4K_Temp.txt', 'a+') as f4:
            f4.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[2,0], Tf[2,1], Tf[2,2]))
        with open(outdir+'1K_Temp.txt', 'a+') as f1:
            f1.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[3,0], Tf[3,1], Tf[3,2]))
        with open(outdir+'100mK_Temp.txt', 'a+') as f100:
            f100.write('{:010.6f} {:010.6f} {:010.6f} {:010.6f}\n'.format(tf, Tf[4,0], Tf[4,1], Tf[4,2]))
    t = np.append(t, tf)

    if Tf[1,2]==300. and tf>=200.:
        break

    if np.all(np.greater(HeatLoad,Q)) == True and np.any(np.greater(Tf[1:,0],Tmin)) == False:
        break

    Ti = Tf.copy()
